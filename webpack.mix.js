let mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/assets/js/app.js", "public/js");
mix.js("resources/assets/js/viewProjetos.js", "public/js");
mix.js("resources/assets/js/editPeriodos.js", "public/js");
mix
  .js("resources/assets/js/planilha.js", "public/js")
  .sourceMaps(true, "source-map");

mix.sass("resources/assets/sass/app.scss", "public/css");
