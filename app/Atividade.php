<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
	protected $table = 'atividade';

	protected $fillable = [
		'nome', 'data_inicial', 'data_fim', 'descricao', 'quantidade'
	];

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

	public function categorias()
	{
		return $this->hasMany('App\Categoria');
	}

	public static function boot()
    {
       parent::boot();
       static::deleting(function($atividade){

       	foreach($atividade->categorias as $categoria => $categoria) { 
       		}
    });

  }
}