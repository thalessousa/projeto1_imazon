<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroPlanilha extends Model{
	protected $table = 'registro_planilha';

	public function rubrica(){
		return $this->belongsTo('App\Rubrica');
	}
}