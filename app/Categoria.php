<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model{
	protected $table = 'categoria';

	public function atividade(){
		return $this->belongsTo('App\Atividade');
	}

	public function rubricas()
	{
		return $this->hasMany('App\Rubrica');
	}

	public static function boot()
    {
       parent::boot();
       static::deleting(function($categoria){

       	foreach($categoria->rubrica as $rubrica => $rubrica) { }
    });

  }
}