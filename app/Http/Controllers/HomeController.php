<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use View;
use Validator;
use Redirect;

class HomeController extends Controller
{
    //

    public function show_login(){
        return View::make('login');
    }

    public function show_home(){
        return View::make('home');
    }

    public function login(Request $request){
        $rules = [
            'email' => 'required|email',
            'senha' => 'required|alphaNum|min:5'
        ];

        $input = $request->all();

        $validator = Validator::make($input,$rules);

        if ($validator->fails()){
            return Redirect::to('/')
                ->withErrors($validator)
                ->withInput();
        }

        $email = $request->get('email');
        $senha = $request->get('senha');

        if (Auth::attempt([
            'email'    => $email,
            'password' => $senha
        ])) {
        // if (Auth::attempt([
        //     'nome_usuario' => $request->get('email'),
        //     'senha' => $request->get('senha'),
        // ])) {
            // Authentication passed...
            return Redirect::to('/home');
        }
        else{
            return redirect('/')->withErrors(['field' => 'Login não foi bem sucedido, por favor tente novamente.']);
        }

        return Redirect::to('/');
    }

    public function logout(Request $request){
        Auth::logout();
        
        return Redirect::to(route('login'));
    }
}
