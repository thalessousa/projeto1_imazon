<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rubrica;

use Hash;
use View;
use Redirect;
use Validator;

class RubricaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('rubrica.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nome'                => 'required|nome|unique:nome',
            'comprometido'         => 'required|alphaNum|min:3|confirmed',
            'executado'         => 'required|alphaNum|min:3|confirmed',
            'saldo'         => 'required|alphaNum|min:3|confirmed'
        ];

        $input = $request->all();

        $rubrica = new Rubrica;
        $rubrica->nome = $input['nome'];
        $rubrica->comprometido = $input['comprometido'];
        $rubrica->executado = $input['executado'];
        $rubrica->saldo = $input['saldo'];

        $rubrica->save();

        return Redirect::to(route('/home'))
                ->with('message','Rubrica cadastrada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
