<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Hash;
use View;
use Redirect;
use Validator;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * TO-DO: 
         *  - Validações:
         *  -   Senha no mínimo 5 caracteres 
         *  -   Senha precisa ser igual à confirmação
         *  -   Email válido
         *  -   Nome de usuário e email precisam ser únicos
         *  -       Provavelmente já existe algo no laravel que faz isso, pesquisar
         */

        $rules = [
            'email'         => 'required|email|unique:usuario',
            'nome_usuario'  => 'required|unique:usuario',
            'senha'         => 'required|alphaNum|min:5|confirmed'
        ];
        $input = $request->all();

        $validator = Validator::make($input, $rules);

        if ($validator->fails()){
            return Redirect::to(route('usuarios.criar'))
                ->withErrors($validator)
                ->withInput($request->except(['senha','senha_confirmation']));
        }

        $user = new User;
        $user->email = $input['email'];
        $user->nome_usuario = $input['nome_usuario'];
        $user->nome = $input['nome'];
        $user->senha = Hash::make($input['senha']);
        $user->admin = isset($input['admin']) && $input['admin'] == true;

        $user->save();

        return Redirect::to(route('usuarios.criar'))
                ->with('message','Usuário cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
