<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projeto;

use App\Atividade;
use App\RegistroPlanilha;
use App\Rubrica;
use View;
use Redirect;
use Validator;
use DB;

use \CloudConvert\Api;

const EPSILON = 0.00001;

class ProjetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return View::make('projeto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'nome' => 'required|nome|unique:nome',
            'data_inicio' => 'required|alphaNum|min:8|confirmed',
            'data_fim' => 'required|alphaNum|min:8|confirmed'
        ];

        $input = $request->all();
        $user = auth()->user();

        $periodos = collect(json_decode($input['periodos']));


        $projeto = new Projeto;

        $projeto->nome = $input['nome'];
        $projeto->data_inicio = $input['data_inicio'];
        $projeto->data_fim = $input['data_fim'];
        $projeto->descricao = $input['descricao'];
        $projeto->criador = $user->id;

        $projeto->save();


        $periodos
            ->map(function ($periodo) use ($projeto) {
                $new_periodo = new Atividade;

                //marca atividade como período(ou "atividade-mãe")
                $new_periodo->is_periodo = true;
                $new_periodo->projeto_id = $projeto->id;
                $new_periodo->nome = $periodo->nomePeriodo;
                $new_periodo->data_inicio = $periodo->data_inicioPeriodo;
                $new_periodo->data_fim = $periodo->data_fimPeriodo;

                return $new_periodo;
            })
            ->each(function ($new_periodo) {
                $new_periodo->save();
            });

        return Redirect::to(route('atividade.criar', $projeto->id))
            ->with('message', 'Projeto Cadastrado com Sucesso! Insira os dados para as atividades do projeto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'data_inicio' => 'date',
            'data_fim' => 'date',
            'orcado' => 'numeric|gte:0'
        ];

        $input = $request->all();

        $rota = route('projeto.view');
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to($rota)
                ->withErrors($validator)
                ->withInput();
        }

        $projeto = Projeto::find($id);

        $request->nome && ($projeto->nome = $request->nome);
        $request->data_inicio && ($projeto->data_inicio = $request->data_inicio);
        $request->data_fim && ($projeto->data_fim = $request->data_fim);
        $request->orcado && ($projeto->orcado = $request->orcado);

        $projeto->save();

        return Redirect::to($rota);
    }


    private function obterProjetoComInfo($id)
    {
        $projeto = Projeto::find($id);

        $atividadesGerais = $projeto
            ->atividades()
            ->orderBy('id', 'asc')
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            });

        $atividades = $atividadesGerais
            ->filter(function ($atividade) {
                return !$atividade->is_periodo;
            })
            ->map(function ($atividade) use ($periodos) {
                $periodoId = $atividade->periodo_id;
                $periodoIndex = $periodos->search(function ($periodo) use ($periodoId) {
                    return $periodo->id == $periodoId;
                });
                $periodo = $periodos[$periodoIndex];
                $atividade->nomePeriodo = $periodo->nome;

                return $atividade;
            })
            ->sortBy('id')
            ->sortBy('periodo_id')
            ->values();

        $errors = [];

        if (!$projeto) {
            array_push($errors, "Projeto de id $id não encontrado!");
        }

        if ($projeto->finalizado) {
            array_push($errors, "Projeto de id $id já foi finalizado!");
        }

        $projeto->atividades = $atividades->map(function ($atividade) {
            $atividade->nome = "$atividade->nomePeriodo - $atividade->nome";
            $atividade->categorias = $atividade->categorias->map(function ($categoria) {
                //calcular comprometido
                $categoria->comprometido = $categoria->rubricas->reduce(function ($acc, $rubrica) {
                    return $acc + $rubrica->comprometido;
                }, 0);

                $categoria->executado = $categoria->rubricas->reduce(function ($acc, $rubrica) {
                    return $acc + $rubrica->executado;
                }, 0);

                return $categoria;
            });

            return $atividade;
        });

        $registrosPlanilha = RegistroPlanilha::with('rubrica')->whereHas('rubrica.categoria.atividade.projeto', function ($query) use ($projeto) {
            $query->where('id', '=', $projeto->id);
        })->get();

        $projeto->periodos = array_values($periodos->toArray());
        $projeto->registrosPlanilha = $registrosPlanilha;

        return [$projeto, $errors];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function planilha_edit($id)
    {
        $res = $this->obterProjetoComInfo($id);
        $projeto = $res[0];
        $errors = $res[1];

        return View::make('projeto.edit-planilha')
            ->with(['projeto' => $projeto])
            ->withErrors($errors);
    }

    private function verificaSomaOrcados($dadosProjeto, $projeto)
    {
        $dadosAtividades = collect($dadosProjeto->atividades);

        $somaOrcadoAtividades = $dadosAtividades->reduce(function ($acc, $atividade) {
            return $acc + $atividade->orcado;
        }, 0);

        $result = ($somaOrcadoAtividades > $dadosProjeto->orcado) ?
            [null]
            : [];

        return array_merge(
            $result,
            $dadosAtividades->reduce(function ($acc, $atividade) use ($projeto) {
                $categorias = collect($atividade->categorias);

                $somaOrcadoCategorias = $categorias->reduce(function ($soma, $categoria) {
                    return $soma + $categoria->valor;
                }, 0);

                if ($somaOrcadoCategorias != $atividade->orcado) {
                    //soma deu errado
                    return array_merge($acc, $categorias->map(function ($categoria) use ($atividade) {
                        return [$categoria->nome, $atividade->nome];
                    })->toArray());
                }


                $result = [];
                foreach ($categorias as $key => $categoria) {
                    $rubricas = $categoria->rubricas;
                    $somaOrcadoRubricas = collect($rubricas)->reduce(function ($soma, $rubrica) {
                        return $soma + $rubrica->orcado;
                    }, 0);

                    if ($somaOrcadoRubricas != $categoria->valor) {
                        //soma deu errado
                        array_push($result, [$categoria->nome, $atividade->nome]);
                    }
                }

                return array_merge($acc, $result);
            }, [])
        );
    }

    private function verificaSomaComprometidos($dadosProjeto)
    {
        $dadosAtividades = $dadosProjeto->atividades;

        return collect($dadosAtividades)->reduce(function ($acc, $atividade) use (&$iAtividade) {
            $categorias = collect($atividade->categorias);

            $result = [];
            foreach ($categorias as $key => $categoria) {
                $rubricas = $categoria->rubricas;

                $somaComprometidoRubricas = collect($rubricas)->reduce(function ($soma, $rubrica) {
                    return $soma + $rubrica->comprometido;
                }, 0);

                if ($somaComprometidoRubricas != $categoria->comprometido) {
                    //soma deu errado
                    array_push($result, [$categoria->nome, $atividade->nome]);
                }
            }

            return array_merge($acc, $result);
        }, []);
    }

    private function verificaSomaComprometidoComExecutado($dadosProjeto)
    {
        $dadosAtividades = $dadosProjeto->atividades;

        return collect($dadosAtividades)->reduce(function ($acc, $atividade) use (&$iAtividade) {
            $categorias = collect($atividade->categorias);

            $result = [];
            foreach ($categorias as $key => $categoria) {
                $rubricas = $categoria->rubricas;

                if ($categoria->comprometido + $categoria->executado > $categoria->valor) {
                    //comprometido + executado é maior que orçado, não é permitido
                    array_push($result, [$categoria->nome, $atividade->nome]);
                    return $result;
                }

                //faz a verificação dessa soma para rúbricas e retorna se
                $somaDeuErradoRubricas = collect($rubricas)->reduce(function ($accRubrica, $rubrica) {
                    if ($accRubrica) {
                        return true;
                    }

                    if ($rubrica->comprometido + $rubrica->executado > $rubrica->orcado) {
                        return true;
                    }

                    return false;
                }, false);

                if ($somaDeuErradoRubricas) {
                    array_push($result, [$categoria->nome, $atividade->nome]);
                }
            }

            return array_merge($acc, $result);
        }, []);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function planilha_update(Request $request, $id)
    {
        $dadosProjeto = json_decode($request->planilhas);

        $projeto = Projeto::find($id);
        $projeto->orcado = $dadosProjeto->orcado;
        $projeto->aplicacao = $dadosProjeto->aplicacao;
        $projeto->rendimento = $dadosProjeto->rendimento;
        $projeto->save();

        $atividadesGerais = $projeto
            ->atividades()
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            });

        collect($dadosProjeto->atividades)->filter(function ($atividade) {
            return !$atividade->is_periodo;
        })->each(function ($atividade) {
            $atividade_db = Atividade::find($atividade->id);
            $atividade_db->orcado = $atividade->orcado;
            $atividade_db->save();
        });

        $atividades = $atividadesGerais
            ->filter(function ($atividade) {
                return !$atividade->is_periodo;
            })
            ->map(function ($atividade) use ($periodos) {
                $periodoId = $atividade->periodo_id;
                $periodoIndex = $periodos->search(function ($periodo) use ($periodoId) {
                    return $periodo->id == $periodoId;
                });
                $periodo = $periodos[$periodoIndex];
                $atividade->nomePeriodo = $periodo->nome;

                return $atividade;
            })->values();


        $errors = [];


        if (count($errors) > 0) {
            return View::make('projeto.edit-planilha')
                ->with(['projeto' => $projeto])
                ->withErrors($errors);
        }

        $atividades->each(function ($atividade, $iAtividade) use ($dadosProjeto, &$errors) {
            $categorias = $atividade->categorias;
            $atividade->categorias->each(function ($categoria, $iCategoria) use ($atividade,$dadosProjeto, $categorias, &$errors, $iAtividade) {
                $categoriaEntrada = ($dadosProjeto
                    ->atividades[$iAtividade]
                    ->categorias[$iCategoria]);

                $categoria->nome = $categoriaEntrada->nome;
                $categoria->valor = $categoriaEntrada->valor;

                $categoria->save();

                $rubricas = $categoria->rubricas;

                $rubricas->each(
                    function ($rubrica, $iRubrica) use ($atividade, $categoria, $rubricas, $dadosProjeto, &$errors, $iAtividade, $iCategoria) {
                        $rubricaEntrada = ($dadosProjeto
                            ->atividades[$iAtividade]
                            ->categorias[$iCategoria]
                            ->rubricas[$iRubrica]);

                        $rubricasMesmoCodigo = Rubrica::whereHas('categoria.atividade.projeto',function($query) use($dadosProjeto,$categoria,$rubricaEntrada){
                            $query->where([
                                [ 'projeto.id'  , '=', $dadosProjeto->id ],
                                [ 'rubrica.codigo', '=', $rubricaEntrada->codigo ]
                            ]);
                        })->get();

                        // Se mudou o código da rubrica, só atualiza se não existir outra rúbrica com esse código
                        if (strtoupper($rubricaEntrada->codigo) !== strtoupper($rubrica->codigo)){
                            if ($iAtividade === 0){
                                if (strtoupper($rubricaEntrada->codigo) !== strtoupper($rubrica->codigo) && $rubricasMesmoCodigo->count() === 0){
                                    /*
                                     * Se for a primeira atividade a ser processada, faz a atualização de código
                                     *  para todas as rubricas de mesmo código de outras atividades
                                     *
                                     * Se náo for a primeira atividade, não faz nada porque a atualização já foi feita
                                     */
                                    
                                    Rubrica::where('codigo','=',$rubrica->codigo)->update(['codigo' => $rubricaEntrada->codigo]);
                                }
                                else{
                                    array_push($errors, "Rubrica de código $rubricaEntrada->codigo já existe!");
                                }
        
                            }
                        }

                        $rubrica->nome = $rubricaEntrada->nome;
                        $rubrica->orcado = $rubricaEntrada->orcado;
                        $rubrica->comprometido = $rubricaEntrada->comprometido;
                        $rubrica->recebido = $rubricaEntrada->recebido;
                        $rubrica->executado = $rubricaEntrada->executado;
                        $rubrica->executado_manual = $rubricaEntrada->executado_manual;
                        $rubrica->saldo = $rubricaEntrada->saldo;
                        $rubrica->saldo_orcamentario = $rubricaEntrada->saldo_orcamentario;
                        $rubrica->saldo_remanescente = $rubricaEntrada->saldo_remanescente;

                        if (isset($rubricaEntrada->registrosPlanilha)) {
                            $registrosPlanilha = collect($rubricaEntrada->registrosPlanilha)
                                ->map(function ($registro) use ($rubrica) {
                                    $novoRegistro = new RegistroPlanilha;
                                    $novoRegistro->rubrica_id = $rubrica->id;
                                    $novoRegistro->emissao = $registro->emissao;
                                    $novoRegistro->baixa = $registro->baixa;

                                    $novoRegistro->numero = $registro->numero;
                                    $novoRegistro->gerencial = $registro->gerencial;
                                    $novoRegistro->sacado_cedente = $registro->sacado_cedente;
                                    $novoRegistro->historico = $registro->historico;

                                    $novoRegistro->valor = $registro->valor;

                                    return $novoRegistro;
                                });
                            RegistroPlanilha::insert($registrosPlanilha->toArray());
                        }

                        $rubrica->save();
                    }
                );
            });
        });

        //Registros planilha entrada
        $novosRegistrosPlanilha = collect($dadosProjeto->registrosPlanilha)
            ->map(function ($registro) {
                $novoRegistro = new RegistroPlanilha;
                $novoRegistro->rubrica_id = $registro->rubrica_id;
                $novoRegistro->emissao = $registro->emissao;
                $novoRegistro->baixa = $registro->baixa;

                $novoRegistro->numero = $registro->numero;
                $novoRegistro->gerencial = $registro->gerencial;
                $novoRegistro->sacado_cedente = $registro->sacado_cedente;
                $novoRegistro->historico = $registro->historico;

                $novoRegistro->valor = $registro->valor;
                $novoRegistro->composicao = $registro->composicao;

                return $novoRegistro;
            });

        DB::transaction(function () use ($novosRegistrosPlanilha, $dadosProjeto) {
            //apagar registros antigos desse projeto
            RegistroPlanilha::whereHas('rubrica.categoria.atividade.projeto', function ($query) use ($dadosProjeto) {
                $query->where('id', '=', $dadosProjeto->id);
            })->delete();

            //salvar novos
            RegistroPlanilha::insert($novosRegistrosPlanilha->toArray());
        });

        if (count($errors) > 0) {
            return View::make('projeto.edit-planilha')
                ->with(['projeto' => $this->obterProjetoComInfo($id)[0]])
                ->withErrors($errors);
        }

        return Redirect::to(route('projeto.planilha.editar', $id));
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view()
    {
        $projetos = Projeto::with('atividades')->get()->map(function ($projeto) {
            $projeto->periodos = $projeto->atividades->filter(function ($atv) {
                return $atv->is_periodo;
            })->values();

            return $projeto;
        });

        return View::make('projeto.view')
            ->with(['projetos' => $projetos]);
    }

    public function finalizar($id)
    {
        $errors = [];

        $projeto = Projeto::find($id);

        if (!$projeto) {
            array_push($errors, "Projeto de id $id não encontrado!");
        } else if ($projeto->finalizado) {
            array_push($errors, "Projeto de id $id já finalizado!");
        }

        if (count($errors) === 0) {
            $projeto->finalizado = true;
            $projeto->save();

            return Redirect::to(
                route('projeto.view')
            )->with('message', "Projeto de id $id finalizado com sucesso!")
                ->withErrors([]);
        } else {
            return Redirect::to(
                route('projeto.view')
            )->withErrors($errors);
        }
    }

    public function reativar($id)
    {
        $errors = [];

        $projeto = Projeto::find($id);

        if (!$projeto) {
            array_push($errors, "Projeto de id $id não encontrado!");
        } else if (!$projeto->finalizado) {
            array_push($errors, "Projeto de id $id não está finalizado!");
        }

        if (count($errors) === 0) {
            $projeto->finalizado = false;
            $projeto->save();

            return Redirect::to(
                route('projeto.view')
            )->with('message', "Projeto de id $id reativado com sucesso!")
                ->withErrors([]);
        } else {
            return Redirect::to(
                route('projeto.view')
            )->withErrors($errors);
        }
    }

    public function deletar($id)
    {
        $errors = [];

        $projeto = Projeto::find($id);

        if (count($errors) === 0) {
            $projeto = Projeto::find($id);
            $projeto->delete();


            $projeto->delete();
            return Redirect::to(
                route('projeto.view')
            )->with('message', "Projeto de id $id foi deletado com sucesso!")
                ->withErrors([]);
        } else {
            return Redirect::to(
                route('projeto.view')
            )->withErrors($errors);
        }
    }

    public function graficos_view($id)
    {
        //
    }


    public function planilhaPdf(Request $request, $id)
    {
        $base64Data = $request->get('base64Data');

        try {
            $api = new Api(\Config::get('app.fileConvertApi'));

            $resposta = $api->convert([
                'inputformat' => 'xlsx',
                'outputformat' => 'pdf',
                'input' => 'base64',
                'filename' => 'planilha.xlsx',
                'file' => $base64Data
            ]);
        } catch (\CloudConvert\Exceptions\ApiException $e) {
            $projeto = $this->obterProjetoComInfo($id)[0];

            return View::make('projeto.edit-planilha')
                ->with(['projeto' => $projeto])
                ->withErrors(['Erro ao converter planilha para PDF! Por favor tente mais tarde']);
        }

        $urlArquivoConvertido = $resposta->output->url;
        return \Redirect::to($urlArquivoConvertido);
    }
}
