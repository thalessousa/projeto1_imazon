<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Projeto;
use App\Atividade;
use App\Categoria;
use App\Rubrica;

use View;
use Redirect;
use Validator;
use DateTime;

const EPSILON = 0.00001;

class AtividadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idProjeto)
    {
        $periodos = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', true);


        return View::make('atividade.create')
            ->with([
                'idProjeto' => $idProjeto,
                'periodos' => $periodos
            ]);
    }

    public function adicionar_periodo($idProjeto)
    {
        $projeto = Projeto::find($idProjeto);
        extract(get_object_vars($projeto->obterProjetoInfo()));

        return View::make('atividade.adicionar_periodo')
            ->with([
                'idProjeto'  => $idProjeto,
                'projeto'    => $projeto,
                'atividades' => $atividades,
                'categorias' => $categorias,
                'rubricas'   => $rubricas
            ]);
    }

	
	public function atualizar_periodo(Request $request,$idPeriodo){

        $rules = [
            'data_inicio' => 'date',
            'data_fim' => 'date',
            'orcado' => 'numeric|gte:0'
        ];

        $input = $request->all();

        $rota = route('projeto.view');
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to($rota)
                ->withErrors($validator)
                ->withInput();
        }

		$periodo = Atividade::find($idPeriodo);

        $request->nome && ($periodo->nome = $request->nome);
        $request->data_inicio && ($periodo->data_inicio = $request->data_inicio);
        $request->data_fim && ($periodo->data_fim = $request->data_fim);
        $request->orcado && ($periodo->orcado = $request->orcado);


        $periodo->save();

        return Redirect::to($rota);

	}


    public function adicionar_atividade($idProjeto)
    {
        $projeto = Projeto::find($idProjeto);
        $periodos = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', true)->get();

        $categorias = Categoria::whereHas('atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select('*')->groupBy('nome')->get();


        $rubricas = Rubrica::whereHas('categoria.atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select(['nome', 'codigo', 'categoria_id'])->groupBy('nome')->get();

        return View::make('atividade.adicionar_atividade')
            ->with([
                'idProjeto'  => $idProjeto,
                'projeto'    => $projeto,
                'periodos'   => $periodos,
                'categorias' => $categorias,
                'rubricas'   => $rubricas
            ]);
    }

    const NOME_JA_EXISTE = 0;
    const CODIGO_JA_EXISTE = 1;
    const ATIVIDADE_NAO_EXISTE = 2;


    //Verifica se já existe uma atividade com determinado nome ou código em um projeto específico
    private static function verificaAtividadeExiste($nome, $codigo, $projeto)
    {

        $nAtividadeComNome = Atividade::where('projeto_id', '=', $projeto->id)->where('nome', 'LIKE', $nome)->count();
        if ($nAtividadeComNome > 0) {
            return self::NOME_JA_EXISTE;
        }

        $nAtividadeComCodigo = Atividade::where('projeto_id', '=', $projeto->id)->where('codigo', 'LIKE', $codigo)->count();

        if ($nAtividadeComCodigo > 0) {
            return self::CODIGO_JA_EXISTE;
        }

        return self::RUBRICA_NAO_EXISTE;
    }

    public function store_add_atividade(Request $request, $idProjeto)
    {
        $atividade = json_decode($request->atividade);

        //Obter informação dos períodos salvos no banco de dados
        //===============================================
        $projeto = Projeto::find($idProjeto);
        $atividadesGerais = $projeto
            ->atividades()
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            })
            ->values();

        //===============================================

        $verificaAtividadeExiste = AtividadeController::verificaAtividadeExiste($atividade->nome, $atividade->codigo, $projeto);
        $erro = $verificaAtividadeExiste == self::CODIGO_JA_EXISTE
            ? 'Código informado já existe, favor inserir outro'
            : ($verificaAtividadeExiste == self::NOME_JA_EXISTE
                ? 'Nome informado já existe, favor inserir outro'
                : null);

        if ($erro) {
            $periodos = Atividade::where('projeto_id', '=', $idProjeto)
                ->where('is_periodo', '=', true)->get();

            $categorias = Categoria::whereHas('atividade.projeto', function ($query) use ($idProjeto) {
                $query->where('id', '=', $idProjeto);
            })->select('*')->groupBy('nome')->get();

            $rubricas = Rubrica::whereHas('categoria.atividade.projeto', function ($query) use ($idProjeto) {
                $query->where('id', '=', $idProjeto);
            })->select(['nome', 'codigo', 'categoria_id'])->groupBy('nome')->get();

            return View::make('atividade.adicionar_atividade')
                ->with([
                    'idProjeto'  => $idProjeto,
                    'projeto'    => $projeto,
                    'periodos'   => $periodos,
                    'categorias' => $categorias,
                    'rubricas'   => $rubricas
                ])
                ->withErrors($erro);
        }

        $categoriasInfo = collect($atividade->categorias);

        //criar uma instância da atividade nova para cada período
        $novasAtividades = $periodos->map(function ($periodo, $i) use ($atividade) {

            $novaAtividade = new Atividade;
            $novaAtividade->projeto_id = $periodo->projeto_id;
            $novaAtividade->periodo_id = $periodo->id;
            $novaAtividade->nome = $atividade->nome;
            $novaAtividade->codigo = $atividade->codigo;
            $novaAtividade->orcado = $atividade->orcado[$i];

            $novaAtividade->save();
            return $novaAtividade;
        });

        $categoriasInfo->each(function ($categoria) use ($novasAtividades) {

            $novasCategorias = collect($categoria->orcados)->map(function ($orcado, $iOrcado) use ($novasAtividades, $categoria) {

                $new_categoria = new Categoria;
                $new_categoria->atividade_id = $novasAtividades[$iOrcado]->id;
                $new_categoria->nome = $categoria->nome;
                $new_categoria->valor = $orcado;

                $new_categoria->save();
                return $new_categoria;
            });


            $rubricasInfo = collect($categoria->rubricas);

            $rubricasInfo->each(function ($rubrica) use ($novasCategorias) {
                $novasRubricas = collect($rubrica->orcados)->map(function ($orcado, $iOrcado) use ($rubrica, $novasCategorias) {
                    $new_rubrica = new Rubrica;
                    $new_rubrica->categoria_id = $novasCategorias[$iOrcado]->id;
                    $new_rubrica->nome = $rubrica->nome;
                    $new_rubrica->codigo = $rubrica->codigo;
                    $new_rubrica->orcado = $orcado;

                    $new_rubrica->save();
                    return $new_rubrica;
                });
            });
        });

        return Redirect::to(
            route('projeto.planilha.editar', $idProjeto)
        )->with('message', 'Atividade adicionada com sucesso!');
    }
    public function store_add_periodo(Request $request, $idProjeto)
    {
        $periodo = json_decode($request->periodo);

        //Obter informação dos períodos salvos no banco de dados
        //===============================================
        $projeto = Projeto::find($idProjeto);
        $atividadesGerais = $projeto
            ->atividades()
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            })
            ->values();

        $atividadesProjeto = $atividadesGerais
            ->filter(function ($atividade) {
                return !$atividade->is_periodo;
            })
            ->map(function ($atividade) use ($periodos) {
                $periodoId = $atividade->periodo_id;
                $periodoIndex = $periodos->search(function ($periodo) use ($periodoId) {
                    return $periodo->id == $periodoId;
                });
                $periodo = $periodos[$periodoIndex];
                $atividade->nomePeriodo = $periodo->nome;

                return $atividade;
            })->values();
        //===============================================


        $atividadesInfo = collect($periodo->atividades);
        $categoriasInfo = collect($periodo->categorias);

        // Criar novo período
        //====================================================
        $novoPeriodo = new Atividade;
        //marca atividade como período
        $novoPeriodo->is_periodo = true;
        $novoPeriodo->projeto_id = $idProjeto;
        $novoPeriodo->nome = $periodo->nome;
        $novoPeriodo->orcado = (double)$periodo->orcado;
        $novoPeriodo->data_inicio = $periodo->dataInicio;
        $novoPeriodo->data_fim = $periodo->dataFim;

        $novoPeriodo->save();
        //====================================================

        $novasAtividades = $atividadesInfo->map(function ($atividade) use ($novoPeriodo) {
            $new_atividade = new Atividade;
            $new_atividade->projeto_id = $novoPeriodo->projeto_id;
            $new_atividade->periodo_id = $novoPeriodo->id;
            $new_atividade->nome = $atividade->nome;
            $new_atividade->codigo = $atividade->codigo;
            $new_atividade->orcado = $atividade->orcado;
            $new_atividade->data_inicio = $atividade->dataInicio;
            $new_atividade->data_fim = $atividade->dataFim;

            $new_atividade->save();

            return $new_atividade;
        });

        $categoriasInfo->each(function ($categoria) use ($novasAtividades) {

            $novasCategorias = collect($categoria->orcados)->map(function ($orcado, $iOrcado) use ($novasAtividades, $categoria) {

                $new_categoria = new Categoria;
                $new_categoria->atividade_id = $novasAtividades[$iOrcado]->id;
                $new_categoria->nome = $categoria->nome;
                $new_categoria->valor = $orcado;

                $new_categoria->save();
                return $new_categoria;
            });


            $rubricasInfo = collect($categoria->rubricas);

            $rubricasInfo->each(function ($rubrica) use ($novasCategorias) {
                $novasRubricas = collect($rubrica->orcados)->map(function ($orcado, $iOrcado) use ($rubrica, $novasCategorias) {
                    $new_rubrica = new Rubrica;
                    $new_rubrica->categoria_id = $novasCategorias[$iOrcado]->id;
                    $new_rubrica->nome = $rubrica->nome;
                    $new_rubrica->codigo = $rubrica->codigo;
                    $new_rubrica->orcado = $orcado;

                    $new_rubrica->save();
                    return $new_rubrica;
                });
            });
        });


        return Redirect::to(
            route('projeto.planilha.editar', $idProjeto)
        )->with('message', 'Período adicionado com sucesso!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idProjeto)
    {
        $atividades = json_decode($request->atividades);
        $periodos = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', true)->get();


        //verificar soma de orçados antes de salvar qualquer coisa
        $listaPeriodos = collect($periodos);
        $erros = [];

        if (count($erros) > 0) {
            return Redirect::to(route('atividade.criar', $idProjeto))
                ->withErrors($erros);
        }

        $listaPeriodos->each(function ($periodo, $iPeriodo) use ($idProjeto, $atividades) {

            collect($atividades)->each(function ($atividade) use ($idProjeto, $periodo, $iPeriodo) {
                $new_atividade = new Atividade;
                $new_atividade->projeto_id = $idProjeto;
                $new_atividade->periodo_id = $periodo->id;
                $new_atividade->nome = $atividade->nome;
                $new_atividade->codigo = $atividade->codigo;
                $new_atividade->data_inicio = $periodo->data_inicio;
                $new_atividade->data_fim = $periodo->data_fim;

                $new_atividade->save();
            });
        });

        return Redirect::to(route('categoria.criar', $idProjeto))
            ->with('message', 'Atividade(s) cadastrada(s) com sucesso! Insira agora os dados das categorias e rúbricas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private static function criarNovoPeriodo($periodo_finalizado, $nome_novo_periodo, $projeto)
    {

        $idProjeto = $projeto->id;

        $novo_periodo = new Atividade();
        $novo_periodo->projeto_id = $idProjeto;
        $novo_periodo->is_periodo = 1;
        $novo_periodo->data_inicio = new DateTime();
        $novo_periodo->nome = $nome_novo_periodo;
        $novo_periodo->orcado = 0;

        $novo_periodo->save();

        $atividades = $projeto
            ->atividades()
            ->where('periodo_id', '=', $periodo_finalizado->id)
            ->get();

        //criar uma instância da atividade nova para cada período
        $novasAtividades = $atividades->map(function ($atividade) use ($novo_periodo) {

            $novaAtividade = new Atividade;
            $novaAtividade->projeto_id = $novo_periodo->projeto_id;
            $novaAtividade->periodo_id = $novo_periodo->id;
            $novaAtividade->nome = $atividade->nome;
            $novaAtividade->codigo = $atividade->codigo;
            $novaAtividade->orcado = 0;

            $novaAtividade->save();
            return $novaAtividade;
        });

        $categorias = $atividades[0]->categorias();


        $categorias->each(function ($categoria) use ($novasAtividades) {

            $novasAtividades->each(function ($atv) use ($categoria) {
                $new_categoria = new Categoria;
                $new_categoria->atividade_id = $atv->id;
                $new_categoria->nome = $categoria->nome;
                $new_categoria->valor = 0;

                $new_categoria->save();

                $rubricas = $categoria->rubricas();

                $rubricas->each(function ($rubrica) use ($new_categoria) {
                    $new_rubrica = new Rubrica;
                    $new_rubrica->categoria_id = $new_categoria->id;
                    $new_rubrica->nome = $rubrica->nome;
                    $new_rubrica->codigo = $rubrica->codigo;
                    $new_rubrica->orcado = 0;

                    $new_rubrica->save();
                });
            });
        });


        return $novo_periodo;
    }

    public function finalizar(Request $request, $id)
    {
        $periodo = Atividade::find($id);
        $projeto_id = $periodo->projeto_id;
        $projeto = Projeto::find($projeto_id);
        $nome_novo_periodo = $request->nome_novo_periodo;

        if (!$periodo->is_periodo) {
            //atividade não é período! Retorna erro

            return View::make('projeto.edit-planilha')
                ->with(['projeto' => $projeto])
                ->withErrors(['Atividade não é período!']);
        }

        $periodo->data_fim_real = new DateTime();
        $periodo->save();

        AtividadeController::criarNovoPeriodo($periodo, $nome_novo_periodo, $projeto);

        return Redirect::to(
            route('projeto.planilha.editar', $projeto_id)
        )->with('message', 'Período finalizado com sucesso!');
    }
}
