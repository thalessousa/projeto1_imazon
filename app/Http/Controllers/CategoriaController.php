<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Projeto;
use App\Atividade;
use App\Categoria;
use App\Rubrica;

use View;
use Redirect;

const EPSILON = 0.00001;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idProjeto)
    {
        $projeto = Projeto::find($idProjeto);
        $atividadesGerais = $projeto
            ->atividades()
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            });

        $atividades = $atividadesGerais
            ->filter(function ($atividade) {
                return !$atividade->is_periodo;
            })
            ->map(function ($atividade) use ($periodos) {
                $periodoId = $atividade->periodo_id;
                $periodoIndex = $periodos->search(function ($periodo) use ($periodoId) {
                    return $periodo->id == $periodoId;
                });
                $periodo = $periodos[$periodoIndex];
                $atividade->nomePeriodo = $periodo->nome;

                return $atividade;
            })->values();

        $nAtividadesProjeto = $atividades->count();

        return View::make('categoria.create')
            ->with([
                'idProjeto' => $idProjeto,
                'atividades' => $atividades,
                'nAtividadesProjeto' => $nAtividadesProjeto
            ]);
    }

    private static function verificarSomaOrcadosSubcategoriasRubricasAtividade($subCategorias, $iAtividade, $atividadesProjeto, $periodos)
    {
        $erros = [];

        $subCategorias->each(function ($categoria) use ($iAtividade, $atividadesProjeto, $periodos, &$erros) {
            $nAtividadesProjeto = count($atividadesProjeto);
            $atividadeAtual = $atividadesProjeto[$iAtividade];
            $rubricas = collect($categoria->rubricas);
            $orcadosCategoria = $categoria->orcados;

            $somaOrcadosRubricasAtividade = $rubricas->reduce(function ($soma, $rubrica) use ($iAtividade) {
                return $soma + $rubrica->orcados[$iAtividade];
            }, 0);

            if (abs((double)$somaOrcadosRubricasAtividade - (double)$orcadosCategoria[$iAtividade]) >= EPSILON) {
                $nomeAtv = $atividadesProjeto[$iAtividade]->nome;
                $periodoAtvIndex = $periodos->search(function ($periodo) use ($atividadeAtual) {
                    return $periodo->id === $atividadeAtual->periodo_id;
                });
                $periodoAtv = $periodos[$periodoAtvIndex];
                $nomePeriodoAtv = $periodoAtv->nome;
                $nomeCat = $categoria->nome;
                $msgErro = "Valor orçado para atividade '$nomeAtv - $nomePeriodoAtv' da categoria '$nomeCat' não corresponde à soma dos orçados das suas rúbricas para essa atividade";
                array_push($erros, $msgErro);
            }
        });
        return $erros;
    }


    private static function verificarSomaOrcadosCategoriasRubricas($categorias, $atividadesProjeto, $periodos)
    {
        $erros = [];

        $categorias->each(function ($categoria) use ($atividadesProjeto, $periodos, &$erros) {
            $orcadosCategoria = $categoria->orcados;
            $subCategorias =
                isset($categoria->subCategorias)
                ? collect($categoria->subCategorias)
                : null;
            $rubricas = !$subCategorias
                ? collect($categoria->rubricas)
                : null;

            $nAtividadesProjeto = count($atividadesProjeto);
            //para cada atividade, verificar os orçados
            for ($i = 0; $i < $nAtividadesProjeto; ++$i) {
                $atividadeAtual = $atividadesProjeto[$i];

                if (!$subCategorias) {
                    // não é categoria-mãe
                    $somaOrcadosRubricasAtividade = $rubricas->reduce(function ($soma, $rubrica) use ($i) {
                        return $soma + $rubrica->orcados[$i];
                    }, 0);

                    if (abs((double)$somaOrcadosRubricasAtividade - (double)$orcadosCategoria[$i]) >= EPSILON) {
                        $nomeAtv = $atividadesProjeto[$i]->nome;
                        $periodoAtvIndex = $periodos->search(function ($periodo) use ($atividadeAtual) {
                            return $periodo->id === $atividadeAtual->periodo_id;
                        });
                        $periodoAtv = $periodos[$periodoAtvIndex];
                        $nomePeriodoAtv = $periodoAtv->nome;
                        $nomeCat = $categoria->nome;
                        $msgErro = "Valor orçado para atividade '$nomeAtv - $nomePeriodoAtv' da categoria '$nomeCat' não corresponde à soma dos orçados das suas rúbricas para essa atividade";
                        array_push($erros, $msgErro);
                    }
                } else {
                    // é categoria-mãe
                    // pegar subcategorias filhas
                    // somar orçados de todas as subcategorias e
                    //  comparar com orçados de categoria mãe
                    $somaOrcadosSubCategoriasAtividade = $subCategorias->reduce(function ($soma, $subCat) use ($i) {
                        return $soma + $subCat->orcados[$i];
                    }, 0);

                    $result = CategoriaController::verificarSomaOrcadosSubcategoriasRubricasAtividade($subCategorias, $i, $atividadesProjeto, $periodos);

                    $erros = array_merge(
                        $erros,
                        $result
                    );

                    if (abs((double)$somaOrcadosSubCategoriasAtividade - (double)$orcadosCategoria[$i]) >= EPSILON) {
                        $nomeAtv   = $atividadeAtual->nome;
                        $periodoAtvIndex = $periodos->search(function ($periodo) use ($atividadeAtual) {
                            return $periodo->id === $atividadeAtual->periodo_id;
                        });
                        $periodoAtv = $periodos[$periodoAtvIndex];
                        $nomePeriodoAtv = $periodoAtv->nome;
                        $nomeCat   = $categoria->nome;
                        $msgErro   = "Valor orçado para atividade '$nomeAtv - $nomePeriodoAtv' da categoria '$nomeCat' não corresponde à soma dos orçados das suas subcategorias para essa atividade";
                        array_push($erros, $msgErro);
                    }
                }
            }
        });

        return $erros;
    }

    public function adicionar_rubrica($idProjeto, $nomeCategoria)
    {

        $projeto = Projeto::find($idProjeto);
        $periodos = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', true)
            ->orderBy('id', 'asc')
            ->get();
        $atividades = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', false)
            ->groupBy('nome')
            ->orderBy('periodo_id', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        $categorias = Categoria::whereHas('atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select('*')->groupBy('nome')->get();

        $categoriaIndex = $categorias->search(function ($categoria) use ($nomeCategoria) {
            return $categoria->nome === $nomeCategoria;
        });

        $categoria = $categoriaIndex !== false ? $categorias[$categoriaIndex] : null;
        $erros = [];
        if ($categoria === null) {
            $erros['categoria'] = "Categoria com nome $nomeCategoria não encontrada!";
        }
        $rubricas = Rubrica::whereHas('categoria.atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select(['nome', 'codigo', 'categoria_id'])->groupBy('nome')->get();



        return View::make('categoria.rubrica_adicionar')
            ->with([
                'idProjeto'  => $idProjeto,
                'projeto'    => $projeto,
                'periodos'   => $periodos,
                'atividades' => $atividades,
                'categorias' => $categorias,
                'categoria'  => $categoria,
                'rubricas'   => $rubricas
            ])
            ->withErrors($erros);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idProjeto)
    {
        $projeto = Projeto::find($idProjeto);
        $atividadesGerais = $projeto
            ->atividades()
            ->get();

        $periodos = $atividadesGerais
            ->filter(function ($atividade) {
                return $atividade->is_periodo;
            });

        $atividadesProjeto = $atividadesGerais
            ->filter(function ($atividade) {
                return !$atividade->is_periodo;
            })
            ->map(function ($atividade) use ($periodos) {
                $periodoId = $atividade->periodo_id;
                $periodoIndex = $periodos->search(function ($periodo) use ($periodoId) {
                    return $periodo->id == $periodoId;
                });
                $periodo = $periodos[$periodoIndex];
                $atividade->nomePeriodo = $periodo->nome;

                return $atividade;
            })->values();

        $categorias = collect(json_decode($request->categorias));
        $erros = [];

        if (count($erros) > 0) {
            return View::make('categoria.create')
                ->with([
                    'inputCategorias' => $categorias,
                    'idProjeto' => $idProjeto,
                    'atividades' => $atividadesProjeto,
                    'nAtividadesProjeto' => $atividadesProjeto->count()
                ])
                ->withErrors($erros);
        }

        collect($categorias)->each(function ($categoria) use ($atividadesProjeto) {
            $atividadesProjeto->each(function ($atividade, $i) use ($categoria) {
                $new_categoria = new Categoria;

                $new_categoria->nome = $categoria->nome;
                $new_categoria->atividade_id = $atividade->id;

                $new_categoria->save();

                if (isset($categoria->subCategorias)) {
                    //categoria possui subcategorias
                    $subCategorias = collect($categoria->subCategorias);
                    $subCategorias->each(function ($subCategoria) use ($new_categoria, $atividade, $i) {
                        $newSubCategoria = new Categoria;

                        $newSubCategoria->nome = $subCategoria->nome;
                        $newSubCategoria->atividade_id = $atividade->id;
                        $newSubCategoria->categoria_mae = $new_categoria->id;
                        $newSubCategoria->save();

                        collect($subCategoria->rubricas)->each(function ($rubrica) use ($newSubCategoria) {
                            $new_rubrica = new Rubrica;

                            $new_rubrica->nome = $rubrica->nome;
                            $new_rubrica->codigo = $rubrica->codigo;
                            $new_rubrica->categoria_id = $newSubCategoria->id;

                            $new_rubrica->save();
                        });
                    });
                } else {
                    //categoria é uma subcategoria
                    collect($categoria->rubricas)->each(function ($rubrica) use ($new_categoria) {
                        $new_rubrica = new Rubrica;

                        $new_rubrica->nome = $rubrica->nome;
                        $new_rubrica->codigo = $rubrica->codigo;
                        $new_rubrica->categoria_id = $new_categoria->id;

                        $new_rubrica->save();
                    });
                }
            });
        });


        return Redirect::to(route('projeto.view', $idProjeto))
            ->with('message', 'Categorias e rubricas cadastradas com sucesso!');
    }

    const NOME_JA_EXISTE     = 0;
    const CODIGO_JA_EXISTE   = 1;
    const RUBRICA_NAO_EXISTE = 2;

    //Verifica se já existe uma rubrica com determinado nome ou código para essa categoria
    private static function verificaRubricaExiste($nome, $codigo, $categorias)
    {
        $idsCategorias = $categorias->map(function ($cat) {
            return $cat->id;
        });


        $nRubricaComNome = Rubrica::whereIn('categoria_id', $idsCategorias)->where('nome', 'LIKE', $nome)->count();
        if ($nRubricaComNome > 0) {
            return self::NOME_JA_EXISTE;
        }

        $nRubricaComCodigo = Rubrica::whereIn('categoria_id', $idsCategorias)->where('codigo', 'LIKE', $codigo)->count();

        if ($nRubricaComCodigo > 0) {
            return self::CODIGO_JA_EXISTE;
        }

        return self::RUBRICA_NAO_EXISTE;
    }

    public function store_add_rubrica(Request $request, $idProjeto, $nomeCategoria)
    {
        $novaRubricaInfo = json_decode($request->rubrica);

        $nomeNovaRubrica  = $novaRubricaInfo->nome;
        $codigoNovaRubrica = $novaRubricaInfo->codigo;
        $novaRubricaOrcadosPeriodos = collect($novaRubricaInfo->orcados);
        $categorias = Categoria::where('nome', '=', $nomeCategoria)->whereHas('atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select('*')->get();


        $verificaRubricaExiste = CategoriaController::verificaRubricaExiste($nomeNovaRubrica, $codigoNovaRubrica, $categorias);
        $erro = $verificaRubricaExiste == self::CODIGO_JA_EXISTE
            ? 'Código informado já existe, favor inserir outro'
            : ($verificaRubricaExiste == self::NOME_JA_EXISTE
                ? 'Nome informado já existe, favor inserir outro'
                : null);

        if ($erro) {
            $projeto = Projeto::find($idProjeto);
            $categoriaIndex = $categorias->search(function ($categoria) use ($nomeCategoria) {
                return $categoria->nome === $nomeCategoria;
            });

            $categoria = $categoriaIndex !== false ? $categorias[$categoriaIndex] : null;

            $periodos = Atividade::where('projeto_id', '=', $idProjeto)
                ->where('is_periodo', '=', true)
                ->orderBy('id', 'asc')
                ->get();

            $atividades = Atividade::where('projeto_id', '=', $idProjeto)
                ->where('is_periodo', '=', false)
                ->groupBy('nome')
                ->orderBy('periodo_id', 'asc')
                ->orderBy('id', 'asc')
                ->get();

            return View::make('categoria.rubrica_adicionar')
                ->with([
                    'idProjeto' => $idProjeto,
                    'projeto'    => $projeto,
                    'periodos'   => $periodos,
                    'atividades' => $atividades,
                    'categoria'  => $categoria,
                    'nomeRubrica' => $nomeNovaRubrica,
                    'codigoRubrica' => $codigoNovaRubrica
                ])
                ->withInput($request->all)
                ->withErrors($erro);
        }

        $periodos = $novaRubricaOrcadosPeriodos->map(function ($rubricaInfoPeriodo) use ($idProjeto) {
            $nomePeriodo = $rubricaInfoPeriodo->nomePeriodo;
            return Atividade::where([
                ['is_periodo', '=', true],
                ['projeto_id', '=', $idProjeto],
                ['nome', '=', $nomePeriodo]
            ])->first();
        });


        $novaRubricaInfoAtividades = collect($novaRubricaOrcadosPeriodos->reduce(function ($result, $rubricaInfoPeriodo) {
            return array_merge(
                $result,
                collect($rubricaInfoPeriodo->orcados)->map(function ($orcadosAtividadesRubrica) use ($rubricaInfoPeriodo) {
                    return [
                        'nomePeriodo'   => $rubricaInfoPeriodo->nomePeriodo,
                        'nomeAtividade' => $orcadosAtividadesRubrica->nomeAtividade,
                        'orcado'        => (double)$orcadosAtividadesRubrica->orcado
                    ];
                })->toArray()
            );
        }, []));


        $atividades = $novaRubricaInfoAtividades->map(function ($rubricaInfoAtividade) use ($idProjeto, $periodos) {
            $nomeAtividade = $rubricaInfoAtividade['nomeAtividade'];
            $nomePeriodo   = $rubricaInfoAtividade['nomePeriodo'];
            $periodoIndex  = $periodos->search(function ($periodo) use ($idProjeto, $nomePeriodo) {
                return ((int)$periodo->projeto_id === (int)$idProjeto) && ($periodo->nome === $nomePeriodo);
            });

            $idPeriodo = $periodos[$periodoIndex]->id;
            return Atividade::where([
                ['is_periodo', '=', false],
                ['projeto_id', '=', $idProjeto],
                ['periodo_id', '=', $idPeriodo],
                ['nome', '=', $nomeAtividade]
            ])->first();
        });

        $novaRubricaInfoAtividades->each(function ($rubInfoAtividade, $i) use ($atividades, $categorias, $nomeNovaRubrica, $codigoNovaRubrica) {
            //pegar a categoria da atividade da collection $atividades, representada pelo "nomeAtividade" e "nomePeríodo"

            $atividade = $atividades[$i];

            $categoria = $categorias->first(function ($categoria) use ($atividade) {
                return $categoria->atividade_id === $atividade->id;
            });

            $novaRubrica               = new Rubrica;
            $novaRubrica->nome         = $nomeNovaRubrica;
            $novaRubrica->codigo       = $codigoNovaRubrica;
            $novaRubrica->categoria_id = $categoria->id;

            $novaRubrica->save();
        });

        return Redirect::to(
            route('projeto.planilha.editar', $idProjeto)
        )->with('message', 'Rubrica adicionada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
