<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Projeto extends Model
{
    use SoftDeletes;

    protected $cascadeDeletes = 'projeto';

    protected $table = 'projeto';

    public function projetos()
    {
        return $this->belongsTo('App\User', 'criador');
    }

    public function obterProjetoInfo()
    {
        $idProjeto = $this->id;

        $atividades = Atividade::where('projeto_id', '=', $idProjeto)
            ->where('is_periodo', '=', false)
            ->select(['nome', 'codigo', 'data_inicio', 'data_fim'])
            ->groupBy('nome')
            ->get();

        $categorias = Categoria::whereHas('atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select('*')->groupBy('nome')->get();

        $rubricas = Rubrica::whereHas('categoria.atividade.projeto', function ($query) use ($idProjeto) {
            $query->where('id', '=', $idProjeto);
        })->select(['nome', 'codigo', 'categoria_id'])->groupBy('nome')->get();

        $res_obj = new \stdClass;
        $res_obj->atividades = $atividades;
        $res_obj->categorias = $categorias;
        $res_obj->rubricas = $rubricas;

        return $res_obj;
    }

    protected $fillable = [
        'nome', 'data_inicio', 'data_fim', 'descricao', 'orcado'
    ];
    public function atividades()
    {
        return $this->hasMany('App\Atividade');
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($projeto) {

            foreach ($projeto->atividades as $atividade => $atividade) { }
        });
    }
}
