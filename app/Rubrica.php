<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubrica extends Model{
	protected $table = 'rubrica';

	public function categoria(){
		return $this->belongsTo('App\Categoria');
	}

	public function registros_planilha(){
		return $this->hasMany('App\RegistroPlanilha');
	}

	protected $fillable = [
        'nome', 'comprometido', 'saldo', 'executado',
    ];
}