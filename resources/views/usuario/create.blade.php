<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IMAZON - Criar novo usuário</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
  <div class="container">
    <img class="imazon" src="/img/logo-imazon.png" alt="Imazon">

    <a href="{{route('logout')}}" class="float-right" title="Logout"><i class="fas fa-sign-out-alt fa-3x"></i></a>
	</div>
	
	<div class="container w-50">
		<h1>Cadastrar usuário</h1>

		@if ($errors->any())
			<div class="alert alert-danger pb-0">
				<ul>
					@foreach ($errors->all() as $error)
						<li class="h5">{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

		<form action="{{ route('usuarios.salvar') }}" method="POST">
			{{ csrf_field() }}

			<div class="form-group">
				<input class="form-control" type="text" name="nome" placeholder="Nome" />
			</div>

			<div class="form-group">
				<input class="form-control" type="email" name="email" placeholder="Email"/>
			</div>

			<div class="form-group">
				<input class="form-control" type="text" name="nome_usuario" placeholder="Nome de usuário"/>
			</div>

			<div class="form-group">
				<input class="form-control" type="password" name="senha" placeholder="Senha" />
			</div>

			<div class="form-group">
				<input class="form-control" type="password" name="senha_confirmation" placeholder="Confirmar senha"/>
			</div>

			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="admin">
				<label class="form-check-label" for="admin">
					Admin
				</label>
			</div>
			
			<div class="row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success btn-block">Cadastrar</button>
				</div>
				<div class="col-sm">
					<a href="{{ route('home') }}" class="btn btn-danger btn-block">Voltar</a>
				</div>
			</div>
			
		</form>

	</div>
</body>