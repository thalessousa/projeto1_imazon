<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Imazon - Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/app.css">
</head>
<body>

	<div class="login-container">
		<img class="login-logo" src="/img/logo-imazon.png" alt="Imazon" />
		<div class="container">
			<h1>Login</h1>
		
			<div class="errors">
				<p>
				@if($errors->any())
				<h4>{{$errors->first()}}</h4>
				@endif
					{{ $errors->first('email') }}
					{{ $errors->first('senha') }}
				</p>
			</div>
		
			<div>
				<form action="/login" method="POST">
		
					{{ csrf_field() }}
		
					<div class="form-group row">
						<label for="email" class="col-sm-2 col-form-label">Email</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="email" placeholder="Email">
						</div>
					</div>

					<div class="form-group row">
						<label for="senha" class="col-sm-2 col-form-label">Senha</label>
						<div class="col-sm-10">
						<input type="password" class="form-control" name="senha" id="senha" placeholder="Senha">
						</div>
					</div>
					{{-- <br><br>
					<label for="senha">Senha: </label>
					<input type="password" name="senha">
					<br><br> --}}
		
					<button class="btn btn-primary btn-block" type="submit">Login</button>
				</form>
			</div>

		</div>
	</div>

</body>
</html>