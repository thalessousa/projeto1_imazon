<!DOCTYPE html>
<html>
<style>
.fa-power-off {
  color: #f43e3e;
  font-size: 3em;
  margin-top: .1em;
  transition: filter ease-in 0.25s;
}
.fa-power-off:hover {
    filter: drop-shadow(0 0 0.15rem #6c585880);
}
thead th {
    background-color: #006DCC;
    color: white;
}

</style>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IMAZON - Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
  <div class="container mt-2">
    <img class="imazon" src="/img/logo-imazon.png" alt="Imazon">

    <a href="{{ route('logout') }}" class="float-right" title="Logout"><i class="fas fa-power-off"></i></a>
  </div>
  <div class="options">

    @if (Auth::user()->admin == 1)
      <div class="option create-user">
        <a href="{{ route('usuarios.criar') }}">
          <i class="far fa-user fa-10x"></i>
          <p>Criar usuário</p>
        </a>
      </div>
    @endif

    <div class="option empty"></div>
    <div class="option add-project">
      <a href="{{ route('projeto.criar') }}">
        <i class="far fa-file fa-10x"></i>
        <p>Novo Projeto</p>
      </a>
    </div>
    <div class="option empty"></div>
    <div class="option edit-project">
      <a href="{{ route('projeto.view') }}">
        <i class="far fa-file fa-10x"></i>
        <p>Visualizar Projetos</p>
      </a>
    </div>

  </div>

</body>
</html>
