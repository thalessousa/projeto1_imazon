<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Criar Rubricas</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">
		<div id="hot"></div>

			<div class="container w-50">
		<h1>Cadastrar de Rubricas</h1>

		@if ($errors->any())
			<div class="alert alert-danger pb-0">
				<ul>
					@foreach ($errors->all() as $error)
						<li class="h5">{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

		<form action="{{ route('rubrica.salvar') }}" method="POST">
			{{ csrf_field() }}

			<div class="form-group">
				<input class="form-control" type="text" name="nome" placeholder="Nome da Rubrica" data-toggle="tooltip" title="Insira o nome para a Rubrica" />
			</div>

			<div class="form-group">
				<input class="submit" type="number" name="comprometido" placeholder="Comprometido" data-toggle="tooltip" title="Insira o valor Comprometido da Rubrica" />
			</div>

			<div class="form-group">
				<input class="submit" type="number" name="executado" placeholder="Executado" data-toggle="tooltip" title="Insira o valor já Executado da Rubrica"/>
			</div>

			<div class="form-group">
				<input class="form-control" type="number" name="saldo" placeholder="Saldo" data-toggle="tooltip" title="Insira o valor de Saldo da Rubrica" />
			</div>

			<div class="row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success btn-block" data-toggle="tooltip" title="Clique para efetuar o cadastro da Rubrica" >Cadastrar</button>
				</div>
				<div class="col-sm">
					<a href="{{ route('home') }}" class="btn btn-danger btn-block"data-toggle="tooltip" title="Clique para voltar">Voltar</a>
				</div>
			</div>
			
		</form>
	</div>
</body>
</html>