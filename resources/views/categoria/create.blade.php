<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Criar Rubricas</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">
		<div id="hot"></div>

			<div class="container w-90">
		<h1 class="mt-2">Cadastro de categorias e rubricas</h1>

        <hr />

		@if ($errors->any())
			<div class="alert alert-danger pb-0">
				<ul>
					@foreach ($errors->all() as $error)
						<li class="h5">{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

		<form id="cadastrarCategorias" action="{{ route('categoria.salvar', ['id'=>$idProjeto]) }}" method="POST">
			@csrf
			<input type="hidden" name="categorias" />

				<div class="forms-categorias">

					<div class="form-categoria" id="categoria-1">
						<h3>Categoria 1</h3>

						<div class="form-row">
							<div class="col">
                                <input required
                                    class="form-control" type="text" name="nome"
                                    placeholder="Nome da categoria" data-toggle="tooltip"
                                    title="Insira o nome da Categoria"
                                    value="{{isset($inputCategorias) && isset($inputCategorias[0]) ? $inputCategorias[0]->nome : ''}}"
                                >
							</div>
						</div>

						<div class="row">
							<div class="col-sm mt-2 mb-3">
								<button type="button" class="btn btn-primary adicionarRubrica" data-toggle="tooltip" title="Clique para adicionar nova rubrica">Adicionar rubrica</button>
								<button type="button" class="btn btn-primary adicionarSubcategoria" data-toggle="tooltip" title="Clique para adicionar nova subcategoria">Adicionar subcategoria</button>
							</div>
						</div>


						<div class="mt-2 mb-2 form-rubrica" id="rubrica-1-1">
							<h5>Rubrica 1</h5>
							<div class="form-row">
                                <div class="col">
                                    <input required type="text" name="nome" placeholder="Nome da rubrica" class="form-control" data-toggle="tooltip" title="Insira o nome da rubrica" >
                                </div>
                                <div class="col">
                                    <input required class="form-control" type="text" name="codigo" placeholder="Código da rubrica" data-toggle="tooltip" title="Insira o código da rubrica" >
                                </div>
							</div>

						</div>
					</div>


				</div>

                <hr />

				<div class="row mt-1 mb-3">
					<div class="col-sm">
						<button type="button" class="btn btn-primary btn-block adicionarCategoria" data-toggle="tooltip" title="Clique para adicionar nova Categoria">Adicionar nova categoria</button>
					</div>
					<div class="col-sm">
						<button type="submit" class="btn btn-success btn-block" data-toggle="tooltip" title="Clique para efetuar o cadastro das categorias e rubricas" >Cadastrar</button>
					</div>
				</div>

		</form>
	</div>

	<script src="/js/app.js"></script>
	<script>

		function criarBotaoFechar(){
			const
				botao = document.createElement('button'),
				botaoDentro = document.createElement('span');

			botao.type = 'button';
			botao.className = 'close float-right';
			botao.setAttribute('aria-hidden','Close');

			botaoDentro.setAttribute('aria-hidden',true);
			botaoDentro.innerHTML = '&times;';
			botao.appendChild(botaoDentro);

			return botao;
		}

		function fecharRubrica(iCategoria,iSubCategoria,iRubrica,e){
			const rubricaElem = iSubCategoria ?
				document.getElementById('rubrica-' + iCategoria + '-' + iSubCategoria + '-' + iRubrica)
				: document.getElementById('rubrica-' + iCategoria + '-' + iRubrica);

			rubricaElem.remove();
		}

		function fecharCategoria(iCategoria,e){
			const categoriaElem = document.getElementById('categoria-' + iCategoria);

			categoriaElem.remove();
		}

		function fecharSubcategoria(iCategoriaMae,iSubcategoria,e){
			const subcategoriaElem = document.getElementById('subcategoria-' + iCategoriaMae + '-' + iSubcategoria);

			subcategoriaElem.remove();
		}

		function getOrcadosElem(nAtividades,classeInputOrcado){
			const container = document.createElement('div');

			container.className = 'form-row mt-2';

			for (let i = 0;i < nAtividades;++i){
				const
					orcadoElem = document.createElement('div'),
					orcadoInput = document.createElement('input');

				orcadoElem.className = 'col';
				orcadoInput.className = 'form-control ' + classeInputOrcado;
				orcadoInput.type = 'number';
				orcadoInput.step = 'any';
				orcadoInput.name = 'orcado';
				orcadoInput.required = true;
				orcadoInput.placeholder = 'Orçado - ' + nomesAtividades[i];

				orcadoElem.appendChild(orcadoInput);
			}

			return container;
		}

		function getCodigosElem(nAtividades,classeInputOrcado){
			const container = document.createElement('div');

			container.className = 'form-row mt-2';

			for (let i = 0;i < nAtividades;++i){
				const
					orcadoElem = document.createElement('div'),
					orcadoInput = document.createElement('input');

				orcadoElem.className = 'col';
				orcadoInput.className = 'form-control ' + classeInputOrcado;
				orcadoInput.type = 'text';
				orcadoInput.name = 'codigo';
				orcadoInput.required = true;
				orcadoInput.placeholder = 'Código - ' + nomesAtividades[i];

				orcadoElem.appendChild(orcadoInput);
				container.appendChild(orcadoElem);
			}

			return container;
		}

		function criarCategoriaElem(iCategoria,onFecharCategoria,isSubcategoria,iCategoriaMae){
			const
				container = document.createElement('div'),
				botaoFecharCategoria = criarBotaoFechar();
				categoriaHeader = document.createElement('h3'),
				nomeCategoriaRow = document.createElement('div'),
				nomeCategoriaElem = document.createElement('div'),
				nomeCategoriaInput = document.createElement('input'),
				btnAddRubricaCol = document.createElement('div'),
				btnAddRubrica = document.createElement('button'),
				btnAddRubricaRow = document.createElement('div'),
				btnAddSubcategoria = document.createElement('button');

			if (isSubcategoria){
				container.className = 'form-subcategoria';
				container.id = 'subcategoria-' + iCategoriaMae + '-' + (iCategoria + 1);
			}
			else{
				container.className = 'form-categoria';
				container.id = 'categoria-' + iCategoria;
			}

			categoriaHeader.className = 'd-inline-block';
			categoriaHeader.innerHTML = isSubcategoria ?
				  'Subcategoria ' + (iCategoria + 1) + ' - Categoria ' + iCategoriaMae
				: 'Categoria ' + iCategoria;

			botaoFecharCategoria.addEventListener('click',onFecharCategoria);

			btnAddRubrica.innerHTML = 'Adicionar rubrica';
			btnAddRubrica.type = 'button';
			btnAddRubrica.className = 'btn btn-primary adicionarRubrica';
			btnAddRubrica.addEventListener('click',handleAddRubrica);
			btnAddRubricaCol.className = 'col-sm mt-2 mb-3';
			btnAddRubricaCol.appendChild(btnAddRubrica);
			btnAddRubricaRow.className = 'row';
			btnAddRubricaRow.appendChild(btnAddRubricaCol);

			btnAddSubcategoria.innerHTML = 'Adicionar subcategoria';
			btnAddSubcategoria.type = 'button';
			btnAddSubcategoria.className = 'btn btn-primary adicionarSubcategoria ml-1';
			btnAddSubcategoria.addEventListener('click',handleAddSubcategoria);
			!isSubcategoria && btnAddRubricaCol.appendChild(btnAddSubcategoria);

			nomeCategoriaInput.type = 'text';
			nomeCategoriaInput.name = 'nome';
			nomeCategoriaInput.placeholder = 'Nome da categoria';
			nomeCategoriaInput.required = true;
			nomeCategoriaInput.className = 'form-control';
			nomeCategoriaElem.className = 'col';
			nomeCategoriaElem.appendChild(nomeCategoriaInput);
			nomeCategoriaRow.className = 'form-row';
			nomeCategoriaRow.appendChild(nomeCategoriaElem);

			container.appendChild(categoriaHeader);
			container.appendChild(botaoFecharCategoria);
			container.appendChild(nomeCategoriaRow);
			container.appendChild(btnAddRubricaRow);

			if (isSubcategoria){
				container.appendChild(criarRubricaElem(iCategoriaMae,1,iCategoria + 1));
			}
			else{
				container.appendChild(criarRubricaElem(iCategoria,1));
			}

			return container;
		}

		function criarRubricaElem(iCategoria,iRubrica,iSubcategoria,onFecharRubrica){
			const
				container = document.createElement('div'),
				rubricaHeader = document.createElement('h5'),
				botaoFecharRubrica = criarBotaoFechar();
				nomeCodigoRubricaRow = document.createElement('div'),
				nomeRubricaElem = document.createElement('div'),
				nomeRubricaInput = document.createElement('input'),
				codigoElem = document.createElement('div'),
				codigoInput = document.createElement('input');

			rubricaHeader.innerHTML = 'Rubrica ' + iRubrica;
			rubricaHeader.className = 'd-inline-block';

			botaoFecharRubrica.addEventListener('click',onFecharRubrica);

			nomeRubricaInput.type = 'text';
			nomeRubricaInput.name = 'nome';
			nomeRubricaInput.placeholder = 'Nome da rubrica';
			nomeRubricaInput.required = true;
			nomeRubricaInput.className = 'form-control';
			nomeRubricaElem.className = 'col';
			nomeRubricaElem.appendChild(nomeRubricaInput);

			codigoInput.type = 'text';
			codigoInput.name = 'codigo';
			codigoInput.placeholder = 'Código da rubrica';
			codigoInput.required = true;
			codigoInput.className = 'form-control';
			codigoElem.className = 'col';
			codigoElem.appendChild(codigoInput);

            nomeCodigoRubricaRow.className = 'form-row';
			nomeCodigoRubricaRow.appendChild(nomeRubricaElem);
			nomeCodigoRubricaRow.appendChild(codigoElem);

            container.className = 'mt-2 mb-2 form-rubrica';
			container.id = iSubcategoria ?
				'rubrica-' + iCategoria + '-' + iSubcategoria + '-' + iRubrica
				: 'rubrica-' + iCategoria + '-' + iRubrica ;
			container.appendChild(rubricaHeader);
			if (onFecharRubrica){
				container.appendChild(botaoFecharRubrica);
			}
			container.appendChild(nomeCodigoRubricaRow);

			return container;
		}

		function getObjCategoriaDeForm(formCategoria){
			const
				nome 	   = formCategoria.querySelector('input[name="nome"]').value,
				formsRubricas   = formCategoria.getElementsByClassName('form-rubrica'),
				rubricas  = [].map.call(formsRubricas,formRubrica => {
					const
						nome 	   = formRubrica.querySelector('input[name="nome"]').value,
						codigo     = formRubrica.querySelector('input[name="codigo"]').value;

					return { nome, codigo };
				}),
				infoIDCategoriaElem = getInfoIDCategoriaElem(formCategoria.id),
				iCategoria = infoIDCategoriaElem.iCategoria;
				iSubCategoria = infoIDCategoriaElem.iSubCategoria,
				isSubCategoria = typeof(iSubCategoria) !== 'undefined';

			return Object.assign(
				{
					nome,
					rubricas
				},
				isSubCategoria && { iCategoriaMae: iCategoria - 1 } //só possui categoria mãe se for subcategoria
			);
		}

		function getCategorias(){
			const
				formsCategorias = document.getElementsByClassName('form-categoria'),
				formsSubCategorias = document.getElementsByClassName('form-subcategoria'),
				subCategorias = [].map.call(formsSubCategorias,getObjCategoriaDeForm);

			return []
				.map.call(formsCategorias,getObjCategoriaDeForm)
				.map((categoria,i) => {
					const
						{
							nome, rubricas
						} = categoria,
						categoriasFilhas = subCategorias.filter(subCat => subCat.iCategoriaMae === i);
					return Object.assign(
						{},{ nome },
						(categoriasFilhas.length > 0) && { subCategorias: categoriasFilhas },
						(categoriasFilhas.length === 0) && { rubricas }
					)
				});
		}

		function getInfoIDCategoriaElem(idCategoriaElem){
			const
				iCategoriaElem = idCategoriaElem.substr(idCategoriaElem.match('-').index + 1).split('-'),
				iCategoria		 	  = parseInt(iCategoriaElem[0],10);
				iSubCategoria		  = iCategoriaElem.length === 2 ?
					parseInt(iCategoriaElem[1],10)
					: undefined;

			return {
				iCategoria,
				iSubCategoria
			};
		}

		function getInfoIDRubricaElem(idRubricaElem){
			const
				iRubricaCategoriaElem = idRubricaElem.substr(idRubricaElem.match('-').index + 1).split('-'),
				iCategoria		 	  = parseInt(iRubricaCategoriaElem[0],10);
				iSubCategoria		  = iRubricaCategoriaElem.length === 3 ?
					parseInt(iRubricaCategoriaElem[1],10)
					: undefined,
				iRubrica			  = iRubricaCategoriaElem.length === 3 ?
					parseInt(iRubricaCategoriaElem[2],10)
					: parseInt(iRubricaCategoriaElem[1],10);

			return {
				iCategoria,
				iSubCategoria,
				iRubrica
			};
		}

		function addRubrica(btnTarget){
			const
				formCategoriaParent = $(btnTarget).closest('.form-categoria')[0],
				formSubCategoriaParent = $(btnTarget).closest('.form-subcategoria')[0],
				headerCategoria = $(formCategoriaParent).children('h3')[0],
				headerSubcategoria = $(formSubCategoriaParent).children('h3')[0],
				iCategoria = parseInt(headerCategoria.innerHTML.substr(headerCategoria.innerHTML.match(/\d/).index),10),
				iSubcategoria = headerSubcategoria ? parseInt(headerSubcategoria.innerHTML.substr(headerSubcategoria.innerHTML.match(/\d/).index),10) : undefined,
				iRubricaAtual = iSubcategoria ? categorias[iCategoria - 1].subcategorias[iSubcategoria - 1]++ : categorias[iCategoria - 1].iRubricas++,
				onFecharRubrica = e => {
					const
						idElemRubricaARemover = e.target.parentElement.parentElement.id,
						rubricaElemInfo = getInfoIDRubricaElem(idElemRubricaARemover),
						iCategoriaRubricaARemover = rubricaElemInfo.iCategoria;
						iSubCategoriaRubricaARemover = rubricaElemInfo.iSubCategoria,
						iRubricaARemover = rubricaElemInfo.iRubrica;

					fecharRubrica(iCategoriaRubricaARemover,iSubCategoriaRubricaARemover,iRubricaARemover,e);

					const
						elemsRubricas = document.getElementsByClassName('form-rubrica');
						rubricasAModificar = [].filter.call(elemsRubricas, elem => {
							const
								id = elem.id,
								rubricaElemInfo = getInfoIDRubricaElem(id),
								iCategoriaElem = rubricaElemInfo.iCategoria,
								iSubCategoriaElem = rubricaElemInfo.iSubCategoria,
								iRubricaElem = rubricaElemInfo.iRubrica;

							return (
								(iCategoriaRubricaARemover == iCategoriaElem) &&
								(iSubCategoriaElem ? (iSubCategoriaRubricaARemover === iSubCategoriaElem) : true) &&
								(iRubricaElem > iRubricaARemover)
							);
						});

						rubricasAModificar.forEach(rubricaElem => {
							const
								id = rubricaElem.id,
								rubricaElemInfo = getInfoIDRubricaElem(id),
								iCategoriaElem = rubricaElemInfo.iCategoria,
								iSubCategoriaElem = rubricaElemInfo.iSubCategoria,
								iRubricaElem = rubricaElemInfo.iRubrica,
								rubricaHeader = rubricaElem.querySelector('h5');

							rubricaElem.id = iSubCategoriaElem ?
								 'rubrica-' + iCategoriaElem + '-' + iSubCategoriaElem + '-' + (iRubricaElem - 1)
								:'rubrica-' + iCategoriaElem + '-' + (iRubricaElem - 1);
							rubricaHeader.innerHTML = 'Rubrica ' + (iRubricaElem - 1);
						});

						iSubCategoriaRubricaARemover ?
						  categorias[iCategoria - 1].subcategorias[iSubCategoriaRubricaARemover - 1]--
						: categorias[iCategoria - 1].iRubricas--;
				},
				novaRubricaElem = criarRubricaElem(
					iCategoria,iRubricaAtual,iSubcategoria,
					(iRubricaAtual === 1) ? undefined : onFecharRubrica
				);

			(formSubCategoriaParent ? formSubCategoriaParent : formCategoriaParent)
			.appendChild(novaRubricaElem);
        }

        function handleAddRubrica(e){
            addRubrica(e.target);
        }

		function handleAddCategoria(){
			const
				iCategoriaAtual = categorias.length + 1,
				novaCategoriaElem = criarCategoriaElem(iCategoriaAtual,e => {
					const
						idElemCategoriaARemover = e.target.parentElement.parentElement.id,
						iCategoriaARemover = idElemCategoriaARemover.substr(idElemCategoriaARemover.match('-').index + 1);

					fecharCategoria(iCategoriaARemover,e);

					const
						elemsCategorias = document.getElementsByClassName('form-categoria');
						elemsRubricas = document.getElementsByClassName('form-rubrica');
						categoriasAModificar = [].filter.call(elemsCategorias, elem => {
							const
								id = elem.id,
								iCategoriaElem = id.substr(id.match('-').index + 1);

							return iCategoriaElem > iCategoriaARemover;
						}),
						rubricasAModificar = [].filter.call(elemsRubricas, elem => {
							const
								id = elem.id,
								iCategoriaElem = id.substr(id.match('-').index + 1);

							return iCategoriaElem > iCategoriaARemover;
						});

					categoriasAModificar.forEach(categoriaElem => {
						const
							id = categoriaElem.id,
							iCategoriaElem = id.substr(id.match('-').index + 1),
							categoriaHeader = categoriaElem.querySelector('h3');

						categoriaElem.id = 'categoria-' + (iCategoriaElem - 1);
						categoriaHeader.innerHTML = 'Categoria ' + (iCategoriaElem - 1);
					});

					rubricasAModificar.forEach(rubricaElem => {
						const
							id = rubricaElem.id,
							iCategoriaRubricaElem = id.substr(id.match('-').index + 1).split('-'),
							iCategoriaElem = parseInt(iCategoriaRubricaElem[0],10),
							iRubricaElem = parseInt(iCategoriaRubricaElem[1],10);

						rubricaElem.id = 'rubrica-' + (iCategoriaElem - 1) + '-' + iRubricaElem;
					});

					categorias = categorias.slice(0,iCategoriaARemover - 1).concat(categorias.slice(iCategoriaARemover,categorias.length));
				});

			document.querySelector('.forms-categorias').appendChild(novaCategoriaElem);
			categorias.push({
				iRubricas: 2,
				subcategorias: []
			});
        }

        function handleAddSubcategoria(e){
            addSubCategoria(e.target);
        }

		function addSubCategoria(btnTarget){
			const
				elemCategoriaMae = btnTarget.parentNode.parentNode.parentNode,
				idElemCategoriaMae = elemCategoriaMae.id,
				elemRubricas = [].filter.call(elemCategoriaMae.children,elem => elem.className.indexOf('form-rubrica') !== -1),
				elemBtnAddRubrica = elemCategoriaMae.querySelector('.adicionarRubrica'),
                iCategoriaMae = parseInt(idElemCategoriaMae.substr(idElemCategoriaMae.match(/\d/).index),10),
			    novaCategoriaElem = criarCategoriaElem(categorias[iCategoriaMae - 1].subcategorias.length,e => {
					const
						idElemCategoriaARemover = e.target.parentElement.parentElement.id,
						idElemCategoriaARemoverInfo = idElemCategoriaARemover.substr(idElemCategoriaARemover.match('-').index + 1).split('-'),
						iCategoriaMae = idElemCategoriaARemoverInfo[0],
						iSubCategoriaARemover = idElemCategoriaARemoverInfo[1],
						btnAddRubrica = elemCategoriaMae.querySelector('.adicionarRubrica');

					fecharSubcategoria(iCategoriaMae,iSubCategoriaARemover,e);

					const
						elemsSubCategorias = document.getElementsByClassName('form-subcategoria');
						elemsRubricas = document.getElementsByClassName('form-rubrica'),
						filtrarCategoriasCb = elem => {
							const
								id = elem.id,
								iSubCategoriaElem = id.substr(id.match('-').index + 1).split('-')[1];

							return iSubCategoriaElem > iSubCategoriaARemover;
						},
						subCategoriasAModificar = [].filter.call(elemsSubCategorias, filtrarCategoriasCb),
						rubricasAModificar = [].filter.call(elemsRubricas,filtrarCategoriasCb);

					subCategoriasAModificar.forEach(subCategoriaElem => {
						const
							id = subCategoriaElem.id,
							iSubCategoriaElem = id.substr(id.match('-').index + 1).split('-')[1];
							subCategoriaHeader = subCategoriaElem.querySelector('h3');

						subCategoriaElem.id = 'subcategoria-' + iCategoriaMae + '-' + (iSubCategoriaElem - 1);
						subCategoriaHeader.innerHTML = 'Subcategoria ' + (iSubCategoriaElem - 1) + ' - Categoria ' + iCategoriaMae;
					});

					rubricasAModificar.forEach(rubricaElem => {
						const
							id = rubricaElem.id,
							iCategoriaRubricaElem = id.substr(id.match('-').index + 1).split('-'),
							iCategoriaElem = parseInt(iCategoriaRubricaElem[0],10),
							iRubricaElem = parseInt(iCategoriaRubricaElem[1],10);

						rubricaElem.id = 'rubrica-' + (iCategoriaElem - 1) + '-' + iRubricaElem;
					});

					const subcategorias = categorias[iCategoriaMae - 1].subcategorias;
					categorias[iCategoriaMae - 1].subcategorias = subcategorias.slice(0,iSubCategoriaARemover - 1).concat(subcategorias.slice(iSubCategoriaARemover,subcategorias.length));

					if (categorias[iCategoriaMae - 1].subcategorias.length === 0){
						btnAddRubrica.disabled = false;
					}
				},true,iCategoriaMae);


			if (elemRubricas.length > 0){
				elemRubricas.forEach(elem => elem.remove());
				categorias[iCategoriaMae - 1].iRubricas = 1;
			}
			elemCategoriaMae.appendChild(novaCategoriaElem);
			elemBtnAddRubrica.disabled = true;
			categorias[iCategoriaMae - 1].subcategorias.push(2);
        }

        function carregarInputRubricaSubCategoria(iCategoria,iSubCategoria,iRubrica,inputRubrica){
            const rubricaElem         = document.querySelector(`#rubrica-${iCategoria}-${iSubCategoria}-${iRubrica}`);
            const nomeRubricaElem     = rubricaElem.querySelector('input[name="nome"]');
            const codigoRubricaElem   = rubricaElem.querySelector('input[name="codigo"]');

            nomeRubricaElem.value   = inputRubrica.nome;
            codigoRubricaElem.value = inputRubrica.codigo;

        }

        function carregarInputRubrica(iCategoria,iRubrica,inputRubrica){
            const rubricaElem         = document.querySelector(`#rubrica-${iCategoria}-${iRubrica}`);
            const nomeRubricaElem     = rubricaElem.querySelector('input[name="nome"]');
            const codigoRubricaElem   = rubricaElem.querySelector('input[name="codigo"]');

            nomeRubricaElem.value   = inputRubrica.nome;
            codigoRubricaElem.value = inputRubrica.codigo;

        }

        function carregarInputCategoria(iCategoria,inputCategoria){
            const categoriaElem         = document.querySelector(`#categoria-${iCategoria}`);
            const nomeCategoriaElem     = categoriaElem.querySelector('input[name="nome"]');

            nomeCategoriaElem.value   = inputCategoria.nome;

        }

        function carregarInputSubCategoria(iCategoria,iSubCategoria,inputSubCategoria){
            const subCategoriaElem         = document.querySelector(`#subcategoria-${iCategoria}-${iSubCategoria}`);
            const nomeSubCategoriaElem     = subCategoriaElem.querySelector('input[name="nome"]');

            nomeSubCategoriaElem.value = inputSubCategoria.nome;
        }

		const nAtividades = {{ $nAtividadesProjeto }};
		const nomesAtividades = {!! json_encode($atividades->map(function($atividade){
			return "$atividade->nomePeriodo - $atividade->nome";
		})) !!};
		let categorias = [{
			iRubricas: 2,
			subcategorias: []
		}];

		document.querySelector('.adicionarCategoria').addEventListener('click',handleAddCategoria);

		document.querySelector('.adicionarRubrica').addEventListener('click',handleAddRubrica);

		document.querySelector('.adicionarSubcategoria').addEventListener('click',handleAddSubcategoria);

		document.querySelector('#cadastrarCategorias').addEventListener('submit',e => {
			e.preventDefault();

			const
				categorias = getCategorias(),
				form = e.target,
				inputJSON = form.querySelector('input[name="categorias"]');

			inputJSON.value = JSON.stringify(categorias);
			form.submit();
        });

        //=================================================================
        const inputCategorias = {!! isset($inputCategorias) ? $inputCategorias->toJson() : 'undefined' !!};

        if (inputCategorias){
            const [primeiraCategoria,...outrosInputsCats] = inputCategorias;
            const {
                rubricas: inputRubricasPrimeiraCategoria,
                subCategorias: inputSubCategoriasPrimeiraCategoria
            } = primeiraCategoria;

            if (inputSubCategoriasPrimeiraCategoria){
                const btnAddSubCategoria = document.querySelector(`#categoria-1 .adicionarSubcategoria`);

                inputSubCategoriasPrimeiraCategoria.forEach((inputSubCat,iSubCat) => {
                    const { rubricas } = inputSubCat;
                    addSubCategoria(btnAddSubCategoria);
                    carregarInputSubCategoria(1,iSubCat + 1,inputSubCat);

                    const [primeiraRubrica,...outrosInputsRubs] = rubricas;

                    // Carrega inputs e rubricas de primeira categoria:
                    //==========================================================================================
                    carregarInputRubricaSubCategoria(1,iSubCat + 1,1,primeiraRubrica);

                    outrosInputsRubs.forEach((inputRubrica,i) => {
                        const btnAddRubrica = document.querySelector(`#subcategoria-1-${iSubCat + 1} .adicionarRubrica`);
                        addRubrica(btnAddRubrica);
                        carregarInputRubricaSubCategoria(1,iSubCat + 1,i + 2,inputRubrica);
                    });
                });
            }

            if (inputRubricasPrimeiraCategoria){
                const [primeiraRubrica,...outrosInputsRubs] = inputRubricasPrimeiraCategoria;

                // Carrega inputs e rubricas de primeira categoria:
                //==========================================================================================
                carregarInputRubrica(1,1,primeiraRubrica);

                outrosInputsRubs.forEach((inputRubrica,i) => {
                    const btnAddRubrica = document.querySelector('#categoria-1 .adicionarRubrica');
                    addRubrica(btnAddRubrica);
                    carregarInputRubrica(1,i + 2,inputRubrica);
                });
            }
            //==========================================================================================

            outrosInputsCats.forEach((inputCat,iCategoria) => {
                const { rubricas, subCategorias: inputSubCategorias } = inputCat;

                handleAddCategoria();
                carregarInputCategoria(iCategoria + 2,inputCat);

                if (inputSubCategorias){
                    const btnAddSubCategoria = document.querySelector(`#categoria-${iCategoria + 2} .adicionarSubcategoria`);

                    inputSubCategorias.forEach((inputSubCat,iSubCategoria) => {
                        addSubCategoria(btnAddSubCategoria);
                        carregarInputSubCategoria(iCategoria + 2,iSubCategoria + 1,inputSubCat);
                        const { rubricas } = inputSubCat;
                        const [primeiraRubrica,...outrosInputsRubs] = rubricas;

                        carregarInputRubricaSubCategoria(iCategoria + 2,iSubCategoria + 1,1,primeiraRubrica);

                        const btnAddRubrica = document.querySelector(`#subcategoria-${iCategoria + 2}-${iSubCategoria + 1} .adicionarRubrica`);
                        outrosInputsRubs.forEach((rub,iRubrica) => {
                            addRubrica(btnAddRubrica);
                            carregarInputRubricaSubCategoria(iCategoria + 2,iSubCategoria + 1,iRubrica + 2,rub);
                        });
                    });
                }

                if (rubricas){
                    const [primeiraRubrica,...outrosInputsRubs] = rubricas;

                    carregarInputRubrica(iCategoria + 2,1,primeiraRubrica);

                    const btnAddRubrica = document.querySelector(`#categoria-${iCategoria + 2} .adicionarRubrica`);
                    outrosInputsRubs.forEach((rub,iRubrica) => {
                        addRubrica(btnAddRubrica);
                        carregarInputRubrica(iCategoria + 2,iRubrica + 2,rub);
                    });
                }

            });
        }

        //=================================================================

	</script>
</body>
</html>
