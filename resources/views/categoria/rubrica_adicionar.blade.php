<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Adicionar rubrica</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">

		<div class="container w-75">
        <h2>Cadastrar Nova Rubrica - Projeto {{ $projeto->nome }}</h2>

			@if (count($errors->get('categoria')) > 0)
				<div class="alert alert-danger pb-0">
					<ul>
						@foreach ($errors->all() as $error)
							<li class="h5">{{ $error }}</li>
						@endforeach
					</ul>
                </div>
            @else
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger pb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="h5">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h2>Categoria - {{ $categoria->nome }}</h2>

                <br />

                <form action="{{ route('categoria.rubrica.salvar.adicionar', ['id'=>$idProjeto,'nomeCategoria' => $categoria->nome]) }}" method="POST" id="adicionarRubrica">
                    @csrf

                    <input type="hidden" name="rubrica" />

                    <div class="form-rubrica" id="rubrica-1">
                        <h4 class="d-inline-block">Nova rubrica</h4>
                        <hr />

                        <div class="form-row mb-3">
                            <label for="nome" class="col-sm-2 col-form-label">Nome: </label>
                            <div class="col-sm-4">
                                <input required class="form-control" type="text" name="nome" placeholder="Nome da Rubrica" data-toggle="tooltip" title="Insira o Nome da Rubrica" value="{{ isset($nomeRubrica) ? $nomeRubrica : ''}}"/>
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <label for="codigo" class="col-sm-2 col-form-label">Código: </label>
                            <div class="col-sm-4">
                                <input required class="form-control" type="text" name="codigo" placeholder="Código da Rubrica" data-toggle="tooltip" title="Insira o Nome da Rubrica" value="{{ isset($codigoRubrica) ? $codigoRubrica : ''}}"/>
                            </div>
                        </div>

                        <hr />
                        <h6>Orçados da rubrica para cada atividade:</h6>
                        @foreach ($periodos as $periodo)
                            <div class="row periodo-info">
                                <input type="hidden" name="nome-periodo" value="{{ $periodo->nome }}"/>
                                @foreach ($atividades as $atividade)
                                    <div class="col mb-2 form-row atividade-info">
                                        <input type="hidden" name="nome-atividade" value="{{ $atividade->nome }}"/>
                                        <input
                                            required class="form-control" type="number" step="any" name="orcado-rub"
                                            placeholder="Orçado - {{ $periodo->nome }} - {{ $atividade->nome }}"
                                            data-toggle="tooltip" title="Insira o orçado para a atividade"
                                        />
                                    </div>
                                @endforeach
                            </div>
                        @endforeach

                    </div>

                    <hr />

                    <div class="row mt-2 mb-3">
                        <div class="col-sm">
                            <button type="submit" class="btn btn-success btn-block"data-toggle="tooltip" title="Clique para efetuar o cadastro do período" >Cadastrar</button>
                        </div>
                    </div>

                </form>

			@endif


		</div>

    </div>

    <script>
        const periodos = {!! $periodos->toJson() !!};

        function getRubricaInfo(){
            const elemsPeriodoInfo = document.getElementsByClassName('periodo-info');
            const nomeRubrica      = document.querySelector('input[name="nome"]').value;
            const codigoRubrica    = document.querySelector('input[name="codigo"]').value;

            //informação do período
            const infoRubricaPeriodos  = [].map.call(elemsPeriodoInfo,elem => {
                const nomePeriodoElem  = elem.querySelector('input[name="nome-periodo"]');
                const elemsAtvInfo     = elem.getElementsByClassName('atividade-info');

                return {
                    nomePeriodo: nomePeriodoElem.value,
                    orcados: [].map.call(elemsAtvInfo,(elem => ({ //informação da atividade
                        nomeAtividade : elem.querySelector('input[name="nome-atividade"]').value,
                        orcado        : elem.querySelector('input[name="orcado-rub"]').value
                    })))
                }
            });

            return {
                nome    : nomeRubrica,
                codigo  : codigoRubrica,
                orcados : infoRubricaPeriodos
            }
        }

		document.querySelector('#adicionarRubrica').addEventListener('submit',e => {
            e.preventDefault();

            const
				infoRubrica = getRubricaInfo(),
				form = e.target,
				inputJSON = form.querySelector('input[name="rubrica"]');

			inputJSON.value = JSON.stringify(infoRubrica);
			form.submit();
        });
    </script>
</body>
</html>
