<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Adicionar atividade</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">

		<div class="container w-75">
        <h2>Cadastrar Nova Atividade - Projeto {{ $projeto->nome }}</h2>

			@if ($errors->any())
				<div class="alert alert-danger pb-0">
					<ul>
						@foreach ($errors->all() as $error)
							<li class="h5">{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (session('message'))
				<div class="alert alert-success">
					{{ session('message') }}
				</div>
            @endif

            <br />
                <hr />

			<form action="{{ route('atividade.salvar.adicionar', ['id'=>$idProjeto]) }}" method="POST" id="adicionarAtividade">
				@csrf

				<input type="hidden" name="atividade" />

                <div class="form-periodo" id="periodo-1">
                    <h4 class="d-inline-block">Nova atividade</h4>

                    <div class="form-row mb-3">
                        <div class="col">
                            <input required class="form-control" type="text" name="nome" placeholder="Nome da Atividade" data-toggle="tooltip" title="Insira o Nome da Atividade" />
                        </div>
                        <div class="col">
                            <input required class="form-control" type="text" step="any" name="codigo" placeholder="Código" data-toggle="tooltip" title="Insira o código da atividade" >
                        </div>
                    </div>

                    <div class="form-row mb-3">
                        @foreach ($periodos as $periodo)
                            <div class="col-sm-3">
                                <input required class="form-control" type="number" step="any" name="orcado" placeholder="Orçado - {{ $periodo->nome }}" data-toggle="tooltip" title="Insira o orçado para a atividade" >
                            </div>
                        @endforeach
                    </div>

                </div>

                <hr />
                <br />

                <div>
                    <h4>Adicione os valores de orçado e código de cada período para as categorias/rubricas referente à nova atividade: </h4>
                    @foreach ($categorias as $categoria)
                        <hr/>
                        <div class="col-xs-4 mt-3 mb-3">
                            <h5>Categoria - {{ $categoria->nome }}<h5>
                            <div class="row form-row">
                                @foreach ($periodos as $periodo)
                                    <div class="col-xs-4 mt-2 mb-2">
                                        <input
                                            required
                                            class="form-control"
                                            type="number" step="any"
                                            name="orcadoCat" placeholder="Orçado - {{ $periodo->nome }}"
                                            data-toggle="tooltip" title="Insira o orçado para o período"
                                        >
                                    </div>
                                @endforeach
                            </div>
                            <hr/>
                            <h5>Rubricas</h5>
                            <br />
                            @foreach ($categoria->rubricas as $rubrica)
                                <div>
                                    <h6>{{ $rubrica->nome }}<h6>
                                    <div class="row form-row">
                                        @foreach ($periodos as $periodo)
                                            <div class="col-xs-6 mt-2">
                                                <div class="row form-row">
                                                    <div class="col-xs-4 mt-2">
                                                        <input
                                                            required
                                                            class="form-control"
                                                            type="number" step="any"
                                                            name="orcadoRub" placeholder="Orçado - {{ $periodo->nome }}"
                                                            data-toggle="tooltip" title="Insira o orçado para a rubrica"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>

                <br />
				<div class="row mt-2 mb-3">
					<div class="col-sm">
						<button type="submit" class="btn btn-success btn-block"data-toggle="tooltip" title="Clique para efetuar o cadastro do período" >Cadastrar</button>
					</div>
				</div>

			</form>

		</div>

    </div>

    <script>
        const periodos = {!! $periodos->toJson() !!};
        const categorias = {!! $categorias->toJson() !!};
        const rubricas   = {!! $rubricas->toJson()   !!};

        console.log(periodos,categorias,rubricas);

        function getAtividadeInfo(periodos, categorias){
            const nomeAtividade       = document.querySelector('input[name="nome"]').value;
            const codigoAtividade     = document.querySelector('input[name="codigo"]').value;
            const orcadosAtividade    = document.querySelectorAll('input[name="orcado"]');
            const orcadosCategorias   = document.querySelectorAll('input[name="orcadoCat"]');
            const nOrcadosCategorias  = orcadosCategorias.length;
            const orcadosRub          = document.querySelectorAll('input[name="orcadoRub"]');
            const codigosRub          = document.querySelectorAll('input[name="codigoRub"]');
            const nPeriodos           = periodos.length;
            const nCategorias         = categorias.length;

            let iRubricaGeral = 0;
            const categoriasComRubricasEOrcados = categorias.map((categoria,iCategoria) => {
                const { nome, rubricas } = categoria;
                const nRubricas = rubricas.length;
                const iOrcadoCategoriaInicio = iCategoria * nPeriodos;

                const orcados = Array(nPeriodos).fill(false).map(
                    (_,i) => orcadosCategorias[iOrcadoCategoriaInicio + i].value
                );

                const rubricasComOrcados = rubricas.map((rubrica,iRubrica) => {
                    const { nome,codigo } = rubrica;
                    const iOrcadoRubricaInicio = iCategoria + (nPeriodos * iRubricaGeral++ - iCategoria);

                    const orcados = Array(nPeriodos).fill(false).map(
                        (_,iPeriodo) => orcadosRub[iOrcadoRubricaInicio + iPeriodo].value
                    );

                    return { nome, codigo, orcados };
                });

                return { nome, orcados, rubricas: rubricasComOrcados };
            });

            return {
                nome       : nomeAtividade,
                codigo     : codigoAtividade,
                orcado     : [].map.call(orcadosAtividade,elem => parseFloat(elem.value)),
                categorias : categoriasComRubricasEOrcados
            };
        }

		document.querySelector('#adicionarAtividade').addEventListener('submit',e => {
            e.preventDefault();

            const
				infoAtividade = getAtividadeInfo(periodos,categorias),
				form = e.target,
				inputJSON = form.querySelector('input[name="atividade"]');

			inputJSON.value = JSON.stringify(infoAtividade);
			form.submit();
        });
    </script>
</body>
</html>
