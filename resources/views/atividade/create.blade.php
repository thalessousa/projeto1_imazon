<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Criar atividades</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">

		<div class="container w-50">
            <h1 class="mt-2">Cadastrar Atividades</h1>
            <hr/>

			@if ($errors->any())
				<div class="alert alert-danger pb-0">
					<ul>
						@foreach ($errors->all() as $error)
							<li class="h5">{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (session('message'))
				<div class="alert alert-success">
					{{ session('message') }}
				</div>
			@endif

			<form action="{{ route('atividade.salvar', ['id'=>$idProjeto]) }}" method="POST" id="cadastrarAtividades">
				@csrf

				<input type="hidden" name="atividades" />

				<div class="forms-atividades">
					<div class="form-atividade" id="atividade-1">
						<h3 class="d-inline-block">Atividade 1</h3>

						<div class="form-row mb-3">
							<div class="col">
								<input required class="form-control" type="text" name="codigo" placeholder="Código" data-toggle="tooltip" title="Insira o Código da Atividade" />
							</div>
							<div class="col">
								<input required class="form-control" type="text" name="nome" placeholder="Nome da Atividade" data-toggle="tooltip" title="Insira o Nome da Atividade" />
							</div>
						</div>


					</div>
				</div>

                <hr />

				<div class="row mt-2 mb-3">
					<div class="col-sm">
						<button type="button" class="btn btn-primary btn-block adicionarAtividade" data-toggle="tooltip" title="Clique para adicionar uma nova Atividade" >Adicionar nova atividade</button>
					</div>
					<div class="col-sm">
						<button type="submit" class="btn btn-success btn-block"data-toggle="tooltip" title="Clique para efetuar o cadastro da Atividade" >Cadastrar</button>
					</div>
				</div>

			</form>

		</div>

	</div>

	<script>

		const nPeriodos = {{ $periodos->count() }};
		const nomesPeriodos = {!! json_encode($periodos->get()->map(function($periodo){ return $periodo->nome; })) !!};

		function criarFormGroup(){
			const elem = document.createElement('div');
			elem.className = 'form-group';

			return elem;
		}

		function criarBotaoFecharAtividade(){
			const
				botao = document.createElement('button'),
				botaoDentro = document.createElement('span');

			botao.type = 'button';
			botao.className = 'close float-right';
			botao.setAttribute('aria-hidden','Close');

			botaoDentro.setAttribute('aria-hidden',true);
			botaoDentro.innerHTML = '&times;';
			botao.appendChild(botaoDentro);

			return botao;
		}

		function fecharAtividade(iAtividade,e){
			const atividadeElem = document.getElementById('atividade-' + iAtividade);

			atividadeElem.remove();
		}

		function criarAtividadeElem(iAtividade,nPeriodos,onFecharAtividade){
			const
				container = document.createElement('div'),
				formRowNomeEOrcado = document.createElement('div'),
				formRowDatas = document.createElement('div'),
				colCodigo = document.createElement('div'),
				colNome = document.createElement('div'),
				colOrcado = document.createElement('div'),
				colDataInicio = document.createElement('div'),
				colDataFim = document.createElement('div'),
				labelDataInicio = document.createElement('label'),
				labelDataFim = document.createElement('label'),
				atividadeHeader = document.createElement('h3'),
				botaoFecharAtividade = criarBotaoFecharAtividade();
				nomeDaAtividadeElem = criarFormGroup(),
				nomeDaAtividadeInput = document.createElement('input'),
				codigoDaAtividadeElem = criarFormGroup(),
				codigoDaAtividadeInput = document.createElement('input');

			formRowNomeEOrcado.className = 'form-row';
			formRowDatas.className = 'form-row mb-4';
			colCodigo.className = 'col';
			colNome.className = 'col';
			colOrcado.className = 'col';
			colDataInicio.className = 'col';
			colDataFim.className = 'col';
			atividadeHeader.className = 'd-inline-block';
			atividadeHeader.innerHTML = 'Atividade ' + iAtividade;

			botaoFecharAtividade.addEventListener('click',onFecharAtividade)

			labelDataInicio.innerHTML = 'Data de início: ';
			labelDataFim.innerHTML = 'Data de fim: ';

			codigoDaAtividadeInput.className = 'form-control';
			codigoDaAtividadeInput.type = 'text';
			codigoDaAtividadeInput.name = 'codigo';
			codigoDaAtividadeInput.required = true;
			codigoDaAtividadeInput.placeholder = 'Código';
			codigoDaAtividadeElem.appendChild(codigoDaAtividadeInput);

			nomeDaAtividadeInput.className = 'form-control';
			nomeDaAtividadeInput.type = 'text';
			nomeDaAtividadeInput.name = 'nome';
			nomeDaAtividadeInput.required = true;
			nomeDaAtividadeInput.placeholder = 'Nome da Atividade';
			nomeDaAtividadeElem.appendChild(nomeDaAtividadeInput);

			colCodigo.appendChild(codigoDaAtividadeElem);
			colNome.appendChild(nomeDaAtividadeElem);

			formRowNomeEOrcado.appendChild(colCodigo);
			formRowNomeEOrcado.appendChild(colNome);

			container.className = 'form-atividade';
			container.id = 'atividade-' + iAtividade;
			container.appendChild(atividadeHeader);
			container.appendChild(botaoFecharAtividade);
			container.appendChild(formRowNomeEOrcado);

			return container;
		}

		function getAtividades(){
			const formsAtividades = document.getElementsByClassName('form-atividade');

			return [].map.call(formsAtividades,form => {
				const
					nome 	   = form.querySelector('input[name="nome"]').value,
					codigo 	   = form.querySelector('input[name="codigo"]').value;

				return {
					nome,
					codigo,
				};
			});
		}

		let iAtividade = 2;

		document.querySelector('.adicionarAtividade').addEventListener('click',() => {
			const
				iAtividadeAtual   = iAtividade++,
				novaAtividadeElem = criarAtividadeElem(iAtividadeAtual,nPeriodos,e => {
					const
						idElemAtividadeARemover = e.target.parentElement.id,
						iAtividadeARemover = idElemAtividadeARemover.substr(idElemAtividadeARemover.match('-').index + 1);

					fecharAtividade(iAtividadeARemover,e);

					const
						elemsAtividades = document.getElementsByClassName('form-atividade');
						atividadesAModificar = [].filter.call(elemsAtividades, elem => {
							const
								id = elem.id,
								iAtividadeElem = id.substr(id.match('-').index + 1);

							return iAtividadeElem > iAtividadeARemover;
						});

					atividadesAModificar.forEach(atividadeElem => {
						const
							id = atividadeElem.id,
							iAtividadeElem = id.substr(id.match('-').index + 1),
							atividadeHeader = atividadeElem.querySelector('h3');

						atividadeElem.id = 'atividade-' + (iAtividadeElem - 1);
						atividadeHeader.innerHTML = 'Atividade ' + (iAtividadeElem - 1);
					});

					iAtividade--;
				});

			document.querySelector('.forms-atividades').appendChild(novaAtividadeElem);
		});

		document.querySelector('#cadastrarAtividades').addEventListener('submit',e => {
			e.preventDefault();

			const
				atividades = getAtividades(),
				form = e.target,
				inputJSON = form.querySelector('input[name="atividades"]');

			inputJSON.value = JSON.stringify(atividades);
			form.submit();
		});
	</script>
</body>
</html>
