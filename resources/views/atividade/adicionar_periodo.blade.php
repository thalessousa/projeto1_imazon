<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Adicionar período</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">

		<div class="container w-75">
        <h2>Cadastrar Novo Período - Projeto {{ $projeto->nome }}</h2>

			@if ($errors->any())
				<div class="alert alert-danger pb-0">
					<ul>
						@foreach ($errors->all() as $error)
							<li class="h5">{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (session('message'))
				<div class="alert alert-success">
					{{ session('message') }}
				</div>
            @endif

            <br />
                <hr />

			<form action="{{ route('periodo.salvar.adicionar', ['id'=>$idProjeto]) }}" method="POST" id="adicionarPeriodo">
				@csrf

				<input type="hidden" name="periodo" />

                <div class="form-periodo" id="periodo-1">
                    <h4 class="d-inline-block">Novo período</h4>

                    <div class="form-row mb-3">
                        <div class="col">
                            <input required class="form-control" type="text" name="nome" placeholder="Nome do Período" data-toggle="tooltip" title="Insira o Nome da Atividade" />
                        </div>
                        <div class="col">
                            <input required class="form-control orcado-periodo" type="number" step="any" name="orcado" placeholder="Orçado" data-toggle="tooltip" title="Insira o orçado para o periodo" >
                        </div>
                    </div>

                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="data_inicio">Data de início: </label>
                            <input required class="form-control" type="date" name="data_inicio" placeholder="Data de início" title="Insira a data de início do período">
                        </div>
                        <div class="col">
                            <label for="data_fim">Data de fim: </label>
                            <input required class="form-control" type="date" name="data_fim" placeholder="Data de fim" title="Insira a data de fim do período">
                        </div>
                    </div>

                </div>
                <hr />

                <br />

                <div>
                    <h4>Adicione os valores de orçado e código de cada atividade do novo período: </h4>
                    <div class="row form-row">
                        @foreach ($atividades as $atividade)
                            <div class="col-xs-4 mt-2 mb-2">
                                <input
                                    required
                                    class="form-control"
                                    type="number" step="any"
                                    name="orcadoAtv" placeholder="Orçado - {{ $atividade->nome }}"
                                    data-toggle="tooltip" title="Insira o orçado para a atividade"
                                >
                            </div>
                            <div class="col-xs-4 mt-2 mb-2">
                                <input
                                    required
                                    class="form-control"
                                    type="text" step="any"
                                    name="codigoAtv" placeholder="Código - {{ $atividade->nome }}"
                                    data-toggle="tooltip" title="Insira o código para a atividade"
                                >
                            </div>
                        @endforeach
                    </div>
                </div>

                <hr />
                <br />

                <div>
                    <h4>Adicione os valores de orçado de cada atividade para as categorias/rubricas referente ao novo período: </h4>
                    @foreach ($categorias as $categoria)
                        <hr/>
                        <div class="col-xs-4 mt-3 mb-3">
                            <h5>Categoria - {{ $categoria->nome }}<h5>
                            <div class="row form-row">
                                @foreach ($atividades as $atividade)
                                    <div class="col-xs-4 mt-2 mb-2">
                                        <input
                                            required
                                            class="form-control"
                                            type="number" step="any"
                                            name="orcadoCat" placeholder="Orçado - {{ $atividade->nome }}"
                                            data-toggle="tooltip" title="Insira o orçado para a atividade"
                                        >
                                    </div>
                                @endforeach
                            </div>
                            <hr/>
                            <h5>Rubricas</h5>
                            <br />
                            @foreach ($categoria->rubricas as $rubrica)
                                <div>
                                    <h6>{{ $rubrica->nome }}<h6>
                                    <div class="row form-row">
                                        @foreach ($atividades as $atividade)
                                            <div class="col-xs-4 mt-2">
                                                <input
                                                    required
                                                    class="form-control"
                                                    type="number" step="any"
                                                    name="orcadoRub" placeholder="Orçado - {{ $atividade->nome }}"
                                                    data-toggle="tooltip" title="Insira o orçado para a rubrica"
                                                >
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>

                <br />
				<div class="row mt-2 mb-3">
					<div class="col-sm">
						<button type="submit" class="btn btn-success btn-block"data-toggle="tooltip" title="Clique para efetuar o cadastro do período" >Cadastrar</button>
					</div>
				</div>

			</form>

		</div>

    </div>

    <script>
        const atividades = {!! $atividades->toJson() !!};
        const categorias = {!! $categorias->toJson() !!};
        const rubricas   = {!! $rubricas->toJson()   !!};

        console.log(atividades,categorias,rubricas);

        function getPeriodoInfo(atividades, categorias){
            const nomePeriodo        = document.querySelector('input[name="nome"]').value;
            const orcadoPeriodo      = document.querySelector('input[name="orcado"]').value;
            const dataInicioPeriodo  = document.querySelector('input[name="data_inicio"]').value;
            const dataFimPeriodo     = document.querySelector('input[name="data_fim"]').value;
            const orcadosAtividades  = document.querySelectorAll('input[name="orcadoAtv"]');
            const codigosAtividades  = document.querySelectorAll('input[name="codigoAtv"]');
            const orcadosCategorias  = document.querySelectorAll('input[name="orcadoCat"]');
            const nOrcadosCategorias = orcadosCategorias.length;
            const orcadosRub         = document.querySelectorAll('input[name="orcadoRub"]');
            const codigosRub         = document.querySelectorAll('input[name="codigoRub"]');
            const nAtividades        = atividades.length;
            const nCategorias        = categorias.length;

            const atividadesComOrcados = atividades.map((atv,i) => {
                const { nome, codigo, data_inicio, data_fim } = atv;

                return {
                    nome, codigo,
                    dataInicio: data_inicio,
                    dataFim: data_fim,
                    orcado: orcadosAtividades[i].value,
                    codigo: codigosAtividades[i].value,
                };
            });

            let iRubricaGeral = 0;
            const categoriasComRubricasEOrcados = categorias.map((categoria,iCategoria) => {
                const { nome, rubricas } = categoria;
                const nRubricas = rubricas.length;
                const iOrcadoCategoriaInicio = iCategoria * nAtividades;

                const orcados = Array(nAtividades).fill(false).map(
                    (_,i) => orcadosCategorias[iOrcadoCategoriaInicio + i].value
                );

                const rubricasComOrcados = rubricas.map((rubrica,iRubrica) => {
                    const { nome,codigo } = rubrica;
                    const iOrcadoRubricaInicio = iCategoria + (nAtividades * iRubricaGeral++ - iCategoria);

                    const orcados = Array(nAtividades).fill(false).map(
                        (_,iAtividade) => orcadosRub[iOrcadoRubricaInicio + iAtividade].value
                    );

                    return { nome, codigo , orcados };
                });

                return { nome, orcados, rubricas: rubricasComOrcados };
            });

            return {
                nome       : nomePeriodo,
                orcado     : orcadoPeriodo,
                dataInicio : dataInicioPeriodo,
                dataFim    : dataFimPeriodo,
                atividades : atividadesComOrcados,
                categorias : categoriasComRubricasEOrcados
            };
        }

		document.querySelector('#adicionarPeriodo').addEventListener('submit',e => {
            e.preventDefault();

            const
				infoPeriodo = getPeriodoInfo(atividades,categorias),
				form = e.target,
				inputJSON = form.querySelector('input[name="periodo"]');

            console.log(infoPeriodo);

			inputJSON.value = JSON.stringify(infoPeriodo);
			form.submit();
        });
    </script>
</body>
</html>
