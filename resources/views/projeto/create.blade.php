<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Cadastrar Projeto</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="container">
		<div id="hot"></div>

			<div class="container w-50">
		<h1 class="mt-2">Cadastrar Projeto</h1>

        <hr/>
		@if ($errors->any())
			<div class="alert alert-danger pb-0">
				<ul>
					@foreach ($errors->all() as $error)
						<li class="h5">{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

		<form action="{{ route('projeto.salvar') }}" method="POST" id="cadastrarProjeto">
			{{ csrf_field() }}

			<div class="form-row">
				<div class="col">
					<input class="form-control" type="text" name="nome" placeholder="Nome do Projeto" data-toggle="tooltip" title="Insira o nome do Projeto" />
				</div>
			</div>

			<div class="form-group mt-3">
				<input class="form-control" type="text" name="descricao" placeholder="Descrição" data-toggle="tooltip" title="Insira a Descrição do Projeto" />
			</div>

			<div class="form-row mb-4">
				<div class="col">
					<label for="data_inicio">Data de início: </label>
					<input required class="form-control" type="date" name="data_inicio" placeholder="Data Inicial" data-toggle="tooltip" title="Insira a Data Inicial do Projeto"/>
				</div>

				<div class="col">
					<label for="data_fim">Data de fim: </label>
					<input class="form-control" type="date" name="data_fim" placeholder="Data Final" data-toggle="tooltip" title="Insira a Data Final do Projeto"/>
				</div>
			</div>

            <hr />

			<input type="hidden" name="periodos" />

				<div class="forms-periodos">
					<div class="form-periodo" id="periodo-1">
						<h3 class="d-inline-block">Período 1</h3>

						<div class="form-row mb-3">
							<div class="col">
								<input required class="form-control" type="text" name="nomePeriodo" placeholder="Nome do Período" data-toggle="tooltip" title="Insira o nome do Período" />
							</div>
						</div>

						<div class="form-row mb-4">
							<div class="col">
								<label for="data_inicio">Data de início: </label>
								<input required class="form-control" type="date" name="data_inicioPeriodo" placeholder="Data Inicial data-toggle="tooltip" title="Insira a Data Inicial do Período""/>
							</div>

							<div class="col">
								<label for="data_fim">Data de fim: </label>
								<input class="form-control" type="date" name="data_fimPeriodo" placeholder="Data Final" data-toggle="tooltip" title="Insira a Data Final do Período"/>
							</div>
						</div>


					</div>
				</div>

                <hr/>
				<div class="row mt-1 mb-3">
					<div class="col-sm">
						<button type="button" class="btn btn-primary btn-block adicionarPeriodo" data-toggle="tooltip" title="Clique para adicionar novo Período">Adicionar novo período</button>
					</div>
				</div>


			<div class="row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success btn-block" data-toggle="tooltip" title="Clique para cadastrar o Projeto">Cadastrar</button>
				</div>
				<div class="col-sm">
					<a href="{{ route('home') }}" class="btn btn-danger btn-block" data-toggle="tooltip" title="Clique para voltar para a tela principal">Voltar</a>
				</div>
			</div>


		</form>
	</div>


	<script>
		function criarFormGroup(){
			const elem = document.createElement('div');
			elem.className = 'form-group';

			return elem;
		}

		function criarBotaoFecharPeriodo(){
			const
				botao = document.createElement('button'),
				botaoDentro = document.createElement('span');

			botao.type = 'button';
			botao.className = 'close float-right';
			botao.setAttribute('aria-hidden','Close');

			botaoDentro.setAttribute('aria-hidden',true);
			botaoDentro.innerHTML = '&times;';
			botao.appendChild(botaoDentro);

			return botao;
		}

		function fecharPeriodo(iPeriodo){
			const periodoElem = document.getElementById('periodo-' + iPeriodo);

			periodoElem.remove();
		}

		function criarPeriodoElem(iPeriodo,onFecharPeriodo){
			const
				container = document.createElement('div'),
				formRowNomeEOrcado = document.createElement('div'),
				formRowDatas = document.createElement('div'),
				colNome = document.createElement('div'),
				colDataInicio = document.createElement('div'),
				colDataFim = document.createElement('div'),
				labelDataInicio = document.createElement('label'),
				labelDataFim = document.createElement('label'),
				periodoHeader = document.createElement('h3'),
				botaoFecharPeriodo = criarBotaoFecharPeriodo();
				nomeDoPeriodoInput = document.createElement('input'),
				dataInicioElem = criarFormGroup(),
				dataInicioInput = document.createElement('input'),
				dataFimElem = criarFormGroup(),
				dataFimInput = document.createElement('input');

			formRowNomeEOrcado.className = 'form-row mb-3';
			formRowDatas.className = 'form-row mb-4';
			colNome.className = 'col';
			colDataInicio.className = 'col';
			colDataFim.className = 'col';
			periodoHeader.className = 'd-inline-block';
			periodoHeader.innerHTML = 'Periodo ' + iPeriodo;

			botaoFecharPeriodo.addEventListener('click',onFecharPeriodo)

			labelDataInicio.innerHTML = 'Data de início: ';
			labelDataFim.innerHTML = 'Data de fim: ';

			nomeDoPeriodoInput.className = 'form-control';
			nomeDoPeriodoInput.type = 'text';
			nomeDoPeriodoInput.name = 'nomePeriodo';
			nomeDoPeriodoInput.required = true;
			nomeDoPeriodoInput.placeholder = 'Nome do Periodo';

			dataInicioInput.className = 'form-control';
			dataInicioInput.type = 'date';
			dataInicioInput.name = 'data_inicioPeriodo';
			dataInicioElem.appendChild(dataInicioInput);

			dataFimInput.className = 'form-control';
			dataFimInput.type = 'date';
			dataFimInput.name = 'data_fimPeriodo';
			dataFimElem.appendChild(dataFimInput);

			colNome.appendChild(nomeDoPeriodoInput);
			colDataInicio.appendChild(labelDataInicio);
			colDataInicio.appendChild(dataInicioElem);
			colDataFim.appendChild(labelDataFim);
			colDataFim.appendChild(dataFimElem);

			formRowNomeEOrcado.appendChild(colNome);
			formRowDatas.appendChild(colDataInicio);
			formRowDatas.appendChild(colDataFim);

			container.className = 'form-periodo';
			container.id = 'periodo-' + iPeriodo;
			container.appendChild(periodoHeader);
			container.appendChild(botaoFecharPeriodo);
			container.appendChild(formRowNomeEOrcado);
			container.appendChild(formRowDatas);

			return container;
		}

		function getPeriodos(){
			const formsPeriodos = document.getElementsByClassName('form-periodo');

			return [].map.call(formsPeriodos,form => {
				const
					nomePeriodo 	    = form.querySelector('input[name="nomePeriodo"]').value,
					data_inicioPeriodo  = form.querySelector('input[name="data_inicioPeriodo"]').value;
					data_fimPeriodo     = form.querySelector('input[name="data_fimPeriodo"]').value;

				return {
					nomePeriodo,
					data_inicioPeriodo,
					data_fimPeriodo
				};
			});
		}

		let iPeriodo = 2;

		document.querySelector('.adicionarPeriodo').addEventListener('click',() => {
			const
				iPeriodoAtual   = iPeriodo++,
				novaPeriodoElem = criarPeriodoElem(iPeriodoAtual,({ target }) => {
					const
						idElemPeriodoARemover = target.parentElement.parentElement.id,
						iPeriodoARemover = idElemPeriodoARemover.substr(idElemPeriodoARemover.match('-').index + 1);

					fecharPeriodo(iPeriodoARemover);

					const
						elemsPeriodos = document.getElementsByClassName('form-periodo');
						periodosAModificar = [].filter.call(elemsPeriodos, elem => {
							const
								id = elem.id,
								iPeriodoElem = id.substr(id.match('-').index + 1);

							return iPeriodoElem > iPeriodoARemover;
						});

					periodosAModificar.forEach(periodoElem => {
						const
							id = periodoElem.id,
							iPeriodoElem = id.substr(id.match('-').index + 1),
							periodoHeader = periodoElem.querySelector('h3');

						periodoElem.id = 'periodo-' + (iPeriodoElem - 1);
						periodoHeader.innerHTML = 'Periodo ' + (iPeriodoElem - 1);
					});

					iPeriodo--;
				});

			document.querySelector('.forms-periodos').appendChild(novaPeriodoElem);
		});

		document.querySelector('#cadastrarProjeto').addEventListener('submit',e => {
			e.preventDefault();

			const
				periodos = getPeriodos(),
				form = e.target,
				inputJSON = form.querySelector('input[name="periodos"]');

			inputJSON.value = JSON.stringify(periodos);
			form.submit();
		});
	</script>
</body>
</html>
