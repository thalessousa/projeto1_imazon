<!DOCTYPE html>
<html>
<style>
    a:hover{
        text-decoration: none !important;
    }
    a[data-toggle="collapse"]{
        cursor: pointer
    }
    table, th, td {
        padding: 5 px;
    }
    table {
  border-collapse: collapse;
  }

  .btn-aceitar-edicao i{
  	color: white;
  }

  .fa-trash {
  color: red;
}
 .fa-times-red {
  color: red;
}
 .fa-search {
  color: light-blue;
}
 .fa-edit {
  color: light-green;
}

thead th {
    background-color: #006DCC;
    color: white;
}

.fa-check{
    color: #36ab36;
}

.seta-direita{
    transition: transform 0.5s cubic-bezier(.68,-0.55,.27,1.55);
}

.rotated{

    transform: rotate(90deg);
}

.nav-hover{
	transition: background 0.5s;
}

</style>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title align="center">IMAZON - Visualizar Projetos</title>
  <link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>

<body>
  <div class="container">

      <div class="container w-100">
    <h1>Visualizar Projetos</h1>

    @if ($errors->any())
          <div class="alert alert-danger pb-0">
        <ul>
          @foreach ($errors->all() as $error)
            <li class="h5">{{ $error }}</li>
          @endforeach
        </ul>
          </div>
    @endif

    @if (session('message'))
      <div class="alert alert-success">
        {{ session('message') }}
      </div>
    @endif

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="andamento-tab" data-toggle="tab" href="#andamento" role="tab" aria-controls="andamento" aria-selected="true">Projetos em andamento</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="finalizados-tab" data-toggle="tab" href="#finalizados" role="tab" aria-controls="finalizados" aria-selected="false">Projetos finalizados</a>
  </li>
</ul>

<div class="tab-content" id="myTabContent">


<div class="tab-pane fade show active" id="andamento" role="tabpanel" aria-labelledby="andamento-tab">

 <form action="{{route('projeto.view')}}" method="GET">
      {{ csrf_field() }}

<table  class="table table-striped" style="width:100%">
 <thead class="black white-text">
  <tr>
    <th scope="col">Nome do Projeto</th>
    <th scope="col">ID </th>
    <th scope="col">Data Inicial  </th>
    <th scope="col">Data Final  </th>
    <th scope="col">Orçado  </th>
    <th scope="col">ID do Criador  </th>
    <th scope="col">Ações</th>
  </tr>
  </thead>
  <tbody id="tabela">
    @foreach ($projetos-> all() as $projeto)
      @if ($projeto->finalizado == 0)
        <tr class="form-editavel-elem" data-rota-atualizar="{!! route('projeto.atualizar',$projeto->id) !!}">
          <td scope="row" class="item-form-editavel tipo-item-texto" data-nome="nome">
            <span class="item-form-editavel-texto">
                <strong>&nbsp;&nbsp; {{ $projeto->nome }} </strong>
            </span>
          </td>
          <td scope="row">{{ $projeto->id }}</td>
          <td scope="row" class="item-form-editavel tipo-item-data" data-nome="data_inicio">
              <span class="item-form-editavel-texto">
                {{ $projeto->data_inicio ? \DateTime::createFromFormat('Y-m-d', $projeto->data_inicio)->format('d/m/Y') : '' }}
              </span>
          </td>
          <td scope="row" class="item-form-editavel tipo-item-data" data-nome="data_fim">
            <span class="item-form-editavel-texto">
                {{ $projeto->data_fim ? \DateTime::createFromFormat('Y-m-d', $projeto->data_fim)->format('d/m/Y') : '' }}
            </span>
          </td>
          <td scope="row" class="item-form-editavel tipo-item-numero" data-nome="orcado">
            <span class="item-form-editavel-texto">
                {{ number_format($projeto->orcado,2,',','.') }}
            </span>
          </td>
          <td scope="row">{{ $projeto->criador }}</td>
          <td class="text-center" nowrap="nowrap">
            <a
              href="#"
              data-toggle="tooltip" title="Editar Projeto"
              data-original-title="Editar Projeto" name="Editar Projeto"
              class="mr-3 alternar-form-editavel"
            >
              <i class="fa fa-edit" aria-hidden="true"></i>
            </a>
            <span data-toggle="modal" data-target="#modal{{ $projeto->id }}">
                <a
                    href="#"
                    data-toggle="tooltip" title="Editar Períodos"
                    data-original-title="Editar Períodos" name="Editar Períodos"
                    class="mr-3"
                >
                    <i class="fas fa-calendar-alt"></i>
                </a>

            </span>
            <a
              href="{{ route('projeto.planilha.editar',$projeto->id) }}"
              data-toggle="tooltip" title="Visualizar Projeto"
              data-original-title="Visualizar Projeto" name="Visualizar Projeto"
              class="mr-3"
            >
              <i class="fa fa-search" aria-hidden="true"></i>
            </a>
            <a href="{{ route('projeto.finalizar',$projeto->id) }}"data-toggle="tooltip" title="Finalizar Projeto">
              <i class="fa fa-times fa-times-red" aria-hidden="true"></i>
            </a>
          </td>
        </tr>
      @endif
    @endforeach
  </tbody>
</table>
 <div class="col-sm mb-3">
          <a href="{{ route('home') }}" class="btn btn-danger ">Voltar</a>
        </div>
       </form>
      </div>

<div class="tab-pane fade" id="finalizados" role="tabpanel" aria-labelledby="finalizados-tab">

   <form action="{{route('projeto.view')}}" method="GET">
      {{ csrf_field() }}

<table  class="table table-striped">
  <thead>
  <tr>
    <th scope="col" text-align: left;>Nome do Projeto</th>
    <th scope="col" text-align: left;>ID </th>
    <th scope="col" text-align: left;>Data Inicial  </th> <th text-align: left;>Data Final  </th>
    <th scope="col" text-align: left;>Orçado  </th>
    <th scope="col" text-align: left;>ID do Criador  </th>
    <th scope="col" text-align: left;>Ações</th>
  </tr>
  </thead>
  <tbody>
          @foreach ($projetos-> all() as $projeto)
          @if ($projeto->finalizado == 1)
          <tr>
            <td scope="row" class="item-form-editavel"><strong> {{ $projeto->nome }} </strong></td>
            <td scope="row" class="item-form-editavel">{{ $projeto->id }}</td>
            <td scope="row" class="item-form-editavel">{{ $projeto->data_inicio ? \DateTime::createFromFormat('Y-m-d', $projeto->data_inicio)->format('d/m/Y') : '' }}</td>
            <td scope="row" class="item-form-editavel">{{ $projeto->data_fim ? \DateTime::createFromFormat('Y-m-d', $projeto->data_fim)->format('d/m/Y') : '' }}</td>
            <td scope="row" class="item-form-editavel">{{ number_format($projeto->orcado,2,',','.') }}</td>
            <td scope="row" class="item-form-editavel">{{ $projeto->criador }}</td>

          <td class="text-center" nowrap="nowrap">
            <a href="{{ route('projeto.planilha.editar',$projeto->id) }}"data-toggle="tooltip"title="Visualizar Projeto" data-original-title="Visualizar Projeto" name="Editar Projeto" class="mr-3">
                <i class="fa fa-search" aria-hidden="true"></i>
            </a>

            <a href="{{ route('projeto.reativar',$projeto->id) }}" data-toggle="tooltip" title="Reativar Projeto" data-original-title="Reativar Projeto" name="Reativar Projeto" class="mr-3">
                <i class="fa fa-check" aria-hidden="true" ></i>
            </a>

            <a href="{{ route('projeto.deletar',$projeto->id) }}"data-toggle="tooltip" title="Deletar Projeto">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </a>
          </td>

          </tr>
          @endif
          @endforeach
  </tbody>
</table>
        <div class="col-sm mb-3">
          <a href="{{ route('home') }}" class="btn btn-danger">Voltar</a>
        </div>
        </form>
      </div>
</div>
</div>

    @foreach ($projetos as $projeto)
        <div id="modal{{ $projeto->id }}" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $projeto->nome }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        @for ($i = 0; $i < count($projeto->periodos); $i++)
                            @php
                                $periodo = $projeto->periodos[$i];
                            @endphp
                            <li class="nav-item ml-2 mr-2">
                                <a class="nav-link nav-hover {{ $i === 0 ? "active" : "" }}" data-toggle="pill" href="#pill-{{ $periodo->id }}" role="tab">{{ $periodo->nome }}</a>
                            </li>
                        @endfor
                    </ul>
                    <div class="tab-content" id="pills-{{ $projeto->id }}">
                        <hr />
                        <div class="row mt-3">
                            <div class="col font-weight-bold">Nome</div>
                            <div class="col font-weight-bold">Data de início</div>
                            <div class="col font-weight-bold">Data de fim</div>
                            <div class="col font-weight-bold">Orçado</div>
                            <div class="col font-weight-bold">Ações</div>
                        </div>
                        @for ($i = 0; $i < count($projeto->periodos); $i++)
                            @php
                                $periodo = $projeto->periodos[$i];
                            @endphp
                            <div class="tab-pane fade {{ $i === 0 ? "show active" : "" }}" id="pill-{{ $periodo->id }}" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="row form-editavel-elem" data-rota-atualizar="{!! route('periodo.atualizar',$periodo->id) !!}">
                                    <div class="col item-form-editavel tipo-item-texto" data-nome="nome">
										<span class="item-form-editavel-texto">
											{{ $periodo->nome }}
										</span>
                                    </div>
                                    <div class="col item-form-editavel tipo-item-data" data-nome="data_inicio">
										<span class="item-form-editavel-texto">
											{{ $periodo->data_inicio ? \DateTime::createFromFormat('Y-m-d', $periodo->data_inicio)->format('d/m/Y') : '' }}
										</span>
                                    </div>
                                    <div class="col item-form-editavel tipo-item-data" data-nome="data_fim">
   										<span class="item-form-editavel-texto">
											{{ $periodo->data_inicio ? \DateTime::createFromFormat('Y-m-d', $periodo->data_inicio)->format('d/m/Y') : '' }}
										</span>
                                    </div>
                                    <div class="col item-form-editavel tipo-item-numero" data-nome="orcado">
   										<span class="item-form-editavel-texto">
											{{ number_format($periodo->orcado,2,',','.') }}
										</span>
                                    </div>
                                    <div class="col">
										<a href="#" class="alternar-form-editavel">
	                                    	<i class="fa fa-edit" data-toggle="tooltip" title="Editar Projeto"></i>  
										</a>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
            </div>
    @endforeach

<script src="/js/app.js"></script>
<script src="/js/viewProjetos.js"></script>
<script src="/js/editPeriodos.js"></script>
</body>
</html>

