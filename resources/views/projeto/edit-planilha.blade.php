<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMAZON - Editar planilha de projeto</title>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
</head>
<body>
	<div class="main-container">
		<h1 class="mt-2">Editar planilha de projeto</h1>
		<hr />

		@if ($errors->any())
			<div class="alert alert-danger pb-0">
				<ul>
					@foreach ($errors->all() as $error)
						<li class="h5">{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

		<div>
			<ul id="tabsContainer" class="nav nav-tabs" role="tablist">
			</ul>

            <div class="menuPlanilhas">
                <div id="planilhasContainer" class="tab-content">
                </div>
                <div class="botoesPlanilhaContainer" id="botoesPlanilhaContainer">
                </div>
            </div>
		</div>

		@if ($projeto)
            <div class="d-inline-block w-100">
                <div class="offset-botao-acoes"></div>
                <div class="btn-group float-right">
                    <button class='btn btn-info dropdown-toggle' style="border: 1px solid rgba(0, 0, 0, 0.15);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ações</button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{!! route('periodo.adicionar',$projeto->id)!!}" target="_blank">
                            Criar novo período
                        </a>
                        <a class="dropdown-item" href="{!! route('atividade.adicionar',$projeto->id)!!}" target="_blank">
                            Criar nova atividade
                        </a>
                        <a class="dropdown-item" href="{!! route('categoria.criar',$projeto->id)!!}" target="_blank">
                            Criar nova categoria
                        </a>
                    </div>
                </div>
                <span class='float-left'>* - Períodos finalizados</span>
            </div>
			<div class="form-group row mt-4">
				<label for="aplicacao" class="col-sm-3 col-form-label"><h4>Total aplicado: </h4></label>
				<div class="col-sm-2">
					@if ($projeto->aplicacao)
						<input
							type="text" readonly name="aplicacao" value="{{ $projeto->aplicacao }}"
							class="form-control" id="aplicacao" placeholder="Aplicação"
						>
					@else
						<input type="text" readonly name="aplicacao" class="form-control" id="aplicacao" placeholder="Aplicação">
					@endif
				</div>
				<div class="btn btn-link" id="editarAplicacao">Editar</div>
			</div>
			<div class="form-group row">
				<label for="rendimento" class="col-sm-3 col-form-label"><h4>Valor rendimento: </h4></label>
				<div class="col-sm-2">
					@if ($projeto->rendimento)
						<input
							type="text" readonly name="rendimento" value="{{ $projeto->rendimento }}"
							class="form-control" id="rendimento" placeholder="Aplicação"
						>
					@else
						<input type="text" readonly class="form-control" name="rendimento" id="rendimento" placeholder="Rendimento">
					@endif
				</div>
				<div class="btn btn-link" id="editarRendimento">Editar</div>
			</div>

			<div class="row mt-2">
				<div class="col-sm">
					<label class="btn btn-primary btn-block" for="entradaPlanilha">
						Selecionar arquivo para leitura <input type="file" id="entradaPlanilha" name="entradaPlanilha" hidden>
					</label>

				</div>

				<div class="col-sm planilha-botoes">
					<div class="row">
						<div class="col-sm-6">
							<button
								class="btn btn-primary btn-block float-left dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
							>
								Exportar
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#" id="exportarPlanilhaProjeto">
									Planilha do projeto
                                </a>
								<a class="dropdown-item" href="#" id="exportarPlanilhaProjetoPDF">
									Planilha do projeto (PDF)
                                </a>
								<a class="dropdown-item" href="#" id="exportarPlanilhaDetalhada">
									Planilha detalhada
								</a>
								<a class="dropdown-item" href="#" id="exportarPlanilhaDetalhadaPDF">
									Planilha detalhada (PDF)
								</a>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="{{ route('home') }}" class="btn btn-primary btn-block">
								Página principal
							</a>
						</div>
					</div>
				</div>

			</div>
			<div class="row mt-2 mb-3">
				<div class="col-sm">
					<button id="finalizarPeriodo" class="btn btn-danger btn-block" type="submit" data-toggle='modal' data-target='#finalizarPeriodoModal'>
						Finalizar período
                    </button>
				</div>

				<div class="col-sm">
					<form
						id="formDadosPlanilhas" action="{{ route('projeto.planilha.atualizar', ['id'=>$projeto->id]) }}" method="POST"
					>
						@csrf
						@method('PUT')

                        <input type="hidden" name="planilhas" />

						<button class="btn btn-success btn-block" type="submit">Atualizar valores</button>
					</form>
				</div>
			</div>

		@endif
	</div>

	<div class="modal fade" id="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Por favor selecione um tipo de arquivo suportado!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Arquivos .xls, .xlsx e .xlsm são suportados</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editarAplicacaoModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Editar valor</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<label for="aplicacao">Valor a somar/subtrair: &nbsp;</label>
				<input type="text">
				<button id="subtrairValorBtn" class="btn btn-danger float-right modificarValorAplicacao">
					-
				</button>
				<button id="adicionarValorBtn" class="btn btn-success float-right mr-2 modificarValorAplicacao">
					+
				</button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="addCelulaModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Somar/subtrair valor à celula selecionada</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<label for="aplicacao">Valor a somar/subtrair: &nbsp;</label>
				<input type="text">
				<button id="subtrairValorCelula" class="btn btn-danger float-right modificarValorCelula">
					-
				</button>
				<button id="adicionarValorCelula" class="btn btn-success float-right mr-2 modificarValorCelula">
					+
				</button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="finalizarPeriodoModal" tabindex="-1" role="dialog">
        <form method='POST'>
            @csrf
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="content-finalizar-periodo">
                        <div class="modal-header">
                            <h5 class="modal-title">Deseja mesmo finalizar o período?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="font-weight-bold">Deseja finalizar o período "<span class="nome-periodo"></span>"?</p>
                            <br />
                            <p><span class="font-weight-bold">Data de finalização: </span>{!! date('d/m/Y') !!}</p>

                            <label for="nome_novo_periodo"><span class="font-weight-bold">Nome do novo período:</span></label>
                            <input type="text" name="nome_novo_periodo">
                        </div>
                    </div>
                    <div class="erro-ja-finalizado d-none">
                        <div class="modal-header">
                            <h5 class="modal-title">Atenção!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="font-weight-bold">Período "<span class="nome-periodo"></span>" já foi finalizado!</p>
                        </div>
                    </div>
                <div class="modal-footer">
                    <div class="botoes-finalizar-modal">
                        <button type="button" class="btn btn-success btnFinalizarPeriodo" data-dismiss="modal">Sim</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                    </div>
                    <button type="button" class="btn botao-ok btn-primary d-none" data-dismiss="modal">OK</button>
                </div>
                </div>
            </div>
        </form>
	</div>

	<div class="modal fade" id="atencaoSemRegistrosModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Atenção!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Não existem registros de planilha de entrada carregados!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
    </div>

	<div class="modal fade" id="verificacaoOrcamentoModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Atenção!</h5>
			</div>
			<div class="modal-body">
                <ul class="lista-erros"></ul>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar para edição</button>
				<button type="button" class="btn btn-primary btn-salvar">Salvar mesmo assim</button>
			</div>
			</div>
		</div>
    </div>

	<div class="modal fade" id="rubricaNaoCadastradaModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Atenção!</h5>
			</div>
			<div class="modal-body">
				<h5>As rubricas abaixo não estão cadastradas no sistema!</h5>
                <ul class="lista-erros"></ul>
				<p class=>Para prosseguir com a leitura de planilhas, as rubricas listadas precisam estar cadastradas</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			</div>
			</div>
		</div>
    </div>
    <form action="">
        @csrf
    </form>

				{{-- <div class="col-lg">

					<a href="{{ route('projeto.graficos.view',['id'=>$projeto->id]) }}" class="btn btn-primary float-right">
						Gráficos
					</a>

				</div> --}}
	<div class="dropdown-menu" id='atividadeDropdown'>
		<a class="dropdown-item" href="#">Action</a>
		<a class="dropdown-item" href="#">Another action</a>
		<a class="dropdown-item" href="#">Something else here</a>
		<div class="dropdown-divider"></div>
		<a class="dropdown-item" href="#">Separated link</a>
	</div>

	<script src="/js/app.js"></script>
	<script>
        window.token = document.head.querySelector('meta[name="csrf-token"]');

        window.projeto = {!! $projeto ? $projeto->toJson() : 'undefined' !!};

		window.rotaCriarPeriodo   = '{!! $projeto ? route('periodo.adicionar',$projeto->id) : 'undefined' !!}';
		window.rotaCriarAtividade = '{!! $projeto ? route('atividade.adicionar',$projeto->id) : 'undefined' !!}';
		window.rotaCriarCategoria = '{!! $projeto ? route('categoria.criar',$projeto->id) : 'undefined' !!}';
        window.rotaCriarRubrica = '{!! $projeto ? route('categoria.rubrica.adicionar',[$projeto->id,":nomeCategoria"]) : 'undefined' !!}';

        window.rotaEnviarArquivo = '{!! $projeto ? route('projeto.planilhaPdf',$projeto->id) : 'undefined' !!}';
	</script>


	<script src="/js/planilha.js"></script>
</body>
</html>
