document.querySelectorAll("a[data-toggle='collapse']").forEach(elem =>
  elem.addEventListener("click", function(e) {
    const { target } = e;
    target.classList.toggle("rotated");
  })
);

document
  .querySelectorAll('*[data-toggle="modal"] a')
  .forEach(elem => elem.addEventListener("click", e => e.preventDefault()));
