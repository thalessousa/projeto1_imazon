//@ts-check

const Handsontable = require("handsontable");
const XLSX = require("xlsx");
const moment = require("moment");

const {
  projeto,
  rotaCriarPeriodo,
  rotaCriarAtividade,
  rotaCriarCategoria,
  rotaCriarRubrica
} = window;

let dadosPeriodos, planilhas;

const rABS = true;

function carregarBotoesAddRubrica(idContainer, hot) {
  const botoesPlanilhaContainerElem = document.getElementById(idContainer);
  const botoesPlanilhaElem = document.createElement("div");
  const nLinhas = hot.countRows();

  botoesPlanilhaElem.className = "botoesPlanilha";

  _.range(nLinhas)
    .map(i => hot.getCellMetaAtRow(i))
    .map(linha => {
      const { isCategoria, nomeCategoria } = linha[0];
      const botaoCategoria = document.createElement("div");
      const rota = rotaCriarRubrica.replace(":nomeCategoria", nomeCategoria);
      botaoCategoria.className = "item";
      botaoCategoria.innerHTML = isCategoria
        ? `
                    <a
                        href="${rota}"
                        target="_blank"
                        data-toggle="tooltip"
                        data-placement="top"
                        title='Adicionar rubrica para a categoria "${nomeCategoria}"'
                    >
                        <i class="fas fa-plus"></i>
                    </a>
                `
        : "";

      return botaoCategoria;
    })
    .forEach(x => botoesPlanilhaElem.appendChild(x));

  botoesPlanilhaContainerElem.appendChild(botoesPlanilhaElem);
  $('[data-toggle="tooltip"]').tooltip(); //initialize tooltipse
  return botoesPlanilhaContainerElem;
}

function obterAtividadesDePlanilha(workbook, projeto) {
  const sheetnames = workbook.SheetNames,
    content = XLSX.utils
      .sheet_to_json(workbook.Sheets[sheetnames[0]], { header: 1 }) //carregar conteúdo da planilha e processa seus dados
      .map(linha => ({
        emissao: moment(linha[1], "DD/MM/YY"),
        baixa: moment(linha[2], "DD/MM/YY"),
        numero: linha[3],
        gerencial: linha[5],
        nomeRubrica: linha[6] ? linha[6].replace(/\(.*-.*\) /g, "") : "",
        sacado_cedente: linha[7] ? linha[7] : null,
        historico: linha[8],
        valor: linha[9] ? parseFloat(linha[9].replace(",", "")) : "",
        codRubrica: linha[0] ? linha[0].replace(/\(.*-.*\) /g, "") : "",
        composicao: linha[10] ? parseFloat(linha[10].replace(",", "")) : "",
        codAtividade: linha[4]
      }))
      .filter(
        obj =>
          obj.emissao.isValid() &&
          obj.baixa.isValid() &&
          obj.nomeRubrica &&
          obj.composicao
      ),
    dadosPeriodos = projeto.periodos.map(periodo => {
      const atividadesPeriodo = projeto.atividades.filter(
        atv => atv.periodo_id === periodo.id
      );

      return {
        ...periodo,
        atividades: atividadesPeriodo
      };
    }),
    atividadesPeriodos = dadosPeriodos.map((_, iPeriodo) => {
      return content.reduce((acc, cur) => {
        //transformar dados em um objeto indexado pelo código das atividades

        const periodoEncontrado = projeto.periodos.findIndex(
          periodo =>
            !periodo.data_fim_real &&
            cur.baixa >= moment(periodo.data_inicio) &&
            cur.baixa <= moment(periodo.data_fim)
        );
        const periodosAtivos = dadosPeriodos.filter(
          periodo => !periodo.data_fim_real
        );

        if (periodosAtivos.length === 0) {
          return acc;
        }
        const ultimoPeriodoAtivoId = periodosAtivos.slice(-1)[0].id;
        const ultimoPeriodoAtivo = projeto.periodos.findIndex(
          periodo => periodo.id === ultimoPeriodoAtivoId
        );
        const periodo =
          periodoEncontrado !== -1 ? periodoEncontrado : ultimoPeriodoAtivo;

        if (periodo !== iPeriodo) return acc;

        return acc[cur.codAtividade]
          ? {
              ...acc,
              [cur.codAtividade]: {
                ...acc[cur.codAtividade],
                [cur.codRubrica]: {
                  executado: acc[cur.codAtividade][cur.codRubrica]
                    ? acc[cur.codAtividade][cur.codRubrica].executado +
                      cur.composicao
                    : cur.composicao,
                  linhas: acc[cur.codAtividade][cur.codRubrica]
                    ? [...acc[cur.codAtividade][cur.codRubrica].linhas, cur]
                    : [cur]
                }
              }
            }
          : {
              ...acc,
              [cur.codAtividade]: {
                [cur.codRubrica]: {
                  executado: cur.composicao,
                  linhas: [cur]
                }
              }
            };
      }, {});
    });

  return [atividadesPeriodos, dadosPeriodos];
}

function encontraRubricaCategorias(codigoRubrica, categorias) {
  const todasRubricas = categorias.reduce(
    (acc, cat) => [...acc, ...cat.rubricas],
    []
  );
  const rubrica = todasRubricas.find(
    rub => rub.codigo.toLowerCase() === codigoRubrica.toLowerCase()
  );

  return rubrica;
}

function encontraCategoriaAtividades(idCategoria, atividades) {
  const todasCategorias = atividades.reduce(
    (acc, atv) => [...acc, ...atv.categorias],
    []
  );
  const categoria = todasCategorias.find(cat => cat.id === idCategoria);

  return categoria;
}

//atualiza variável de projeto com dados da planilha de entrada que não ficam visíveis na planilha da página
//	ex: (data de emissão/baixa, sacado/cedente, etc)
function atualizaProjetoDadosPlanilhaEntrada(dadosPeriodos, projeto) {
  const { periodos, atividades } = projeto;
  let registrosPlanilha = [];

  let codigosRubricasNaoCadastradas = [];
  dadosPeriodos.forEach((dadoPeriodo, iPeriodo) => {
    const periodo = periodos[iPeriodo],
      idPeriodo = periodo.id;

    _.forOwn(dadoPeriodo, (rubricas, codigoAtividade) => {
      //pega atividade dentro de 'periodo' cujo codigo é igual a codigoAtividade
      const atividadesPeriodo = atividades.filter(
        atv => atv.periodo_id === idPeriodo
      );

      const atividade = atividadesPeriodo.find(
        atv => atv.codigo.toLowerCase() === codigoAtividade.toLowerCase()
      );

      _.forOwn(rubricas, (rubricaEntrada, codigoRubrica) => {
        //pega aqui rubrica dentro de atividade (e das categorias/subcategorias)
        //	cujo codigo é igual a codigoRubrica
        //depois atualiza essa rubrica com as linhas
        const { categorias } = atividade;

        const rubrica = encontraRubricaCategorias(codigoRubrica, categorias);

        console.log(codigoRubrica, rubrica);
        console.log(codigosRubricasNaoCadastradas.includes(codigoRubrica));
        console.log(
          !rubrica,
          !codigosRubricasNaoCadastradas.includes(codigoRubrica),
          !rubrica &&
          !codigosRubricasNaoCadastradas.includes(codigoRubrica)
        )
        if (
          !rubrica &&
          !codigosRubricasNaoCadastradas.includes(codigoRubrica)
        ) {
          codigosRubricasNaoCadastradas = [
            ...codigosRubricasNaoCadastradas,
            codigoRubrica
          ];
          return;
        }

        if(codigosRubricasNaoCadastradas.includes(codigoRubrica)){
            return ;
        }

        rubrica.registrosPlanilha = rubricaEntrada.linhas;
        registrosPlanilha = registrosPlanilha.concat(
          rubricaEntrada.linhas.map(registro => ({
            ...registro,
            rubrica_id: rubrica.id,
            rubrica
          }))
        );
      });
    });
  });

  projeto.registrosPlanilha = registrosPlanilha;
  return [projeto, codigosRubricasNaoCadastradas];
}

function atualizaPlanilhasComDadosDeEntrada(dadosPeriodos, projeto, planilhas) {
  const periodos = projeto.periodos;

  dadosPeriodos.forEach((dadoPeriodo, iPeriodo) => {
    const periodoInfo = periodos[iPeriodo],
      idPeriodo = periodoInfo.id;

    _.forOwn(dadoPeriodo, (rubricas, codigoAtividade) => {
      /**
       * Iterar sobre dadosAtividades
       * Para cada atividade
       * 	verificar índice de atividade no objeto projeto
       * 	acessar essa atividade no objeto planilhas por meio do mesmo índice
       * 	procurar rúbrica por match no objeto planilhas
       * 	atualizar valores da rúbrica
       */
      const planilha = planilhas.find(
        plan =>
          plan.rootElement.id ===
          "hot-" + idPeriodo + "-" + codigoAtividade.toLowerCase()
      );

      if (!planilha) return;

      const dadosPlanilha = planilha.getSourceData();

      _.forOwn(
        rubricas,
        ({ executado: valorExecutadoRubrica }, codigoRubrica) => {
          const iLinhaRubricaPlanilha = dadosPlanilha.findIndex(
              (linha, i) =>
                i === dadosPlanilha.length - 1
                  ? false
                  : linha.codigo &&
                    linha.codigo.toLowerCase() === codigoRubrica.toLowerCase()
            ),
            linhaRubricaPlanilha = dadosPlanilha[iLinhaRubricaPlanilha];

          if (!linhaRubricaPlanilha) return;
          const colExecutado = planilha.propToCol("executado"),
            novoExecutado = parseFloat(valorExecutadoRubrica).toFixed(3);

          planilha.setDataAtCell(
            iLinhaRubricaPlanilha,
            colExecutado,
            novoExecutado
          );
        }
      );
    });
  });

  return [planilhas, projeto];
}

function carregarDadosEntradaPlanilha(
  rubricaNaoCadastradaModal,
  dadosPeriodos,
  planilhas,
  projeto
) {
  /**
   * - dadosPeriodos: dados recebidos da planilha de entrada
   * - planilhas: objetos handsontable que representam planilha
   * - projeto: fonte global de informações do projeto recebidas no servidor
   *
   * Atualizar dados tanto no planilhas.getSourceData() quanto no projeto
   * - planilhas é o utilizado para renderizar as planilhas na página
   * - projeto é o objeto enviado para o servidor
   */
  let codigosRubricasNaoCadastradas = [];

  [
    projeto,
    codigosRubricasNaoCadastradas
  ] = atualizaProjetoDadosPlanilhaEntrada(dadosPeriodos, projeto);

  if (codigosRubricasNaoCadastradas.length > 0) {
    //mostrar modal que indica que existe pelo menos uma
    // rubrica na planilha que não foi cadastrada no sistema

    rubricaNaoCadastradaModal
      .find(".lista-erros")
      .html(
        codigosRubricasNaoCadastradas
          .map(rub => `<li>${rub}</li>`)
          .reduce((acc, rub) => acc + rub, "")
      );
    rubricaNaoCadastradaModal.modal();
    return;
  }

  [planilhas, projeto] = atualizaPlanilhasComDadosDeEntrada(
    dadosPeriodos,
    projeto,
    planilhas
  );
}

function handleFile(e) {
  const files = e.target.files,
    f = files[0],
    extensaoArquivo = f.name
      .split(".")
      .pop()
      .toLowerCase();

  if (
    extensaoArquivo !== "xls" &&
    extensaoArquivo !== "xlsx" &&
    extensaoArquivo !== "xlsm"
  ) {
    return $("#modal").modal("toggle");
  }

  const reader = new FileReader();
  reader.onload = function(e) {
    let data = e.target.result;
    if (!rABS) data = new Uint8Array(data);
    const workbook = XLSX.read(data, { type: rABS ? "binary" : "array" });

    const [periodos, content] = obterAtividadesDePlanilha(workbook, projeto);

    dadosPeriodos = periodos;
    const rubricaNaoCadastradaModal = $("#rubricaNaoCadastradaModal");
    carregarDadosEntradaPlanilha(
      rubricaNaoCadastradaModal,
      dadosPeriodos,
      planilhas,
      projeto
    );
  };
  if (rABS) reader.readAsBinaryString(f);
  else reader.readAsArrayBuffer(f);
}
document
  .getElementById("entradaPlanilha")
  .addEventListener("change", handleFile, false);

const nomesColunas = [
  "Código",
  "Nome",
  "Orçado",
  "Saldo Remanescente",
  "Saldo Remanescente + orçado",
  "Recebido",
  "Executado",
  "Executado Manual",
  "Executado Total",
  "Comprometido",
  "Saldo Financeiro",
  "Saldo Orçamentário"
];
const colunas = [
  {
    data: "codigo",
    type: "text"
    // width: 120
  },
  {
    data: "nome",
    type: "text"
    // width: 200
  },
  {
    data: "orcado",
    type: "numeric"
  },
  {
    data: "saldo_remanescente",
    type: "numeric",
    wordWrap: true
  },
  {
    data: "novo_orcado",
    type: "numeric",
    wordWrap: true
  },
  {
    data: "recebido",
    type: "numeric"
  },
  {
    data: "executado",
    type: "numeric"
  },
  {
    data: "executado_manual",
    type: "numeric"
    // width: 100
  },
  {
    data: "executado_total",
    type: "numeric"
  },
  {
    data: "comprometido",
    type: "numeric"
  },
  {
    data: "saldo",
    type: "numeric"
  },
  {
    data: "saldo_orcamentario",
    type: "numeric",
    wordWrap: true
  }
];

function obterIdPeriodoAtividadeDeIdTab(idTab) {
  const indicePrimeiroTraco = idTab.match(/(\d)*-/).index,
    indiceUltimoTraco = idTab.match(/-([^-]*)$/).index,
    idPeriodoAtividade = idTab.substring(
      indicePrimeiroTraco,
      indiceUltimoTraco
    );

  return idPeriodoAtividade;
}

function obterIdPeriodoDeIdTab(idTab) {
  const idPeriodoAtividade = obterIdPeriodoAtividadeDeIdTab(idTab),
    indicePrimeiroTraco = idPeriodoAtividade.match(/(\d)*-/).index,
    indiceUltimoTraco = idPeriodoAtividade.match(/-([^-]*)$/).index;
  idPeriodo = idPeriodoAtividade.substring(
    indicePrimeiroTraco + 1,
    indiceUltimoTraco
  );

  return parseInt(idPeriodo, 10);
}

function obterPlanilhaDeId(idTab) {
  const idPeriodoAtividade = obterIdPeriodoAtividadeDeIdTab(idTab),
    planilha = planilhas.find(
      plan => plan.rootElement.id === "hot" + idPeriodoAtividade
    );

  return planilha;
}

function modificaValoresCampo(hot, row, campos, fn) {
  try {
    const cols = campos.map(hot.propToCol),
      colAModificar = cols[0],
      linhas = cols.map(col => {
        return !hot.getCell(row, col) || hot.getCell(row, col).innerHTML === ""
          ? 0
          : parseFloat(hot.getCell(row, col).innerHTML);
      });

    if (typeof fn !== "function")
      throw "Último argumento precisa ser do tipo função!";

    const result = fn(linhas);

    hot.setDataAtCell(row, colAModificar, result);

    return result;
  } catch (e) {
    console.error(e);
  }
}

function modificaValoresSaldo(hot, row) {
  const campos = ["saldo", "comprometido", "executado", "recebido"],
    fn = ([_, comprometidoLinha, executadoLinha, recebidoLinha]) =>
      parseFloat(
        (recebidoLinha - comprometidoLinha - executadoLinha).toFixed(2)
      );

  return modificaValoresCampo(hot, row, campos, fn);
}

function modificaValoresSaldoOrcamentario(hot, row, dataObject) {
  const campos = ["saldo_orcamentario", "comprometido", "executado", "orcado"],
    executadoLinha = dataObject[row].executado,
    comprometidoLinha = dataObject[row].comprometido,
    orcadoLinha = dataObject[row].orcado,
    fn = () =>
      parseFloat((orcadoLinha - comprometidoLinha - executadoLinha).toFixed(2));

  return modificaValoresCampo(hot, row, campos, fn);
}

function modificaValoresNovoOrcado(hot, row) {
  const campos = ["novo_orcado", "orcado", "saldo_remanescente"],
    fn = ([_, orcadoLinha, saldoRemanescenteLinha]) =>
      parseFloat((orcadoLinha + saldoRemanescenteLinha).toFixed(2));

  return modificaValoresCampo(hot, row, campos, fn);
}

function modificaValoresExecutadoTotal(hot, row, dataObject) {
  const campos = ["executado_total", "executado", "executado_manual"],
    executadoManualLinha = dataObject[row].executado_manual,
    executadoLinha = dataObject[row].executado,
    fn = () => parseFloat((executadoLinha + executadoManualLinha).toFixed(2));

  return modificaValoresCampo(hot, row, campos, fn);
}

/**
 * Modifica total de categoria quando uma de suas rubricas teve o valor modificado
 */
function modificaTotalCampoCategoria(
  hot,
  dataObject,
  campo,
  iCategoria,
  linhaCategoria,
  linhasCategorias,
  hotPlanilha
) {
  const linhaUltimaCategoria = linhasCategorias[linhasCategorias.length - 1];
  const linhaAtividade = dataObject.length - 2;
  const linhaProximaCategoria = linhaCategoria === linhaUltimaCategoria
    ? linhaAtividade 
    : linhasCategorias[
        linhasCategorias.indexOf(linhaCategoria) + 1
    ];
  const colCampo = hot.propToCol(campo),
    linhaPrimeiraRubricaCategoria = linhaCategoria + 1,
    linhaUltimaRubricaCategoria =
      (linhaCategoria < linhasCategorias[linhasCategorias.length - 1]
        ? linhasCategorias[iCategoria + 1]
        : dataObject.length - 1) - 1,
    novoCampoTotalCategoriaOld = parseFloat(
      _.range(linhaPrimeiraRubricaCategoria, linhaUltimaRubricaCategoria)
        .map(linha => hot.getDataAtCell(linha, colCampo))
        .reduce((soma, atual) => soma + atual, 0)
        .toFixed(3)
    ),
    novoCampoTotalCategoria = dataObject.slice(linhaCategoria + 1, linhaProximaCategoria).reduce((total, linha) => total + linha[campo],0);


  hot.setDataAtCell(linhaCategoria, colCampo, novoCampoTotalCategoria);
}

/**
 * Modifica total de categoria quando uma de suas subcategorias teve o valor modificado
 */
function modificaTotalCampoCategoriaSubCategorias(
  hot,
  dataObject,
  campo,
  iCategoria,
  linhaCategoria,
  linhasSubCategorias
) {
  const colCampo = hot.propToCol(campo),
    linhaPrimeiraSubCategoriaCategoria = linhaCategoria + 1,
    linhaUltimaSubCategoriaCategoria =
      (linhaCategoria < linhasSubCategorias[linhasSubCategorias.length - 1]
        ? linhasSubCategorias[iCategoria + 1]
        : dataObject.length - 1) - 1,
    novoCampoTotalCategoria = parseFloat(
      _.range(
        linhaPrimeiraSubCategoriaCategoria,
        linhaUltimaSubCategoriaCategoria + 1
      )
        .map(linha => hot.getDataAtCell(linha, colCampo))
        .reduce((soma, atual) => soma + atual, 0)
        .toFixed(3)
    );

  hot.setDataAtCell(linhaCategoria, colCampo, novoCampoTotalCategoria);
}

function modificaTotalCampoAtividade(hot, dataObject, campo, linhasCategorias) {
  const colCampo = hot.propToCol(campo),
    totalCampoAtividade = parseFloat(
      _.sum(
        linhasCategorias.map(linha => hot.getDataAtCell(linha, colCampo))
      ).toFixed(3)
    );

  hot.setDataAtCell(dataObject.length - 2, colCampo, totalCampoAtividade);
}

function modificaTotalCampoProjeto(
  hot,
  dataObject,
  campo,
  novoValor,
  linhasCategorias,
  planilhas
) {
  const linhaProjeto = dataObject.length - 1;
  const linhaAtividade = dataObject.length - 2;
  const totalCampo = planilhas.reduce((total, hotPlanilha) => {
    const dataObjectAtv = hotPlanilha.getSourceData();
    const valorCampo = dataObjectAtv[linhaAtividade][campo];
    return total + valorCampo;
  }, 0);
  planilhas.forEach(hot =>
    hot.setDataAtCell(
      linhaProjeto,
      hot.propToCol(campo),
      totalCampo,
      "afterChange"
    )
  );
  return;
}

function modificaValoresRecebido(hot, prop, newValue) {
  const dataObject = hot.getSourceData();
  const iUltimaLinha = dataObject.length - 1;
  const col = hot.propToCol(prop);
  const orcadoTotal = dataObject[iUltimaLinha].orcado;
  const changesArray = Array(iUltimaLinha)
    .fill(false)
    .map((_, i) => {
      const orcadoLinha = dataObject[i].orcado,
        valorRecebidoCalculado = parseFloat(
          ((orcadoLinha / orcadoTotal) * newValue).toFixed(3)
        );
      return [i, col, valorRecebidoCalculado];
    })

  // Modificar linha de recebido da planilha atual (para casos em que seja a 
  //        planilha diferente da qual foi modificada o valor pelo usuário
  hot.setDataAtCell(iUltimaLinha + 1, col, parseFloat(newValue.toFixed(3)), "afterChange")
  // Modificar linha de recebido para as linhas restantes da planilha em questão
  hot.setDataAtCell(changesArray);
}

function modificaValoresRecebidoPlanilhas(
  [row, prop, oldValue, newValue],
  planilhas
) {
  planilhas.forEach(hotPlanilha =>
    modificaValoresRecebido(hotPlanilha, prop, newValue)
  );
}

function modificaTotais(
  campo,
  hot,
  row,
  dataObject,
  planilhas,
  iCategoria,
  linhaCategoria,
  linhaCategoriaGeral,
  linhasCategorias,
  linhasSubCategorias,
  novoValor
) {
  if (row === dataObject.length - 2) {
    modificaTotalCampoProjeto(
      hot,
      dataObject,
      campo,
      novoValor,
      linhasCategorias,
      planilhas
    );

    return;
  }

  if (
    linhasCategorias.indexOf(row) === -1 &&
    linhasSubCategorias.indexOf(row) === -1
  ) {
    modificaTotalCampoCategoria(
      hot,
      dataObject,
      campo,
      iCategoria,
      linhaCategoria,
      linhasCategorias,
      hot
    );
  } else if (linhasSubCategorias.indexOf(row) !== -1) {
    modificaTotalCampoCategoriaSubCategorias(
      hot,
      dataObject,
      campo,
      iCategoria,
      linhaCategoriaGeral,
      linhasSubCategorias
    );
  } else if (row !== dataObject.length - 2) {
    modificaTotalCampoAtividade(hot, dataObject, campo, linhasCategorias);
  }
}

function obterSomaTotalCampoAtividade(atividade, campo) {
  return parseFloat(
    atividade.categorias
      .reduce(
        (soma, categoria) =>
          soma +
          categoria.rubricas.reduce(
            (soma, rubrica) => soma + rubrica[campo],
            0
          ),
        0
      )
      .toFixed(3)
  );
}

function obterSomaTotalCampoProjeto(projeto, campo) {
  return projeto.atividades.reduce(
    (soma, atividade) => soma + obterSomaTotalCampoAtividade(atividade, campo),
    0
  );
}

function obterSomaTotalCampoCategoria(atividade, categoria, campo) {
  const categorias = atividade.categorias,
    isSubCategoria = categoria.categoria_mae !== null,
    subCategorias = categorias.filter(
      cat => cat.categoria_mae === categoria.id
    ),
    rubricas = categoria.rubricas;

  return isSubCategoria
    ? rubricas.reduce((soma, rubrica) => soma + rubrica[campo], 0)
    : subCategorias.length > 0
      ? subCategorias.reduce(
          (acc, subCat) =>
            obterSomaTotalCampoCategoria(atividade, subCat, campo),
          0
        )
      : rubricas.reduce((soma, rubrica) => soma + rubrica[campo], 0);
}

function atualizaNomePlanilhas(source, row, col, newValue, hot, planilhas) {
  source === "edit" &&
    planilhas.forEach(
      ({ rootElement: { id }, setDataAtCell }) =>
        id !== hot.rootElement.id &&
        setDataAtCell(row, col, newValue.trim(), "afterChange")
    );
}

function carregarPlanilhaAtividade(atividade, i) {
  const linhasCategorias = [];
  const nomesCategorias = [];
  const linhasSubCategorias = [];
  const nomesSubCategorias = [];
  const categorias = atividade.categorias;
  let nLinhas = 0;

  var dataObject = categorias
    .map(categoria => {
      //juntar dados de categoria com as suas rubricas
      const { categoria_mae: categoriaMae, nome } = categoria,
        comprometido = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "comprometido"
        ),
        recebido = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "recebido"
        ),
        executado = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "executado"
        ),
        executado_manual = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "executado_manual"
        ),
        saldo = obterSomaTotalCampoCategoria(atividade, categoria, "saldo"),
        saldo_orcamentario = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "saldo_orcamentario"
        ),
        saldo_remanescente = obterSomaTotalCampoCategoria(
          atividade,
          categoria,
          "saldo_remanescente"
        );

      categoriaMae
        ? linhasSubCategorias.push(nLinhas++) && nomesSubCategorias.push(nome)
        : linhasCategorias.push(nLinhas++) && nomesCategorias.push(nome);

      return [
        {
          nome: categoria.nome,
          orcado: categoria.valor,
          comprometido: Number.isInteger(comprometido)
            ? comprometido
            : parseFloat(comprometido.toFixed(3)),
          recebido: Number.isInteger(recebido)
            ? recebido
            : parseFloat(recebido.toFixed(3)),
          executado: Number.isInteger(executado)
            ? executado
            : parseFloat(executado.toFixed(3)),
          executado_manual: Number.isInteger(executado_manual)
            ? executado_manual
            : parseFloat(executado_manual.toFixed(3)),
          executado_total: executado + executado_manual,
          saldo: Number.isInteger(saldo) ? saldo : parseFloat(saldo.toFixed(3)),
          saldo_orcamentario: Number.isInteger(saldo_orcamentario)
            ? saldo_orcamentario
            : parseFloat(saldo_orcamentario.toFixed(3)),
          saldo_remanescente: Number.isInteger(saldo_remanescente)
            ? saldo_remanescente
            : parseFloat(saldo_remanescente.toFixed(3)),
          novo_orcado: categoria.valor + saldo_remanescente
        }
      ].concat(
        categoria.rubricas.reduce((acc, rubrica) => {
          ++nLinhas;
          return acc.concat({
            nome: rubrica.nome,
            codigo: rubrica.codigo,
            orcado: rubrica.orcado ? parseFloat(rubrica.orcado.toFixed(3)) : 0,
            comprometido: rubrica.comprometido
              ? parseFloat(rubrica.comprometido.toFixed(3))
              : 0,
            recebido: rubrica.recebido
              ? parseFloat(rubrica.recebido.toFixed(3))
              : 0,
            executado: rubrica.executado
              ? parseFloat(rubrica.executado.toFixed(3))
              : 0,
            executado_manual: rubrica.executado_manual
              ? parseFloat(rubrica.executado_manual.toFixed(3))
              : 0,
            executado_total: executado + executado_manual,
            saldo: rubrica.saldo ? parseFloat(rubrica.saldo.toFixed(3)) : 0,
            saldo_orcamentario: rubrica.saldo_orcamentario
              ? parseFloat(rubrica.saldo_orcamentario.toFixed(3))
              : 0,
            saldo_remanescente: rubrica.saldo_remanescente
              ? parseFloat(rubrica.saldo_remanescente.toFixed(3))
              : 0,
            novo_orcado: rubrica.orcado + saldo_remanescente
          });
        }, [])
      );
    })
    .reduce((acc, categoria) => acc.concat(categoria), [])
    .concat({
      nome: 'TOTAL "' + atividade.nome + '"',
      orcado: atividade.orcado,
      novo_orcado:
        atividade.orcado +
        obterSomaTotalCampoAtividade(atividade, "saldo_remanescente"),
      comprometido: obterSomaTotalCampoAtividade(atividade, "comprometido"),
      recebido: obterSomaTotalCampoAtividade(atividade, "recebido"),
      executado: obterSomaTotalCampoAtividade(atividade, "executado"),
      executado_manual: obterSomaTotalCampoAtividade(
        atividade,
        "executado_manual"
      ),
      executado_total:
        obterSomaTotalCampoAtividade(atividade, "executado") +
        obterSomaTotalCampoAtividade(atividade, "executado_manual"),
      saldo: obterSomaTotalCampoAtividade(atividade, "saldo"),
      saldo_orcamentario: obterSomaTotalCampoAtividade(
        atividade,
        "saldo_orcamentario"
      ),
      saldo_remanescente: obterSomaTotalCampoAtividade(
        atividade,
        "saldo_remanescente"
      )
    })
    .concat({
      nome: 'TOTAL "' + projeto.nome + '"',
      orcado: projeto.orcado,
      novo_orcado:
        projeto.orcado +
        obterSomaTotalCampoProjeto(projeto, "saldo_remanescente"),
      comprometido: obterSomaTotalCampoProjeto(projeto, "comprometido"),
      recebido: obterSomaTotalCampoProjeto(projeto, "recebido"),
      executado: obterSomaTotalCampoProjeto(projeto, "executado"),
      executado_manual: obterSomaTotalCampoProjeto(projeto, "executado_manual"),
      executado_total:
        obterSomaTotalCampoProjeto(projeto, "executado") +
        obterSomaTotalCampoProjeto(projeto, "executado_manual"),
      saldo: obterSomaTotalCampoProjeto(projeto, "saldo"),
      saldo_orcamentario: obterSomaTotalCampoProjeto(
        projeto,
        "saldo_orcamentario"
      ),
      saldo_remanescente: obterSomaTotalCampoProjeto(
        projeto,
        "saldo_remanescente"
      )
    });

  let doubleClick = false;
  var hotSettings = {
    data: dataObject,
    columns: colunas,
    stretchH: "all",
    cells: function(row, col, nomeColuna) {
      const cellProperties = {};
      if (linhasCategorias.indexOf(row) !== -1) {
        cellProperties.isCategoria = true;
        cellProperties.nomeCategoria =
          nomesCategorias[linhasCategorias.indexOf(row)];
        cellProperties.renderer = function(instance, td) {
          Handsontable.renderers.TextRenderer.apply(this, arguments);
          td.style.background = "#71a4f7";
          td.style.fontWeight = "bold";

          if (col > 1) td.style.textAlign = "right";
        };
      }

      if (linhasSubCategorias.indexOf(row) !== -1) {
        cellProperties.isCategoria = true;
        cellProperties.nomeCategoria =
          nomesSubCategorias[linhasSubCategorias.indexOf(row)];
        cellProperties.renderer = function(instance, td) {
          Handsontable.renderers.TextRenderer.apply(this, arguments);
          td.style.background = "#a8c9ff";
          td.style.fontWeight = "bold";

          if (col > 1) td.style.textAlign = "right";
        };
      }

      if (row < dataObject.length - 1) {
        if (
            nomeColuna.toLowerCase() === "recebido" ||
            nomeColuna.toLowerCase() === "saldo_orcamentario"
        )
          cellProperties.readOnly = true;
      }

      if (row < dataObject.length - 2) {
        if (
          linhasCategorias.includes(row) ||
          linhasSubCategorias.includes(row)
        ) {
          if (
            nomeColuna.toLowerCase() === "comprometido" ||
            nomeColuna.toLowerCase() === "executado_manual" ||
            nomeColuna.toLowerCase() === "saldo_orcamentario" ||
            nomeColuna.toLowerCase() === "saldo_remanescente"
          ) {
            cellProperties.readOnly = true;
          }
        }
      } else {
        if (
          nomeColuna.toLowerCase() === "comprometido" ||
          nomeColuna.toLowerCase() === "executado_manual" ||
          nomeColuna.toLowerCase() === "saldo_orcamentario" ||
          nomeColuna.toLowerCase() === "saldo_remanescente"
        ) {
          cellProperties.readOnly = true;
        }

        cellProperties.renderer = function(instance, td) {
          Handsontable.renderers.TextRenderer.apply(this, arguments);
          if (row === dataObject.length - 1)
            //ultimo
            td.style.background = "#4286f4";
          //penultimo
          else td.style.background = "#71a4f7";

          td.style.fontWeight = "bold";
          if (col > 1) td.style.textAlign = "right";
        };
      }

      if (
        nomeColuna.toLowerCase() === "executado" ||
        nomeColuna.toLowerCase() === "saldo" ||
//        nomeColuna.toLowerCase() === "codigo" ||
        nomeColuna.toLowerCase() === "novo_orcado" ||
        nomeColuna.toLowerCase() === "executado_total"
      )
        cellProperties.readOnly = true;

      return cellProperties;
    },
    afterChange: (changes, source) => {
      if (!changes) return;
      changes.forEach(([row, prop, oldValue, newValue]) => {
        const col = hot.propToCol(prop),
          cellMeta = hot.getCellMeta(row, col),
          penultimaLinha = row === dataObject.length - 2,
          ultimaLinha = row === dataObject.length - 1,
          linhaCategoriaGeral = linhasCategorias.reduce(
            (acc, curr) => (curr <= row ? curr : acc),
            0
          ),
          linhaSubCategoria = linhasSubCategorias.reduce(
            (acc, curr) => (curr <= row ? curr : acc),
            0
          ),
          isSubCategoria = linhaSubCategoria > linhaCategoriaGeral,
          linhaCategoria = isSubCategoria
            ? linhaSubCategoria
            : linhaCategoriaGeral,
          iCategoria = isSubCategoria
            ? linhasSubCategorias.indexOf(linhaCategoria)
            : linhasCategorias.indexOf(linhaCategoria);

        const idTabela = hot.rootElement.id,
          indicePrimeiroTraco = idTabela.match(/(\d)*-/).index,
          idPeriodoAtividade = idTabela
            .substr(indicePrimeiroTraco + 1)
            .split("-"),
          periodoId = parseInt(idPeriodoAtividade[0], 10),
          codAtividade = idPeriodoAtividade[1].toLowerCase(),
          atividade = projeto.atividades.find(
            atv =>
              atv.periodo_id === periodoId &&
              atv.codigo.toLowerCase() === codAtividade
          ),
          subCategorias = atividade.categorias.filter(
            cat => cat.categoria_mae !== null
          ),
          categorias = atividade.categorias.filter(
            cat => cat.categoria_mae === null
          ),
          categoria = isSubCategoria
            ? subCategorias[iCategoria]
            : categorias[iCategoria],
          iRubrica = row - linhaCategoria - 1 >= categoria.rubricas.length ? -1 : row - linhaCategoria - 1;

        if (cellMeta.type === "numeric" && isNaN(parseFloat(newValue))) return;
        if (prop === "recebido" && source === "edit") {
          //se mudanças forem feitas na coluna "Recebido" da última linha
          modificaValoresSaldo(hot, row);
          if (row === dataObject.length - 1) {
            modificaValoresRecebidoPlanilhas(
              [row, prop, oldValue, newValue],
              planilhas
            );
          }
        } else if (prop === "comprometido") {
          modificaValoresSaldo(hot, row);
          modificaValoresSaldoOrcamentario(hot, row, dataObject);

          if (!ultimaLinha) {
            modificaTotais(
              prop,
              hot,
              row,
              dataObject,
              planilhas,
              iCategoria,
              linhaCategoria,
              linhaCategoriaGeral,
              linhasCategorias,
              linhasSubCategorias,
              newValue
            );
          }
        } else if (prop === "executado") {
          modificaValoresSaldo(hot, row);
          modificaValoresSaldoOrcamentario(hot, row, dataObject);
          modificaValoresExecutadoTotal(hot, row, dataObject);
          if (!ultimaLinha) {
            modificaTotais(
              prop,
              hot,
              row,
              dataObject,
              planilhas,
              iCategoria,
              linhaCategoria,
              linhaCategoriaGeral,
              linhasCategorias,
              linhasSubCategorias,
              newValue
            );
          }
        } else if (prop === "executado_manual") {
          modificaValoresExecutadoTotal(hot, row, dataObject);
          if (!ultimaLinha) {
            modificaTotais(
              prop,
              hot,
              row,
              dataObject,
              planilhas,
              iCategoria,
              linhaCategoria,
              linhaCategoriaGeral,
              linhasCategorias,
              linhasSubCategorias,
              newValue
            );
          }
        } else if (prop === "orcado") {
          modificaValoresSaldoOrcamentario(hot, row, dataObject);
          modificaValoresNovoOrcado(hot, row);
        } else if (prop === "saldo_remanescente") {
          modificaValoresNovoOrcado(hot, row);
          if (!ultimaLinha) {
            modificaTotais(
              prop,
              hot,
              row,
              dataObject,
              planilhas,
              iCategoria,
              linhaCategoria,
              linhaCategoriaGeral,
              linhasCategorias,
              linhasSubCategorias,
              newValue
            );
          }
        } else if (prop === "novo_orcado") {
          if (!ultimaLinha) {
            modificaTotais(
              prop,
              hot,
              row,
              dataObject,
              planilhas,
              iCategoria,
              linhaCategoria,
              linhaCategoriaGeral,
              linhasCategorias,
              linhasSubCategorias,
              newValue
            );
          }
        }

        if (penultimaLinha) {
          if (prop === "orcado") {
            atividade.orcado = parseFloat(parseFloat(newValue).toFixed(3));
          }
          return;
        } else if (ultimaLinha) {
          if (prop === "orcado") {
            projeto.orcado = parseFloat(parseFloat(newValue).toFixed(3));
            source === "edit" &&
              planilhas.forEach(
                ({ rootElement: { id }, setDataAtCell }) =>
                  id !== hot.rootElement.id &&
                  setDataAtCell(
                    row,
                    col,
                    parseFloat(newValue).toFixed(3),
                    "afterChange"
                  )
              );
          }
          return;
        }
        //mudanças foram feitas em uma linha de rubrica
        if (iRubrica !== -1) {
          if (prop === "nome") {
            atualizaNomePlanilhas(source, row, col, newValue, hot, planilhas);

            projeto.atividades.forEach(atividade =>
              atividade.categorias.forEach(
                (categoria, i) =>
                  i === iCategoria &&
                  (categoria.rubricas[iRubrica].nome = newValue.trim())
              )
            );

            return;
          }
          if (prop === "codigo"){
            atualizaNomePlanilhas(source, row, col, newValue, hot, planilhas);

            projeto.atividades.forEach(atividade =>
              atividade.categorias.forEach(
                (categoria, i) =>
                  i === iCategoria &&
                  (categoria.rubricas[iRubrica].codigo = newValue.trim())
              )
            );

            return;
          }

          const rubrica = categoria.rubricas[iRubrica];
          rubrica[prop] = parseFloat(parseFloat(newValue).toFixed(3));
        }
        //mudanças foram feitas em uma linha de categoria
        else {
          if (prop === "orcado") {
            categoria.valor = parseFloat(parseFloat(newValue).toFixed(3));
          } else if (prop === "comprometido") {
            categoria.comprometido = parseFloat(
              parseFloat(newValue).toFixed(3)
            );
          } else if (prop === "nome") {
            atualizaNomePlanilhas(source, row, col, newValue, hot, planilhas);

            projeto.atividades.forEach(
              atividade =>
                (atividade.categorias[iCategoria].nome = newValue.trim())
            );
          }
        }
      });
    },
    afterOnCellMouseDown: function(e, coords, tdElem) {
      setTimeout(() => {
        doubleClick = false;
      }, 200);

      if (doubleClick) {
        const { row, col } = coords,
          { readOnly, prop } = hot.getCellMeta(row, col),
          isPropValida =
            prop === "orcado" ||
            prop === "comprometido" ||
            prop === "executado_manual" ||
            prop === "novo_orcado" ||
            prop === "saldo_remanescente" ||
            prop === "saldo_orcamentario";

        if (readOnly || !isPropValida) {
          return;
        }
        const modalElem = $("#addCelulaModal");

        modalElem.data("row", row);
        modalElem.data("col", col);

        return modalElem.modal("toggle");
      }
      doubleClick = true;
    },
    // stretchH: 'all',
    width: `calc(${document.body.clientWidth} - 10%)`,
    height: 26 + dataObject.length * 24.75,
    autoWrapRow: true,
    manualRowResize: true,
    manualColumnResize: true,
    defaultRowHeight: 200,
    rowHeaders: false,
    colHeaders: nomesColunas,
    manualRowMove: true,
    manualColumnMove: true,
    contextMenu: false,
    filters: true,
    dropdownMenu: true
  };

  const periodo_id = atividade.periodo_id;
  const codAtividade = atividade.codigo;
  const hotElement = document.getElementById(
    "hot-" + periodo_id + "-" + codAtividade.toLowerCase()
  );
  const hot = new Handsontable(hotElement, hotSettings, (row, col) => {
    const cellProperties = {};

    if (row === 0) {
      cellProperties.renderer = (
        instance,
        td,
        row,
        col,
        prop,
        value,
        cellProperties
      ) => {
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        td.style.fontWeight = "bold";
      };
    }

    return cellProperties;
  });

  return hot;
}

function criarDropdownBtnAtividade() {
  const dropdownElem = document.createElement("div"),
    items = [
      ["Criar novo período", rotaCriarPeriodo],
      ["Criar nova atividade", rotaCriarAtividade],
      ["Criar nova categoria", rotaCriarCategoria]
      // [ 'Criar nova rubrica', rotaCriarRubrica ]
    ],
    itemsElems = items.map(([text, link]) => {
      const elem = document.createElement("a");

      elem.className = "dropdown-item";
      elem.innerHTML = text;
      elem.href = link;
      elem.onclick = () => window.open(link, "_blank");

      return elem;
    });

  dropdownElem.className = "dropdown-menu";

  Array.from(itemsElems).forEach(item => dropdownElem.appendChild(item));

  return dropdownElem;
}

function verificaErroOrcado(orcadoCalculado, orcado, erroMax) {
  return Math.abs(orcadoCalculado - orcado) > erroMax;
}

function verificaErroOrcadoRubricas(
  atividade,
  nomeCategoria,
  rubricas,
  orcado,
  erroMax
) {
  return verificaErroOrcado(
    rubricas.reduce((soma, rub) => soma + rub.orcado, 0),
    orcado,
    erroMax
  )
    ? [
        `A soma dos orçados das rubricas não é igual ao valor orçado da categoria ${nomeCategoria} para a atividade ${
          atividade.nome
        }!`
      ]
    : [];
}

function verificaErroOrcadoSubcategorias(
  atividade,
  subCategorias,
  nomeCategoria,
  orcado,
  erroMax
) {
  return verificaErroOrcado(
    subCategorias.reduce((soma, { valor }) => soma + valor, 0),
    orcado,
    erroMax
  )
    ? [
        `A soma dos orçados das subcategorias não é igual ao valor orçado da categoria ${nomeCategoria} para a atividade ${
          atividade.nome
        }!`
      ]
    : [];
}

function verificarOrcamentosCategorias(atividade, categorias, erroMax) {
  return categorias.reduce((erros, { id, rubricas, nome, valor: orcado }) => {
    const subCategorias = categorias.filter(cat => cat.categoria_mae === id);

    return subCategorias.length === 0
      ? erros.concat(
          verificaErroOrcadoRubricas(atividade, nome, rubricas, orcado, erroMax)
        )
      : erros
          .concat(
            verificaErroOrcadoSubcategorias(
              atividade,
              subCategorias,
              nome,
              orcado,
              erroMax
            )
          )
          .concat(
            subCategorias.reduce(
              (erros, { orcado }) =>
                erros.concat(
                  verificaErroOrcadoRubricas(
                    atividade,
                    nome,
                    rubricas,
                    orcado,
                    erroMax
                  )
                ),
              []
            )
          );
  }, []);
}

function verificarOrcamentosProjeto(projeto, erroMax) {
  const { atividades, orcado } = projeto;
  const somaOrcadoAtividades = atividades.reduce(
    (soma, atv) => soma + atv.orcado,
    0
  );
  const erros = [];

  if (Math.abs(somaOrcadoAtividades - orcado) > erroMax) {
    erros.push(
      "A soma dos orçados das atividades não é igual ao valor orçado do projeto!"
    );
  }

  return erros.concat(
    atividades.reduce(
      (erros, atividade) =>
        (Math.abs(
          atividade.categorias
            .filter(categoria => !categoria.categoria_mae)
            .reduce((soma, cat) => soma + cat.valor, 0) - atividade.orcado
        ) > erroMax
          ? erros.concat(
              `A soma dos orçados das categorias não é igual ao valor orçado da atividade ${
                atividade.nome
              }!`
            )
          : erros
        ).concat(
          verificarOrcamentosCategorias(
            atividade,
            atividade.categorias,
            0.000001
          )
        ),
      []
    )
  );
}

function main(projeto) {
  if (!projeto) return;

  document
    .getElementById("formDadosPlanilhas")
    .addEventListener("submit", e => {
      e.preventDefault();

      const erros = verificarOrcamentosProjeto(projeto, 0.000001);
      if (erros.length > 0) {
        const modal = $("#verificacaoOrcamentoModal");
        modal
          .find(".lista-erros")
          .html(
            erros
              .map(erro => `<li>${erro}</li>`)
              .reduce((acc, erro) => acc + erro, "")
          );
        modal.modal();
        return;
      }

      const form = e.target,
        inputJSON = form.querySelector('input[name="planilhas"]');

      inputJSON.value = JSON.stringify(projeto);
      form.submit();
    });

  const planilhasContainer = document.getElementById("planilhasContainer"),
    tabsContainer = document.getElementById("tabsContainer");

  projeto.atividades.forEach((atividade, i) => {
    const planilhaContainer = document.createElement("div"),
      tab = document.createElement("li"),
      tabAnchor = document.createElement("a"),
      periodo = projeto.periodos.find(
        periodo => periodo.id === atividade.periodo_id
      );
    planilhaContainer.id =
      "hot-" + atividade.periodo_id + "-" + atividade.codigo.toLowerCase();

    planilhaContainer.className = "tab-pane planilha-container";
    planilhaContainer.setAttribute("role", "tabpanel");

    const nomeAMostrar =
        projeto.nome +
        " - " +
        atividade.nome +
        (periodo.data_fim_real ? "*" : ""),
      btnAtividade = document.createElement("button");

    btnAtividade.className = "btn btn-sm btn-light btn-detalhes-atividade";
    btnAtividade.setAttribute("data-toggle", "dropdown");
    btnAtividade.innerHTML = "...";

    tab.className = "nav-item";
    tabAnchor.innerHTML = nomeAMostrar;

    tabAnchor.id =
      "hot-" +
      atividade.periodo_id +
      "-" +
      atividade.codigo.toLowerCase() +
      "-tab";
    tabAnchor.className = "nav-link";
    tabAnchor.setAttribute(
      "href",
      (
        "#hot-" +
        atividade.periodo_id +
        "-" +
        atividade.codigo.toLowerCase()
      ).replace(".", "\\.")
    );
    tabAnchor.setAttribute("role", "tab");
    tabAnchor.setAttribute("data-toggle", "tab");

    if (i === 0) {
      tabAnchor.className += " active";
      planilhaContainer.className += " show active";
    }

    planilhasContainer.appendChild(planilhaContainer);
    tab.appendChild(tabAnchor);
    tabsContainer.appendChild(tab);
  });

  const planilhas = projeto.atividades.map(carregarPlanilhaAtividade);

  carregarBotoesAddRubrica("botoesPlanilhaContainer", planilhas[0]);

  return planilhas;
}

function obterAtividadeDeCategoria(atividades, categoriaId) {
  const { atividade_id } = encontraCategoriaAtividades(categoriaId, atividades);
  const atividade = atividades.find(atv => atv.id === atividade_id);

  return atividade;
}

function criarInputNomeValor(nome, valor) {
  const inputElem = document.createElement("input");

  inputElem.type = "hidden";
  inputElem.name = nome;
  inputElem.value = valor;

  return inputElem;
}

function enviarArquivoPlanilha(base64Data) {
  const form = document.createElement("form");
  const fileDataInput = criarInputNomeValor("base64Data", base64Data);
  const csrfTokenInput = criarInputNomeValor("_token", window.token.content);

  form.method = "POST";
  form.action = window.rotaEnviarArquivo;
  form.appendChild(fileDataInput);
  form.appendChild(csrfTokenInput);

  document.body.appendChild(form);
  form.submit();
}

function obterWorkbookPlanilhaProjeto() {
  const workbook = XLSX.utils.book_new(),
    cabecalho = nomesColunas,
    worksheet = XLSX.utils.aoa_to_sheet(
      planilhas
        .map((planilha, i) =>
          [
            [
              projeto.atividades[i].nome,
              ...Array(cabecalho.length - 1).fill(" ")
            ]
          ].concat([cabecalho], planilha.getData())
        )
        .reduce(
          (result, atvData) =>
            result.length === 0
              ? atvData.map(x => x)
              : result.map((data, i) => data.concat([" "], atvData[i])),
          []
        )
    );

  XLSX.utils.book_append_sheet(workbook, worksheet, projeto.nome);

  return workbook;
}
//------------------------------------------------------------------------------------

planilhas = main(projeto);
const modalFinalizarPeriodo = $("#finalizarPeriodoModal");

$('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
  const id = e.target.id,
    planilha = obterPlanilhaDeId(id);

  planilha.render();
  // planilha.rootElement.style.overflowX = "scroll";
});

//exportar planilha do projeto
$("#exportarPlanilhaProjeto").click(function(e) {
  const workbook = obterWorkbookPlanilhaProjeto();
  const nomeArquivo = projeto.nome.replace(" ", "-") + ".xlsx";
  XLSX.writeFile(workbook, nomeArquivo);
});

$("#exportarPlanilhaProjetoPDF").click(function(e) {
  const workbook = obterWorkbookPlanilhaProjeto();

  const base64Data = XLSX.write(workbook, {
    bookType: "xlsx",
    bookSST: false,
    type: "base64"
  });

  enviarArquivoPlanilha(base64Data);
});

function obterWorkbookPlanilhaDetalhada() {
  const { registrosPlanilha, atividades } = projeto;

  if (
    !registrosPlanilha ||
    (Array.isArray(registrosPlanilha) && registrosPlanilha.length === 0)
  ) {
    $("#atencaoSemRegistrosModal").modal();
    return;
  }

  const workbook = XLSX.utils.book_new(),
    cabecalho = [
      "Cód. Classificação",
      "Emissão",
      "Baixa",
      "Número",
      "Cód. Gerencial",
      "Gerencial",
      "Classificação",
      "Sacado/Cedente",
      "Histórico",
      "Valor",
      "Composição"
    ],
    data = registrosPlanilha.map(
      ({
        emissao,
        baixa,
        numero,
        gerencial,
        sacado_cedente,
        historico,
        valor,
        composicao,
        rubrica
      }) => {
        const {
          codigo,
          categoria_id: categoriaId,
          nome: classificacao
        } = rubrica;
        const { codigo: codAtividade } = obterAtividadeDeCategoria(
          atividades,
          categoriaId
        );

        return [
          codigo,
          moment(emissao, "YYYY-MM-DD").format("DD/MM/YYYY"),
          moment(baixa, "YYYY-MM-DD").format("DD/MM/YYYY"),
          numero,
          codAtividade,
          gerencial,
          classificacao,
          sacado_cedente,
          historico,
          valor,
          composicao
        ];
      }
    ),
    worksheet = XLSX.utils.aoa_to_sheet([cabecalho].concat(data));

  XLSX.utils.book_append_sheet(
    workbook,
    worksheet,
    projeto.nome.replace(" ", "-")
  );

  return workbook;
}

//exportar planilha detalhada
$("#exportarPlanilhaDetalhada").click(function(e) {
  const nomeArquivo = `${projeto.nome} - detalhada`.replace(" ", "-") + ".xlsx";

  const workbook = obterWorkbookPlanilhaDetalhada();
  XLSX.writeFile(workbook, nomeArquivo);
});

$("#exportarPlanilhaDetalhadaPDF").click(function(e) {
  const workbook = obterWorkbookPlanilhaDetalhada();
  const base64Data = XLSX.write(workbook, {
    bookType: "xlsx",
    bookSST: false,
    type: "base64"
  });

  enviarArquivoPlanilha(base64Data);
});

$(".modificarValorCelula").click(e => {
  const target = e.target,
    idElem = target.id,
    inputElem = target.parentElement.querySelector("input"),
    novoValor = parseFloat(inputElem.value.replace(",", "."));

  if (isNaN(novoValor)) return;

  const modalElem = $("#addCelulaModal"),
    data = modalElem.data(),
    planilhaElem = $(".tab-pane.active")[0],
    planilha = planilhas.find(plan => plan.rootElement.id === planilhaElem.id),
    row = data.row,
    col = data.col,
    valorAtualCelula = isNaN(parseFloat(planilha.getDataAtCell(row, col)))
      ? 0
      : parseFloat(planilha.getDataAtCell(row, col)),
    novoValorCelula =
      idElem === "adicionarValorCelula"
        ? valorAtualCelula + novoValor
        : idElem === "subtrairValorCelula"
          ? valorAtualCelula - novoValor
          : valorAtualCelula;

  planilha.setDataAtCell(row, col, novoValorCelula.toFixed(3));
});

$("#editarAplicacao").click(e => {
  const modalElem = $("#editarAplicacaoModal");
  modalElem.data("tipo", "editarAplicacao");
  modalElem.modal();
});

$('input[name="aplicacao"]').change(e => (projeto.aplicacao = e.target.value));

$('input[name="rendimento"]').change(
  e => (projeto.rendimento = e.target.value)
);

$("#editarRendimento").click(e => {
  const modalElem = $("#editarAplicacaoModal");
  modalElem.data("tipo", "editarRendimento");
  modalElem.modal();
});

$(".modificarValorAplicacao").click(e => {
  const target = e.target,
    idElem = target.id,
    modalElem = $("#editarAplicacaoModal"),
    data = modalElem.data(),
    inputElem = target.parentElement.querySelector("input"),
    inputValorElem = document
      .getElementById(data.tipo)
      .parentNode.querySelector("input"),
    valorAtual =
      inputValorElem.value === "" ? 0 : parseFloat(inputValorElem.value),
    valorInput = parseFloat(inputElem.value.replace(",", "."));

  if (isNaN(valorInput)) return;

  const novoValor =
    idElem === "adicionarValorBtn"
      ? valorAtual + valorInput
      : idElem === "subtrairValorBtn"
        ? valorAtual - valorInput
        : valorAtual;

  const changeEvent = document.createEvent("Event");
  changeEvent.initEvent("change", true, false);
  inputValorElem.value = novoValor;
  inputValorElem.dispatchEvent(changeEvent);
});

$("#finalizarPeriodo").click(e => {
  const tabAtividadeSelecionada = $('a[data-toggle="tab"].active')[0],
    idTab = tabAtividadeSelecionada.id,
    idPeriodoSelecionado = obterIdPeriodoDeIdTab(idTab),
    periodo = projeto.periodos.find(
      periodo => periodo.id === idPeriodoSelecionado
    );

  modalFinalizarPeriodo.data("periodo_id", periodo.id);
  modalFinalizarPeriodo.data("periodo_nome", periodo.nome);
});

modalFinalizarPeriodo.on("show.bs.modal", function() {
  const data = $(this).data();
  const { periodo_nome, periodo_id } = data;
  const periodo = projeto.periodos.find(({ id }) => id === periodo_id);
  const contentContainer = $(this).find(".content-finalizar-periodo");
  const erroJaFinalizado = $(this).find(".erro-ja-finalizado");
  const botoesFinalizarModalContainer = $(this).find(".botoes-finalizar-modal");
  const botaoOkModal = $(this).find(".botao-ok");

  $(this)
    .find(".nome-periodo")
    .html(periodo_nome);

  if (periodo.data_fim_real) {
    // Mostrar erro no modal e esconder conteúdo principal
    erroJaFinalizado.removeClass("d-none");
    contentContainer.addClass("d-none");
    botoesFinalizarModalContainer.addClass("d-none");
    botaoOkModal.removeClass("d-none");
    return;
  }

  // Se não, mostrar conteúdo principal
  erroJaFinalizado.addClass("d-none");
  contentContainer.removeClass("d-none");
  botoesFinalizarModalContainer.removeClass("d-none");
  botaoOkModal.addClass("d-none");
});

modalFinalizarPeriodo.find(".btnFinalizarPeriodo").click(function() {
  const data = modalFinalizarPeriodo.data(),
    { periodo_id } = data,
    form = modalFinalizarPeriodo.find("form")[0];

  form.action = `/periodos/${periodo_id}/finalizar`;
  modalFinalizarPeriodo.append(form);

  form.submit();
});

$("#verificacaoOrcamentoModal .btn-salvar").click(e => {
  const form = document.getElementById("formDadosPlanilhas");
  const inputJSON = form.querySelector('input[name="planilhas"]');

  inputJSON.value = JSON.stringify(projeto);
  form.submit();
});
