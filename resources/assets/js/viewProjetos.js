import moment from "moment";
window.moment = moment;

//@ts-check
$('[data-toggle="tooltip"]').tooltip();

const token = document
  .querySelector('[name="csrf-token"]')
  .getAttribute("content");

const criarInputHidden = (nome, valor) => {
  const inputMetodo = document.createElement("input");
  inputMetodo.type = "hidden";
  inputMetodo.name = nome;
  inputMetodo.value = valor;

  return inputMetodo;
};

const criarFormElemEditavel = (action, token) => {
  const form = document.createElement("form");

  form.action = action;
  form.method = "POST";
  form.appendChild(criarInputHidden("_method", "PUT"));
  form.appendChild(criarInputHidden("_token", token));

  return form;
};

const obterTipoInputDeTipo = tipo => {
  switch (tipo) {
    case "numero":
      return "number";
    case "data":
      return "date";
    case "senha":
      return "password";
    case "email":
      return "email";
    default:
      return "text";
  }
};

const obterTipoItemDeClasses = classes => {
  const match = classes.match(/tipo-item.*(\s|$)/g);

  if (!match) {
    return "text";
  }

  const tipo = match[0].trim().replace("tipo-item-", "");

  return obterTipoInputDeTipo(tipo);
};

const criarItemsInput = itensEditaveis =>
  itensEditaveis.map(item => {
    const input = document.createElement("input");
    const tipoItem = obterTipoItemDeClasses(item.className);
    const textoElem = item.querySelector(".item-form-editavel-texto");

    const width =
      textoElem.clientWidth || textoElem.getBoundingClientRect().width;
    input.style.width =
      tipoItem === "text"
        ? `${width}px`
        : tipoItem === "number"
          ? `calc(${width}px + 15%)`
          : `calc(${width}px + 25%)`;
	console.log('textoElem: ',textoElem.innerText,tipoItem);
    input.name = item.dataset.nome;
    input.type = tipoItem || "text";
    input.step = "any";
    input.value = textoElem
      ? tipoItem === "date"
        ? moment(textoElem.innerText.trim(), "DD/MM/YYYY").format("YYYY-MM-DD")
        : tipoItem === "number"
          ? textoElem.innerText.replace(".", "").replace(",", ".")
          : textoElem.innerText.trim()
      : "";

    return input;
  });

const criaEEnviaForm = (action, token, itensInput) => {
  const form = criarFormElemEditavel(action, token);
  itensInput
    .map(item => {
      const novoInput = document.createElement("input");

      novoInput.type = "hidden";
      novoInput.name = item.name;
      novoInput.value = item.value;

      return novoInput;
    })
    .forEach(item => form.appendChild(item));

  document.body.appendChild(form);

  console.log(form);
	form.submit();
};

const iniciarEdicao = itensEditaveis => {
  const inputItems = criarItemsInput(itensEditaveis);
  itensEditaveis.forEach((itemEditavel, i) =>
    itemEditavel.appendChild(inputItems[i])
  );
};

const finalizarEdicao = (token, trElem, itensEditaveis) => {
  const itensInput = itensEditaveis.map(item => item.querySelector("input"));
  const { rotaAtualizar } = trElem.dataset;

  itensInput.forEach(item => item.remove());

  criaEEnviaForm(rotaAtualizar, token, itensInput);
};

const cancelarEdicao = (e, btnElem, itensEditaveis) => {
  const btnElems = e.currentTarget.parentElement.querySelectorAll(
    ".btn-confirmacao-edicao"
  );
  const itensInput = itensEditaveis.map(item => item.querySelector("input"));

  itensInput.forEach(item => item.remove());
  btnElems.forEach(item => item.remove());
  btnElem.classList.toggle("d-none");

  itensEditaveis.forEach(itemEditavel => {
    const textoElem = itemEditavel.querySelector(".item-form-editavel-texto");
    if (textoElem) {
      textoElem.classList.toggle("d-none");
    }
  });
};

const criarBotaoConfirmacao = (
  innerHTML,
  textoTooltip,
  className,
  handleClick
) => {
  const btn = document.createElement("button");
  btn.className = `btn btn-sm btn-confirmacao-edicao ${className}`;
  btn.setAttribute("data-toggle", "tooltip");
  btn.setAttribute("title", textoTooltip);
  btn.addEventListener("click", e => {
    e.preventDefault();
    handleClick(e);
  });

  btn.innerHTML = innerHTML;

  return btn;
};

const criarBotoesConfirmacaoCancelamento = (
  btnElem,
  trElem,
  itensEditaveis
) => {
  btnElem.classList.toggle("d-none");
  const iconeAceitar = ` <i class="fa fa-check" aria-hidden="true"></i>`;
  const iconeRecusar = ` <i class="fa fa-times" aria-hidden="true"></i>`;
  btnElem.parentElement.insertBefore(
    criarBotaoConfirmacao(
      iconeAceitar,
      "Confirmar edição",
      "btn-aceitar-edicao",
      _ => finalizarEdicao(token, trElem, itensEditaveis)
    ),
    btnElem
  );

  btnElem.parentElement.insertBefore(
    criarBotaoConfirmacao(
      iconeRecusar,
      "Cancelar edição",
      "btn-recusar-edicao",
      e => cancelarEdicao(e, btnElem, itensEditaveis)
    ),
    btnElem
  );
};

function onToggleFormEditavel(e) {
  e.preventDefault();

  const formEditavelElem = e.target.closest(".form-editavel-elem");
  const itensEditaveis = Array.from(
    formEditavelElem.getElementsByClassName("item-form-editavel")
  );

  criarBotoesConfirmacaoCancelamento(e.currentTarget, formEditavelElem, itensEditaveis);

  iniciarEdicao(itensEditaveis);

  formEditavelElem.classList.toggle("ativo");

  itensEditaveis.forEach(itemEditavel => {
    const textoElem = itemEditavel.querySelector(".item-form-editavel-texto");
    if (textoElem) {
      textoElem.classList.toggle("d-none");
    }
  });
}

Array.from(document.getElementsByClassName("alternar-form-editavel")).forEach(
  elem => elem.addEventListener("click", onToggleFormEditavel)
);

//===============================================================
const config = {
  attributes: true,
  attributeOldValue: true,
  characterData: true,
  characterDataOldValue: true,
  childList: true,
  subtree: true
};

function subscriber(mutations) {
  mutations.forEach(({ addedNodes, removedNodes }) => {
    if (addedNodes.length) {
      if (
        Array.from(addedNodes).filter(
          elem => elem.dataset && elem.dataset.toggle === "tooltip"
        ).length
      ) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    }
    if (removedNodes.length) {
      const tooltipRemovedNodes = Array.from(removedNodes).filter(
        elem => elem.dataset && elem.dataset.toggle === "tooltip"
      );
      if (tooltipRemovedNodes.length) {
        $(tooltipRemovedNodes).tooltip("hide");
      }
    }
  });
}

const observer = new MutationObserver(subscriber);

observer.observe(document.body, config);
