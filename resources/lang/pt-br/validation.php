<?php

return [
	'min'			=> [
        'numeric' => 'O campo :attribute deve ser no mínimo :min.',
        'file'    => 'O campo :attribute precisa ter tamanho mínimo de :min kilobytes.',
        'string'  => 'O campo :attribute precisa ter no mínimo :min caracteres.',
        'array'   => 'O campo :attribute precisa te no mínimo :min items.',
	],
    'email'         => 'O campo :attribute precisa ser um endereço de email válido.',
    'required'		=> 'O campo :attribute é obrigatório.',
    'confirmed'     => 'A confirmação do campo :attribute não confere.',
    'unique'        => 'O campo :attribute já está em uso. Favor escolher outro.',
    'gte'           => [
        'numeric' => 'O campo :attribute precisa ser maior ou igual a :value.',
    ],
    'date'          => 'O campo :attribute não é uma data válida.',
];
