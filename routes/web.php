<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', ['as' => 'login', 'uses' => 'HomeController@show_login']);
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@show_home'])->middleware('auth');
Route::post('/login', 'HomeController@login');
Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);

//usuarios
Route::get('/usuarios/criar', [
    'as' => 'usuarios.criar', 'uses' => 'UsuarioController@create'
])->middleware('auth');

Route::post('/usuarios', [
    'as' => 'usuarios.salvar', 'uses' => 'UsuarioController@store'
])->middleware('auth');

//projetos
//==============================================================================================
Route::get('/projetos/{id}/planilha/editar', [
    'as' => 'projeto.planilha.editar', 'uses' => 'ProjetoController@planilha_edit'
])->middleware('auth');

Route::get('/projeto/criar', [
    'as' => 'projeto.criar', 'uses' => 'ProjetoController@create'
])->middleware('auth');

Route::post('/projeto', [
    'as' => 'projeto.salvar', 'uses' => 'ProjetoController@store'
])->middleware('auth');

Route::put('/projeto/{id}', [
    'as' => 'projeto.atualizar', 'uses' => 'ProjetoController@update'
])->middleware('auth');

Route::post('/projeto/aplicacao', [
    'as' => 'projeto.atualizarAplicacao', 'uses' => 'ProjetoController@atualizaAplicacao'
])->middleware('auth');

Route::put('/projeto/{id}/planilha', [
    'as' => 'projeto.planilha.atualizar', 'uses' => 'ProjetoController@planilha_update'
])->middleware('auth');

Route::get('/projeto/view', [
    'as' => 'projeto.view', 'uses' => 'ProjetoController@view'
])->middleware('auth');

Route::get('/projetos/{id}/finalizar', [
    'as' => 'projeto.finalizar', 'uses' => 'ProjetoController@finalizar'
])->middleware('auth');

Route::get('/projetos/{id}/reativar', [
    'as' => 'projeto.reativar', 'uses' => 'ProjetoController@reativar'
])->middleware('auth');

Route::get('/projetos/{id}/deletar', [
    'as' => 'projeto.deletar', 'uses' => 'ProjetoController@deletar'
])->middleware('auth');
Route::post('/projetos/{id}/planilha/pdf', ['as' => 'projeto.planilhaPdf', 'uses' => 'ProjetoController@planilhaPdf']);
//==============================================================================================

//atividades
Route::get('/projetos/{id}/atividades/criar', [
    'as' => 'atividade.criar', 'uses' => 'AtividadeController@create'
])->middleware('auth');

Route::post('/projetos/{id}/atividades', [
    'as' => 'atividade.salvar', 'uses' => 'AtividadeController@store'
])->middleware('auth');

Route::get('/projetos/{id}/atividades/adicionar', [
    'as' => 'atividade.adicionar', 'uses' => 'AtividadeController@adicionar_atividade'
])->middleware('auth');

Route::post('/projetos/{id}/atividades/adicionar', [
    'as' => 'atividade.salvar.adicionar', 'uses' => 'AtividadeController@store_add_atividade'
])->middleware('auth');


Route::get('/projetos/{id}/periodos/adicionar', [
    'as' => 'periodo.adicionar', 'uses' => 'AtividadeController@adicionar_periodo'
])->middleware('auth');

Route::put('/projetos/{id}/periodos/atualizar', [
    'as' => 'periodo.atualizar', 'uses' => 'AtividadeController@atualizar_periodo'
])->middleware('auth');

Route::post('/projetos/{id}/periodos/adicionar', [
    'as' => 'periodo.salvar.adicionar', 'uses' => 'AtividadeController@store_add_periodo'
])->middleware('auth');

Route::post('/periodos/{id}/finalizar', [
    'as' => 'periodo.finalizar', 'uses' => 'AtividadeController@finalizar'
])->middleware('auth');



//categorias
Route::get('/projetos/{id}/categorias/criar', [
    'as' => 'categoria.criar', 'uses' => 'CategoriaController@create'
])->middleware('auth');

Route::get('/projetos/{id}/categorias/{nomeCategoria}/rubricas/adicionar', [
    'as' => 'categoria.rubrica.adicionar', 'uses' => 'CategoriaController@adicionar_rubrica'
])->middleware('auth');

Route::post('/projetos/{id}/categorias/{nomeCategoria}/rubricas/adicionar', [
    'as' => 'categoria.rubrica.salvar.adicionar', 'uses' => 'CategoriaController@store_add_rubrica'
])->middleware('auth');

Route::post('/projetos/{id}/categorias', [
    'as' => 'categoria.salvar', 'uses' => 'CategoriaController@store'
])->middleware('auth');


//rubricas
Route::get('/rubrica/criar', [
    'as' => 'rubrica.criar', 'uses' => 'RubricaController@create'
])->middleware('auth');

Route::post('/rubrica', [
    'as' => 'rubrica.salvar', 'uses' => 'RubricaController@store'
])->middleware('auth');

//graficos
Route::get('/projeto/{id}/view', [
    'as' => 'projeto.graficos.view', 'uses' => 'ProjetoController@graficos_view'
])->middleware('auth');
