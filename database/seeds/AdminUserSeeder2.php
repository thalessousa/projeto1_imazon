<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->insert([
            'admin'         => true,
            'nome'          => 'Administrador2',
            'nome_usuario'  => 'admin2',
            'email'         => 'admin2@imazon.org',
            'senha'         => bcrypt('12345'),
        ]);
    }
}
