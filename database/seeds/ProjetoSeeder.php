<?php

// namespace App;

use App\User;
use App\Projeto;
use App\Atividade;
use App\Categoria;
use App\Rubrica;

use Illuminate\Database\Seeder;

class ProjetoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where('email', 'admin@imazon.org')->first();

        /**
         * Criar projeto
         */
        $projeto = new Projeto;
        $projeto->nome = 'Moore Áreas Protegidas';
        $projeto->orcado = 500203.25;
        $projeto->data_inicio = DateTime::createFromFormat('d-m-Y','01-01-2017');
        $projeto->data_fim    = DateTime::createFromFormat('d-m-Y','31-12-2018');
        $projeto->n_atividades = 2;

        $admin->projetos()->save($projeto);

        
        /**
         * Criar atividade
         */
        $atividade1 = new Atividade;
        $atividade1->nome = '1.1.1';
        $atividade1->data_inicio = DateTime::createFromFormat('d-m-Y','01-01-2017');
        $atividade1->data_fim    = DateTime::createFromFormat('d-m-Y','31-12-2017');

        $atividade2 = new Atividade;
        $atividade2->nome = '1.1.2';
        $atividade2->data_inicio = DateTime::createFromFormat('d-m-Y','01-01-2017');
        $atividade2->data_fim    = DateTime::createFromFormat('d-m-Y','31-12-2017');
        
        $projeto->atividades()->save($atividade1);
        $projeto->atividades()->save($atividade2);

        /**
         * Criar categorias e rúbricas
         */
        $categoria1 = new Categoria;
        $categoria1->nome = 'Personnel & Time Allocation';
        $categoria1->valor = 151934.50;
        
        $categoria1_2 = new Categoria;
        $categoria1_2->nome = 'Personnel & Time Allocation';
        $categoria1_2->valor = 73805.00;

        $atividade1->categorias()->save($categoria1);
        $atividade2->categorias()->save($categoria1_2);

        $rubrica1 = new Rubrica;
        $rubrica1->nome = 'Antonio Victor Fonseca - Assistant Researcher II';
        $rubrica1->orcado = 32952;

        $rubrica1_2 = new Rubrica;
        $rubrica1_2->nome = 'Amintas Brandão Jr. - Adjunct Researcher';
        $rubrica1_2->orcado =  35734;

        $rubrica2 = new Rubrica;
        $rubrica2->nome = 'Dalton Cardoso - Assistant Researcher I';
        $rubrica2->orcado = 27636;

        $rubrica2_2 = new Rubrica;
        $rubrica2_2->nome = 'Helton Rodrigues - Information technology (IT)';
        $rubrica2_2->orcado = 850.50;

        $rubrica3 = new Rubrica;
        $rubrica3->nome = 'Fabiany Lucidos - Financial Manager';
        $rubrica3->orcado = 9536;

        $rubrica3_2 = new Rubrica;
        $rubrica3_2->nome = 'Fabiany Lucidos - Financial Manager';
        $rubrica3_2->orcado = 4768.00;
        
        $categoria1->rubricas()->save($rubrica1);
        $categoria1->rubricas()->save($rubrica2);
        $categoria1->rubricas()->save($rubrica3);

        $categoria1_2->rubricas()->save($rubrica1_2);
        $categoria1_2->rubricas()->save($rubrica2_2);
        $categoria1_2->rubricas()->save($rubrica3_2);
        
        $categoria2 = new Categoria;
        $categoria2->nome = 'Benefits';
        $categoria2->valor = 162196.50;
        
        $categoria2_2 = new Categoria;
        $categoria2_2->nome = 'Benefits';
        $categoria2_2->valor = 62360.50;

        $atividade1->categorias()->save($categoria2);
        $atividade2->categorias()->save($categoria2_2);
 
        $rubrica4 = new Rubrica;
        $rubrica4->nome = 'Antonio Victor Fonseca - Assistant Researcher II';
        $rubrica4->orcado = 29980;

        $rubrica4_2 = new Rubrica;
        $rubrica4_2->nome = 'Amintas Brandão Jr. - Adjunct Researcher';
        $rubrica4_2->orcado = 24518.00;

        $rubrica5 = new Rubrica;
        $rubrica5->nome = 'Dalton Cardoso - Assistant Researcher I';
        $rubrica5->orcado = 33888;

        $rubrica5_2 = new Rubrica;
        $rubrica5_2->nome = 'Helton Rodrigues - Information technology (IT)';
        $rubrica5_2->orcado = 2199.50;

        $rubrica6 = new Rubrica;
        $rubrica6->nome = 'Fabiany Lucidos - Financial Manager';
        $rubrica6->orcado = 7967;

        $rubrica6_2 = new Rubrica;
        $rubrica6_2->nome = 'Fabiany Lucidos - Financial Manager';
        $rubrica6_2->orcado =  3983.50;

        $categoria2->rubricas()->save($rubrica4);
        $categoria2->rubricas()->save($rubrica5);
        $categoria2->rubricas()->save($rubrica6);

        $categoria2_2->rubricas()->save($rubrica4_2);
        $categoria2_2->rubricas()->save($rubrica5_2);
        $categoria2_2->rubricas()->save($rubrica6_2);

    }
}
