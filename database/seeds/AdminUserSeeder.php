<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->insert([
            'admin'         => true,
            'nome'          => 'Administrador',
            'nome_usuario'  => 'admin',
            'email'         => 'admin@imazon.org',
            'senha'         => bcrypt('12345'),
        ]);
    }
}
