<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NovosCamposOrcadoExecutadoSaldo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rubrica', function (Blueprint $table) {
            $table->double('executado_manual', 10, 2);
            $table->double('saldo_orcamentario', 10, 2);
            $table->double('saldo_remanescente', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rubrica', function (Blueprint $table) {
            $table->dropColumn('executado_manual');
            $table->dropColumn('saldo_orcamentario');
            $table->dropColumn('saldo_remanescente');
        });        
    }
}
