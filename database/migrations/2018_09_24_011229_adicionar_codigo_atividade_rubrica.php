<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionarCodigoAtividadeRubrica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fnAddCodigo = function(Blueprint $table){
            $table->string('codigo',15)->nullable();
        };

        Schema::table('atividade', $fnAddCodigo);
        Schema::table('rubrica',   $fnAddCodigo);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $fnDelCodigo = function (Blueprint $table) {
            $table->dropColumn('codigo');
        };

        Schema::table('atividade', $fnDelCodigo);
        Schema::table('rubrica',   $fnDelCodigo);
    }
}
