<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// use DB;

class ModificarPrecisaoValores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `categoria` MODIFY valor DOUBLE(11,2)');
        DB::statement('ALTER TABLE `rubrica` MODIFY orcado DOUBLE(11,2)');
        DB::statement('ALTER TABLE `projeto` MODIFY orcado DOUBLE(11,2)');
        DB::statement('ALTER TABLE `projeto` MODIFY aplicacao DOUBLE(11,2)');
        DB::statement('ALTER TABLE `atividade` MODIFY orcado DOUBLE(11,2)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `categoria` MODIFY valor DOUBLE(11,2)');
        DB::statement('ALTER TABLE `rubrica` MODIFY orcado DOUBLE(11,2)');
        DB::statement('ALTER TABLE `projeto` MODIFY orcado DOUBLE(11,2)');
        DB::statement('ALTER TABLE `projeto` MODIFY aplicacao DOUBLE(11,2)');
        DB::statement('ALTER TABLE `atividade` MODIFY orcado DOUBLE(11,2)');
    }
}
