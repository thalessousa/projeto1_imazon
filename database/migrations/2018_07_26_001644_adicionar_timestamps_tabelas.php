<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionarTimestampsTabelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('categoria', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('servico', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('projeto', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('planilha', function (Blueprint $table) {
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('categoria', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('servico', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('projeto', function (Blueprint $table) {
            $table->dropTimestamps();
        });

        Schema::table('planilha', function (Blueprint $table) {
            $table->dropTimestamps();
        });
    }
}
