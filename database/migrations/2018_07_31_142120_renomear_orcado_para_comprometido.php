<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenomearOrcadoParaComprometido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rubrica', function (Blueprint $table) {
            $table->renameColumn('orcado','comprometido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rubrica', function (Blueprint $table) {
            $table->renameColumn('comprometido', 'orcado');
        });
    }
}
