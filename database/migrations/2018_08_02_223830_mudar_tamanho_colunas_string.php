<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MudarTamanhoColunasString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->string('nome',255)->change();
        });

        Schema::table('projeto', function (Blueprint $table) {
            $table->string('nome', 255)->change();
            $table->string('descricao', 255)->change();
        });

        Schema::table('atividade', function (Blueprint $table) {
            $table->string('nome', 255)->change();            
        });

        Schema::table('categoria', function (Blueprint $table) {
            $table->string('nome', 255)->change();
            
        });

        Schema::table('rubrica', function (Blueprint $table) {
            $table->string('nome', 255)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->string('nome', 45)->change();
        });

        Schema::table('projeto', function (Blueprint $table) {
            $table->string('nome', 45)->change();
            $table->string('descricao', 100)->change();
        });

        Schema::table('atividade', function (Blueprint $table) {
            $table->string('nome', 45)->change();
        });

        Schema::table('categoria', function (Blueprint $table) {
            $table->string('nome', 45)->change();

        });

        Schema::table('rubrica', function (Blueprint $table) {
            $table->string('nome', 45)->change();
        });
    }
}
