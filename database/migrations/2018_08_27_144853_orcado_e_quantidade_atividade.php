<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrcadoEQuantidadeAtividade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projeto', function (Blueprint $table) {
            $table->double('orcado', 10, 2);
            $table->integer('n_atividades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projeto', function (Blueprint $table) {
            $table->dropColumn('orcado');
            $table->dropColumn('n_atividades');
        });

    }
}
