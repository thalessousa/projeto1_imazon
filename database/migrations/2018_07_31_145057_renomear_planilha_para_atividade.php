<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenomearPlanilhaParaAtividade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('planilha', 'atividade');

        Schema::table('categoria', function (Blueprint $table) {
            $table->renameColumn('planilha_id','atividade_id');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('atividade','planilha');

        Schema::table('categoria', function (Blueprint $table) {
            $table->renameColumn('atividade_id', 'planilha_id');
        });        
    }
}
