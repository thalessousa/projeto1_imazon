<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaRegistroPlanilha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_planilha', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('rubrica_id');

            $table->foreign('rubrica_id')->references('id')->on('rubrica');

            $table->date('emissao');
            $table->date('baixa');
            
            $table->string('numero');
            $table->string('gerencial');
            $table->string('sacado_cedente');
            $table->string('historico');

            $table->double('valor',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_planilha');
    }
}
