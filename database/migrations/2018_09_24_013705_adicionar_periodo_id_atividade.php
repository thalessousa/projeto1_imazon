<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionarPeriodoIdAtividade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atividade', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('periodo_id');
            // $table->foreign('atividade_id')->references('id')->on('atividade'); //NÃO FUNCIONA
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atividade', function (Blueprint $table) {
            $table->dropColum('periodo_id');
            // $table->dropForeign(['periodo_id']);
        });
    }
}
