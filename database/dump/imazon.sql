SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `imazon` DEFAULT CHARACTER SET utf8 ;
USE `imazon` ;

-- -----------------------------------------------------
-- Table `imazon`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`usuario` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`usuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `admin` TINYINT(4) NOT NULL DEFAULT '0' ,
  `nome_usuario` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NOT NULL ,
  `senha` VARCHAR(255) NOT NULL ,
  `nome` VARCHAR(255) NULL DEFAULT NULL ,
  `remember_token` VARCHAR(100) NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `nome_usuario_UNIQUE` (`nome_usuario` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `imazon`.`projeto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`projeto` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`projeto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `criador` INT(11) NOT NULL ,
  `nome` VARCHAR(255) NOT NULL ,
  `descricao` VARCHAR(255) NULL DEFAULT NULL ,
  `data_inicio` DATE NULL DEFAULT NULL ,
  `data_fim` DATE NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  `orcado` DOUBLE(10,2) NOT NULL ,
  `n_atividades` INT(11) NOT NULL ,
  `finalizado` TINYINT(1) NOT NULL DEFAULT '0' ,
  `aplicacao` DOUBLE NULL DEFAULT NULL ,
  `resgatado` DOUBLE NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_projeto_usuario1_idx` (`criador` ASC) ,
  CONSTRAINT `fk_projeto_usuario1`
    FOREIGN KEY (`criador` )
    REFERENCES `imazon`.`usuario` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 57
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `imazon`.`atividade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`atividade` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`atividade` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `projeto_id` INT(11) NOT NULL ,
  `nome` VARCHAR(255) NOT NULL DEFAULT 'Planilha' ,
  `data_inicio` DATE NOT NULL ,
  `data_fim` DATE NOT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  `orcado` DOUBLE(10,2) NOT NULL ,
  `is_periodo` TINYINT(1) NOT NULL DEFAULT '0' ,
  `codigo` VARCHAR(15) NULL DEFAULT NULL ,
  `periodo_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_planilha_projeto_idx` (`projeto_id` ASC) ,
  CONSTRAINT `fk_planilha_projeto`
    FOREIGN KEY (`projeto_id` )
    REFERENCES `imazon`.`projeto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 112
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `imazon`.`categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`categoria` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`categoria` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `atividade_id` INT(11) NOT NULL ,
  `nome` VARCHAR(255) NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  `valor` DOUBLE(8,2) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_categoria_planilha1_idx` (`atividade_id` ASC) ,
  CONSTRAINT `fk_categoria_planilha1`
    FOREIGN KEY (`atividade_id` )
    REFERENCES `imazon`.`atividade` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 126
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `imazon`.`migrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`migrations` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`migrations` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `migration` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `batch` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 57
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `imazon`.`password_resets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`password_resets` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`password_resets` (
  `email` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `token` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  INDEX `password_resets_email_index` (`email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `imazon`.`rubrica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`rubrica` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`rubrica` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `categoria_id` INT(11) NOT NULL ,
  `nome` VARCHAR(255) NOT NULL ,
  `comprometido` DOUBLE NULL DEFAULT NULL ,
  `executado` DOUBLE NULL DEFAULT NULL ,
  `saldo` DOUBLE NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  `orcado` DOUBLE NOT NULL ,
  `recebido` DOUBLE(10,2) NOT NULL ,
  `codigo` VARCHAR(15) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_servico_categoria1_idx` (`categoria_id` ASC) ,
  CONSTRAINT `fk_servico_categoria1`
    FOREIGN KEY (`categoria_id` )
    REFERENCES `imazon`.`categoria` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 222
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `imazon`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `imazon`.`users` ;

CREATE  TABLE IF NOT EXISTS `imazon`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `email` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `password` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
  `remember_token` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT NULL ,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `users_email_unique` (`email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

USE `imazon` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
